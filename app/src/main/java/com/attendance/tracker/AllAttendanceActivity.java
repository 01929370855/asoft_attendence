package com.attendance.tracker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.attendance.tracker.activity.AttendanceReport.AttendanceHistoryActivity;
import com.attendance.tracker.activity.MonthlyReportActivity;
import com.attendance.tracker.activity.ProfileDetails.ProfileDetailsActivity;
import com.attendance.tracker.activity.UserListActivity;
import com.attendance.tracker.activity.company.BlockUserListActivity;
import com.attendance.tracker.activity.company.CompanyDueListActivity;
import com.attendance.tracker.activity.company.LeaderListActivity;
import com.attendance.tracker.activity.leader.DateWiseGroupReportActivity;
import com.attendance.tracker.activity.master.CompanyEdit.UserEditActivity;
import com.attendance.tracker.activity.user.MapTestUserActivity;
import com.attendance.tracker.agent.SalesReportActivity;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;

import de.hdodenhof.circleimageview.CircleImageView;

public class AllAttendanceActivity extends AppCompatActivity {
    AppSessionManager appSessionManager;
    TextView userName, usermobile;
    String userId, userType;
    CircleImageView imgview;
    CheckInternetConnection internetConnection;
    MapTestUserActivity.LocationService mLocationService = new MapTestUserActivity.LocationService();
    Intent mServiceIntent;
    String getLink;
    AppCompatImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_attendance);

        initVariable();
        initView();
        iniFunction();
        initListener();
    }


    private void initVariable() {

        appSessionManager = new AppSessionManager(this);
        internetConnection = new CheckInternetConnection();
        mServiceIntent = new Intent(this, mLocationService.getClass());
    }

    private void initView() {
        userName = findViewById(R.id.userName);
        usermobile = findViewById(R.id.userMobile);
        imgview = findViewById(R.id.img);
        back = findViewById(R.id.back);
    }

    private void iniFunction() {

    }

    @Override
    protected void onResume() {
        super.onResume();
//        userName.setText(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERNAME));
        userId = appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID);
        userType = appSessionManager.getUserDetails().get(AppSessionManager.KEY_CATEGORY);
    }

    private void initListener() {


        findViewById(R.id.card2).setOnClickListener(view -> gotoLeaderAttendanceList());
        findViewById(R.id.card1).setOnClickListener(view -> gotoDateWizeList());
        findViewById(R.id.card3).setOnClickListener(view -> AttendanceReport());
        findViewById(R.id.card4).setOnClickListener(view -> gotoDateWizeSignleReport());
        findViewById(R.id.card5).setOnClickListener(view -> gotoDateWizeGroupReport());
        findViewById(R.id.card6).setOnClickListener(view -> gotomonthly());
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void gotomonthly() {
        Intent mIntent = new Intent(this, MonthlyReportActivity.class);
        mIntent.putExtra("userType","0");
        mIntent.putExtra("userName","Monthly");
        startActivity(mIntent);
    }

    private void gotoDateWizeGroupReport() {
        Intent mIntent = new Intent(this, DateWiseGroupReportActivity.class);
        mIntent.putExtra("userType","0");
        mIntent.putExtra("userName","Date Wise group Attendance");
        startActivity(mIntent);
    }
    private void gotoDateWizeList() {
        Intent mIntent = new Intent(this, UserListActivity.class);
        mIntent.putExtra("userType", "0");
        mIntent.putExtra("userName", "Date");
        startActivity(mIntent);

    }
    private void gotoDateWizeSignleReport() {
        Intent mIntent = new Intent(this, UserListActivity.class);
        mIntent.putExtra("userType","0");
        mIntent.putExtra("userName","Date Wise single Attendance");
        startActivity(mIntent);
    }



    private void AttendanceReport() {
        Intent mIntent = new Intent(getApplicationContext(), AttendanceHistoryActivity.class);
        mIntent.putExtra("userID", appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID));
        startActivity(mIntent);

    }


    private void gotoLeaderAttendanceList() {
        Intent mIntent = new Intent(this, LeaderListActivity.class);
        mIntent.putExtra("userType", "1");
        mIntent.putExtra("userName", "Leader");
        startActivity(mIntent);
    }


    private void gotoLeaderList() {
        Intent mIntent = new Intent(this, UserListActivity.class);
        mIntent.putExtra("userType", "1");
        mIntent.putExtra("userName", "Leader List");
        startActivity(mIntent);


    }
}