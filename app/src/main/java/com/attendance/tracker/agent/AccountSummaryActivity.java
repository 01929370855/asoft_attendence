package com.attendance.tracker.agent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.attendance.tracker.R;

public class AccountSummaryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_summary);
    }
}