package com.attendance.tracker.agent;

public interface OnBottomReachedListener {
        void onBottomReached(int position);
}
