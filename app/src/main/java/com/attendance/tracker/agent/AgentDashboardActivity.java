package com.attendance.tracker.agent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.BuildConfig;
import com.attendance.tracker.ChangePassActivity;
import com.attendance.tracker.R;
import com.attendance.tracker.ServerMaintainActivity;
import com.attendance.tracker.SupportActivity;
import com.attendance.tracker.UserBlockActivity;
import com.attendance.tracker.activity.AgentList.AgentListActivity;
import com.attendance.tracker.activity.NewCompanyList.NewCompanyListActivity;
import com.attendance.tracker.activity.ProfileDetails.MasterProfileDetailsActivity;
import com.attendance.tracker.activity.ProfileDetails.ProfileDetailsData;
import com.attendance.tracker.activity.company.CompanyActivity;
import com.attendance.tracker.activity.company.message.MessageActivity;
import com.attendance.tracker.activity.login.LoginActivity;
import com.attendance.tracker.activity.master.CompanyEdit.UserEditActivity;
import com.attendance.tracker.activity.master.CreateCompanyActivity;
import com.attendance.tracker.activity.master.MasterActivity;
import com.attendance.tracker.activity.notice.view.NoticeActivity;
import com.attendance.tracker.activity.user.UserMainActivity;
import com.attendance.tracker.data.ControllingModel;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.google.gson.JsonObject;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgentDashboardActivity extends AppCompatActivity {

    AppSessionManager appSessionManager;
    TextView userName, userEmail;
    CircleImageView img;
    CheckInternetConnection internetConnection;
    int serverStatus,userBlock;
    String getLink;
    String userId;
    String uName;
    TextView notification_badge,email_badge;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_dashboard);
        initVariable();
        initView();
        iniFunction();
        initListener();
    }


    private void initVariable() {

        internetConnection = new CheckInternetConnection();
        appSessionManager = new AppSessionManager(this);
        userId = appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID);
        userId = appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERNAME);
    }

    private void initView() {
        userName = findViewById(R.id.userName);
        userEmail = findViewById(R.id.userMobile);
        img = findViewById(R.id.profile_image);
        notification_badge = findViewById(R.id.notification_badge);
        email_badge = findViewById(R.id.email_badge);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID));
         getSignalData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID));
    }

    private void iniFunction() {
        getServerData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID));

    }


    private void initListener() {
        findViewById(R.id.cvCreateUser).setOnClickListener(view -> callCreateCompany());
        findViewById(R.id.cvUserEdit).setOnClickListener(view -> callEditCompany());
        findViewById(R.id.cvUserList).setOnClickListener(view -> gotoUserList());
        findViewById(R.id.logout).setOnClickListener(view -> logout());
        findViewById(R.id.cv_chnagepass).setOnClickListener(view -> chnagePass());
        findViewById(R.id.profile).setOnClickListener(view -> Profile());
        findViewById(R.id.cv_account_summary).setOnClickListener(view -> gotoAccountSummary());
        findViewById(R.id.cv_block_company).setOnClickListener(view -> gotoBlockCompany());
        findViewById(R.id.cv_sales_report).setOnClickListener(view -> gotoSales());
        findViewById(R.id.cv_commission_list).setOnClickListener(view -> gotoComission());
        findViewById(R.id.cv_deu_list).setOnClickListener(view -> gotoDueList());

        findViewById(R.id.cv_support).setOnClickListener(view -> support());
        findViewById(R.id.cvShareApp).setOnClickListener(view -> shareApp());
        findViewById(R.id.cv_Changepass).setOnClickListener(view -> ChnagePass());
        findViewById(R.id.lytNotification).setOnClickListener(v -> gotoNotification());
        findViewById(R.id.lytemail).setOnClickListener(v -> gotoMail());
        //  findViewById(R.id.cv_addAgent).setOnClickListener(view -> AgentList());
    }
    private void gotoNotification() {
        startActivity(new Intent(this, NoticeActivity.class));
    }
    private void gotoMail() {
        startActivity(new Intent(this, MessageActivity.class));
    }
    private void shareApp() {
        getLink = "https://artificial-soft.com/"+uName;
        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setDomainUriPrefix("https://artisoft.page.link/")
                .setLink(Uri.parse(getLink))
                .setAndroidParameters(
                        new DynamicLink.AndroidParameters.Builder("com.attendance.tracker")
                                //.setMinimumVersion(125)
                                .build())

                .buildShortDynamicLink()
                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            Uri flowchartLink = task.getResult().getPreviewLink();
                            try {
                                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                                shareIntent.setType("text/plain");
                                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Attendance Tracking");
                                assert shortLink != null;
                                getLink= shortLink.toString();
                                shareIntent.putExtra(Intent.EXTRA_TEXT, getLink);
                                startActivity(Intent.createChooser(shareIntent, "choose one"));
                            } catch(Exception e) {
                                //e.toString();
                            }
                        } else {
                            // Error
                            // ...
                        }
                    }
                });
    }
    private void support() {
        Intent mIntent = new Intent(getApplicationContext(), SupportActivity.class);
        startActivity(mIntent);
    }
    private void ChnagePass() {
        Intent mIntent = new Intent(getApplicationContext(), ChangePassActivity.class);
        mIntent.putExtra("userName", appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERNAME));
        mIntent.putExtra("userPass", appSessionManager.getUserDetails().get(AppSessionManager.KEY_PASSWORD));
        startActivity(mIntent);
    }
    private void gotoAccountSummary() {
        startActivity(new Intent(this,AccountSummaryActivity.class));
    }
    private void gotoDueList() {
        startActivity(new Intent(this,DueListActivity.class));
    }
    private void gotoBlockCompany() {
        Intent mIntent = new Intent(getApplicationContext(), NewCompanyListActivity.class);
        mIntent.putExtra("agentID", "");
        startActivity(mIntent);
    }
    private void gotoSales() {
        startActivity(new Intent(this, SalesReportActivity.class));
    }
    private void gotoComission() {
        startActivity(new Intent(this, CommisionListActivity.class));
    }
    private void AgentList() {
        Intent mIntent = new Intent(getApplicationContext(), AgentListActivity.class);
        mIntent.putExtra("userId", appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID));
        startActivity(mIntent);
    }

    private void Profile() {
        Intent mIntent = new Intent(getApplicationContext(), MasterProfileDetailsActivity.class);
        mIntent.putExtra("userId", appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID));
        startActivity(mIntent);
    }

    private void chnagePass() {
        Intent mIntent = new Intent(getApplicationContext(), ChangePassActivity.class);
        mIntent.putExtra("userName", appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERNAME));
        mIntent.putExtra("userPass", appSessionManager.getUserDetails().get(AppSessionManager.KEY_PASSWORD));
        startActivity(mIntent);
    }

    private void callEditCompany() {
        Intent mIntent = new Intent(this, UserEditActivity.class);
        mIntent.putExtra("userType","4");
        mIntent.putExtra("userName","Company Edit");
        startActivity(mIntent);

    }

    private void gotoUserList() {

        Intent mIntent = new Intent(this, NewCompanyListActivity.class);
        mIntent.putExtra("userType","4");
        mIntent.putExtra("userName","Company List");
        mIntent.putExtra("agentID","");
        startActivity(mIntent);
    }

    private void logout() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getString(R.string.app_name))
                .setMessage("Are you sure you want to Logout App?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        appSessionManager.logoutUser();
                        startActivity(new Intent(AgentDashboardActivity.this, LoginActivity.class));
                        finish();                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    private void callCreateCompany() {
        Intent nIntent = new Intent(this,CreateCompanyActivity.class);
        nIntent.putExtra("source","agent");
        startActivity(nIntent);
    }


    public void getServerData(String userId) {
        if (internetConnection.isInternetAvailable(AgentDashboardActivity.this)) {

            final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getServerStatus(userId).enqueue(new Callback<ControllingModel>() {
                @SuppressLint("NotifyDataSetChanged")
                @Override
                public void onResponse(Call<ControllingModel> call, Response<ControllingModel> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getError() == 0) {
                            serverStatus = response.body().getServerStatus();
                            userBlock = response.body().getUserBlock();
                            if (serverStatus != 0){
                                Intent mIntent = new Intent(AgentDashboardActivity.this, ServerMaintainActivity.class);
                                startActivity(mIntent);
                                finish();
                            }else {
                                if (userBlock != 0){
                                    Intent mIntent = new Intent(AgentDashboardActivity.this, UserBlockActivity.class);
                                    startActivity(mIntent);
                                    finish();
                                }

                            }
                        } else {
                            Toast.makeText(AgentDashboardActivity.this, "some thing went to wrong!", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ControllingModel> call, Throwable t) {
                    dialog.dismiss();
                    Log.d("LOGIN", "onFailure: " + t.getMessage());
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }
    public void getData(String userID) {
        if (internetConnection.isInternetAvailable(AgentDashboardActivity.this)) {
            // progressBar.setVisibility(View.VISIBLE);
            final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getProfiledetails(userID).enqueue(new Callback<ProfileDetailsData>() {
                @Override
                public void onResponse(Call<ProfileDetailsData> call, Response<ProfileDetailsData> response) {
                    if (response.isSuccessful()) {
                        if (response.isSuccessful())
                            //  progressBar.setVisibility(View.GONE);
                            dialog.dismiss();
                        userName.setText(response.body().getName());
                        userEmail.setText(response.body().getMobile());

                        Glide.with(getApplicationContext())
                                .load(BuildConfig.BASE_URL + "" + response.body().getPhoto())
                                .into(img);



                    }
                }


                @Override
                public void onFailure(Call<ProfileDetailsData> call, Throwable t) {
                    t.printStackTrace();
                    // progressBar.setVisibility(View.GONE);
                    dialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }
    public void getSignalData(String userID) {
        if (internetConnection.isInternetAvailable(AgentDashboardActivity.this)) {
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getSignal(userID).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.isSuccessful()) {
                        if (response.isSuccessful()){
                            if (response.body().get("error").getAsInt() == 0){
                                email_badge.setText(""+response.body().get("mails").getAsString());
                                notification_badge.setText(""+response.body().get("notice").getAsInt());
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();

                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }


}