package com.attendance.tracker.network;

public class ConstantValue {
    public static final String URL ="http://attendance-tracker.com/";
    public static final String ImageURL ="http://101bd.info/apps/stores";

    public static final String api_res_key_error = "error";
    public static final String api_res_key_error_report = "error_report";
    public static final String api_res_key_reports = "reports";
    public static final int int_value_10 = 10;
    public static final String str_value_loadmore_10 = ",10";

}
