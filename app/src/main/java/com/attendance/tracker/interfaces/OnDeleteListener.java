package com.attendance.tracker.interfaces;

import android.view.View;

public interface OnDeleteListener {
    void itemDeleteClick(View view, int position);
}


