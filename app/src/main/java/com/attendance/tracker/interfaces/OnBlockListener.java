package com.attendance.tracker.interfaces;

import android.view.View;

public interface OnBlockListener{
    void itemUserBlockClick(View view, int position);

}
