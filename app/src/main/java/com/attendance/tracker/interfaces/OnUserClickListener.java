package com.attendance.tracker.interfaces;

import android.view.View;

public interface OnUserClickListener {
    void itemUserClick(View view, int position);
}
