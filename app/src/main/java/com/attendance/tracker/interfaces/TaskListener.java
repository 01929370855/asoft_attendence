package com.attendance.tracker.interfaces;

import android.view.View;

public interface TaskListener {
    void taskItemClick(View view, int position);
}
