package com.attendance.tracker.activity.leader;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.attendance.tracker.R;
import com.attendance.tracker.activity.leader.leaderadapter.MessageListAdapter;
import com.attendance.tracker.activity.leader.model.MessageList;
import com.attendance.tracker.activity.leader.model.ReplyList;
import com.attendance.tracker.data.JoinResponse;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.google.android.material.snackbar.Snackbar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity {
    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEvent(String title){
        Log.d("",title);
    }
    private RecyclerView mMessageRecycler;
    private MessageListAdapter mMessageAdapter;
    CheckInternetConnection internetConnection;
    String userID,taskID;
    ArrayList<ReplyList> messageList = new ArrayList<>();
    private Button sendMsg;
    AppCompatImageView back;
    private EditText chatMessage;
    AppSessionManager appSessionManager;
    private Timer timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        internetConnection = new CheckInternetConnection();
        appSessionManager = new AppSessionManager(this);
        Intent mIntent = getIntent();
        taskID = mIntent.getStringExtra("TaskID");
      //  userID = mIntent.getStringExtra("UserID");
        initView();
    }




    private void initView() {
        sendMsg = findViewById(R.id.button_gchat_send);
        chatMessage = findViewById(R.id.edit_gchat_message);
        back = findViewById(R.id.back);
        sendMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!chatMessage.getText().toString().trim().isEmpty()){
                    sendMessage(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID),taskID,chatMessage.getText().toString());
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        getChatHistory();

        startApiCalls();

    }

    // call this method to start the API calls
    private void startApiCalls() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                // make API call here using your preferred method (e.g. HttpURLConnection, OkHttp, Retrofit, etc.)
                getChatHistory();
            }
        }, 10000, new Random().nextInt(10000) + 10000);
        // schedule the task to run every 5 seconds with a random delay between 5 and 10 seconds
    }

    // call this method to stop the API calls
    private void stopApiCalls() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }
    private void sendMessage(String userID,String taskID, String msg){
        if (internetConnection.isInternetAvailable(ChatActivity.this)) {


            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.sendMessage(userID,taskID,msg).enqueue(new Callback<JoinResponse>() {
                @Override
                public void onResponse(Call<JoinResponse> call, Response<JoinResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getError() == 0) {
                            chatMessage.setText("");
                            getChatHistory();

                        } else if (response.body().getError() == 1) {

                        }
                    }
                }

                @Override
                public void onFailure(Call<JoinResponse> call, Throwable t) {

                    Log.d("LOGIN", "onFailure: " + t.getMessage());
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void getChatHistory() {


        if (internetConnection.isInternetAvailable(this)) {
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getReplyHistory(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID),taskID).enqueue(new Callback<MessageList>() {
                @SuppressLint("NotifyDataSetChanged")
                @Override
                public void onResponse(Call<MessageList> call, Response<MessageList> response) {
                    if (response.isSuccessful()) {
                        messageList = new ArrayList<>();
                        messageList.clear();
                        messageList.addAll(response.body().getTask_reply_list());
                        mMessageRecycler = findViewById(R.id.recycler_gchat);
                        mMessageAdapter = new MessageListAdapter(ChatActivity.this, messageList);
                        mMessageRecycler.setLayoutManager(new LinearLayoutManager(ChatActivity.this));
                        mMessageRecycler.setAdapter(mMessageAdapter);
                        int i = mMessageRecycler.getAdapter().getItemCount() - 1;
                        mMessageRecycler.smoothScrollToPosition(i);
                        mMessageAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<MessageList> call, Throwable t) {

                }
            });
        } else {

        }

    }
    @Override
    public void onBackPressed() {
        finish();

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }
}