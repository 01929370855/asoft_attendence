package com.attendance.tracker.activity.company.holiday;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.attendance.tracker.R;

public class HolidayActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holiday);

        findViewById(R.id.card1).setOnClickListener(view -> gotoPersonalHoliday());
        findViewById(R.id.card2).setOnClickListener(view -> gotoWeeklyHoliday());
        findViewById(R.id.card3).setOnClickListener(view -> gotoYearlyHoliday());
        findViewById(R.id.card4).setOnClickListener(view -> gotoReport());
        findViewById(R.id.back).setOnClickListener(view -> finish());
    }

    private void gotoPersonalHoliday() {
        gotoAddHoliday("Personal");


    }

    private void gotoWeeklyHoliday() {

        gotoWeeklyHoliday("Weekly");
    }
    private void gotoReport() {
        Intent pIntent = new Intent(this, HolidayPersonalReportActivity.class);
        startActivity(pIntent);

    }

    private void gotoYearlyHoliday() {
        gotoWeeklyHoliday("Yearly");
    }

    private void gotoAddHoliday(String type) {
        Intent pIntent = new Intent(this, AddHolidayActivity.class);
        pIntent.putExtra("hType",type);
        startActivity(pIntent);
    }

    private void gotoWeeklyHoliday(String type) {
        Intent pIntent = new Intent(this, AddWeeklyActivity.class);
        pIntent.putExtra("hType",type);
        startActivity(pIntent);
    }
}