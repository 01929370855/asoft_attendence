package com.attendance.tracker.activity.company.holiday.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HolidayPersonalModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("error_report")
    @Expose
    private String errorReport;
    @SerializedName("holiday_list")
    @Expose
    private List<HolidayPersonalList> holidayList;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getErrorReport() {
        return errorReport;
    }

    public void setErrorReport(String errorReport) {
        this.errorReport = errorReport;
    }

    public List<HolidayPersonalList> getHolidayList() {
        return holidayList;
    }

    public void setHolidayList(List<HolidayPersonalList> holidayList) {
        this.holidayList = holidayList;
    }
}
