package com.attendance.tracker.activity.company.message;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.R;
import com.attendance.tracker.activity.company.message.model.MailDetailsModel;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MailDetailsActivity extends AppCompatActivity {
    private AppSessionManager appSessionManager;
    private CheckInternetConnection checkInternetConnection;

    public static final String MESSAGE_CATAGORY_INBOX = "inbox";
    public static final String MESSAGE_CATAGORY_OUTBOX = "outbox";

    TextView textViewFrom_h;
    TextView textViewMsgFrom;
    TextView textViewMsgSubject;
    TextView textViewMsgBody;
    private Bundle bundle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_details);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar_employeeOutbox);
        setSupportActionBar(myToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Message");
        }
        initView();
        appSessionManager = new AppSessionManager(MailDetailsActivity.this);
        checkInternetConnection = new CheckInternetConnection();

        bundle = getIntent().getExtras();
        downloadMailDetails();
    }

    private void initView() {
        textViewFrom_h = findViewById(R.id.from_h);
        textViewMsgFrom = findViewById(R.id.txt_outbox_messageFrom);
        textViewMsgSubject = findViewById(R.id.txt_outbox_messageSubject);
        textViewMsgBody = findViewById(R.id.txt_outbox_messageBody);
    }

    void downloadMailDetails() {
        if (checkInternetConnection.isInternetAvailable(MailDetailsActivity.this)) {
            final MaterialDialog dialog = new MaterialDialog.Builder(MailDetailsActivity.this).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();

            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getDetailsMail(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERNAME)
                            , appSessionManager.getUserDetails().get(AppSessionManager.KEY_PASSWORD)
                            , appSessionManager.getUserDetails().get(AppSessionManager.KEY_CATEGORY)
                            , bundle.getString("MESSAGEID"))
                    .enqueue(new Callback<MailDetailsModel>() {
                        @Override
                        public void onResponse(Call<MailDetailsModel> call, Response<MailDetailsModel> response) {
                            if (response.isSuccessful()) {
                                if (bundle.getString("MSGCATEGORY").contains(MESSAGE_CATAGORY_OUTBOX)) {
                                    textViewFrom_h.setText("To: ");
                                } else if (bundle.getString("MSGCATEGORY").contains(MESSAGE_CATAGORY_INBOX)) {
                                    textViewFrom_h.setText("From: ");
                                }
                                textViewMsgFrom.setText(bundle.getString("SENDERNAME"));
                                textViewMsgSubject.setText(response.body().getSubject());
                                textViewMsgBody.setText(response.body().getMessage());
                            } else {
                                Log.e("MAILDETAILS", "Error :" + response.code());
                                Toast.makeText(MailDetailsActivity.this, "Error!", Toast.LENGTH_SHORT).show();
                            }
                            dialog.dismiss();
                        }

                        @Override
                        public void onFailure(Call<MailDetailsModel> call, Throwable t) {
                            dialog.dismiss();
                            Log.d("OUTBOXLIST", "onFailure: " + t.getMessage());
                            Toast.makeText(MailDetailsActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            Snackbar.make(getWindow().getDecorView().getRootView(), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.message_menu, menu);
        if (bundle.getString("MSGCATEGORY").contains(MESSAGE_CATAGORY_OUTBOX)) {
            MenuItem item = menu.findItem(R.id.action_reply);
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                MailDetailsActivity.this.finish();
                return true;
            case R.id.action_reply:
                Intent goComposeMailForReply = new Intent(MailDetailsActivity.this, ComposeMailActivity.class);
                goComposeMailForReply.putExtra("ACTIONIDENTY", "MAILREPLY");
                goComposeMailForReply.putExtras(bundle);
                startActivity(goComposeMailForReply);
                break;
            case R.id.action_forward:
                Intent goComposeMailForForward = new Intent(MailDetailsActivity.this, ComposeMailActivity.class);
                goComposeMailForForward.putExtra("MAILBODY", textViewMsgBody.getText().toString());
                goComposeMailForForward.putExtra("ACTIONIDENTY", "MAILFORWARD");
                goComposeMailForForward.putExtras(bundle);
                startActivity(goComposeMailForForward);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            MailDetailsActivity.this.finish();
            return true;
        }
        return false;
    }
}
