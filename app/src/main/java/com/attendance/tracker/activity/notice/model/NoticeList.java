package com.attendance.tracker.activity.notice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NoticeList {
    @SerializedName("notice_id")
    @Expose
    private String noticeId;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Post_Time")
    @Expose
    private String postTime;
    @SerializedName("Read")
    @Expose
    private String read;

    public String getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(String noticeId) {
        this.noticeId = noticeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPostTime() {
        return postTime;
    }

    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }

    public String getRead() {
        return read;
    }

    public void setRead(String read) {
        this.read = read;
    }
}
