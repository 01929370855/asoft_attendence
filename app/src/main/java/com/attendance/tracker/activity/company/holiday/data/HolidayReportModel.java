package com.attendance.tracker.activity.company.holiday.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HolidayReportModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("error_report")
    @Expose
    private String errorReport;
    @SerializedName("holiday_list")
    @Expose
    private List<HolidayList> holidayList;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getErrorReport() {
        return errorReport;
    }

    public void setErrorReport(String errorReport) {
        this.errorReport = errorReport;
    }

    public List<HolidayList> getHolidayList() {
        return holidayList;
    }

    public void setHolidayList(List<HolidayList> holidayList) {
        this.holidayList = holidayList;
    }
}
