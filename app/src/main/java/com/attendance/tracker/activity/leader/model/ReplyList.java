package com.attendance.tracker.activity.leader.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReplyList {
    @SerializedName("Task_id")
    @Expose
    private String taskId;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Time")
    @Expose
    private String time;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Who")
    @Expose
    private String who;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }
}
