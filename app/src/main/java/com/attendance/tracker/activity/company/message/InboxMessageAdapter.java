package com.attendance.tracker.activity.company.message;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.R;
import com.attendance.tracker.activity.company.message.model.CallFromInboxMessageList;
import com.attendance.tracker.activity.company.message.model.InboxDataListModel;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InboxMessageAdapter extends RecyclerView.Adapter<InboxMessageAdapter.MyViewHolder> {

    private Context context;
    private List<InboxDataListModel> idlModel;
    private CallFromInboxMessageList callFromInboxMessageList;
    String msgStatus;
    AppSessionManager appSessionManager;

    public InboxMessageAdapter(Context context, List<InboxDataListModel> idlModel, CallFromInboxMessageList callFromInboxMessageList) {
        this.context = context;
        this.idlModel = idlModel;
        this.callFromInboxMessageList = callFromInboxMessageList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayoutBold;
        RelativeLayout relativeLayoutNormal;
        ImageView senderImage;
        TextView messageSenderBold;
        TextView messegeSendTimeBold;
        TextView messageContentBold;
        TextView messageSenderNormal;
        TextView messegeSendTimeNormal;
        TextView messageContentNormal;

        public MyViewHolder(View itemView) {
            super(itemView);
           // ButterKnife.bind(this, itemView);
            relativeLayoutBold = itemView.findViewById(R.id.rl_inboxBold);
            relativeLayoutNormal = itemView.findViewById(R.id.rl_inboxNormal);
            senderImage = itemView.findViewById(R.id.message_sender_image);
            messageSenderBold = itemView.findViewById(R.id.messege_senderBold);
            messegeSendTimeBold = itemView.findViewById(R.id.messege_send_timeBold);
            messageContentBold = itemView.findViewById(R.id.messege_contentBold);
            messageSenderNormal = itemView.findViewById(R.id.messege_senderNormal);
            messegeSendTimeNormal = itemView.findViewById(R.id.messege_send_timeNormal);
            messageContentNormal = itemView.findViewById(R.id.messege_contentNormal);

        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_inboxlist, parent, false);
        return new MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final InboxDataListModel mModel = idlModel.get(position);
        msgStatus = mModel.getRead();
        holder.messageSenderBold.setText(mModel.getSenderName());
        holder.messegeSendTimeBold.setText(mModel.getTime());
        holder.messageContentBold.setText(mModel.getSubject());

        holder.messageSenderNormal.setText(mModel.getSenderName());
        holder.messegeSendTimeNormal.setText(mModel.getTime());
        holder.messageContentNormal.setText(mModel.getSubject());

        appSessionManager = new AppSessionManager(context);

        String userImageUrl = appSessionManager.getUserDetails().get(ConstantValue.URL) + mModel.getSenderImage();

        if (msgStatus.equals("0")) {
            holder.relativeLayoutBold.setVisibility(View.VISIBLE);
            holder.relativeLayoutNormal.setVisibility(View.GONE);
        } else if (msgStatus.equals("1")) {
            holder.relativeLayoutBold.setVisibility(View.GONE);
            holder.relativeLayoutNormal.setVisibility(View.VISIBLE);
        }

        Glide.with(context).load(userImageUrl).apply(new RequestOptions().error(R.drawable.logo).
                placeholder(R.drawable.logo).fitCenter()).into(holder.senderImage);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // parseData(mModel.getMailId(), mModel.getSenderId(), mModel.getSenderCategory(), mModel.getSubject(), mModel.getSenderName());
                callFromInboxMessageList.onItemClickListner(mModel.getMailId(), mModel.getSenderId(), mModel.getSenderCategory(), mModel.getSubject(), mModel.getSenderName());
            }
        });

    }

    @Override
    public int getItemCount() {
        return idlModel.size();
    }

    private void parseData(final String msgID, final String senderID, final String senderCategory, final String msgSubject, final String senderName) {

        final MaterialDialog dialog = new MaterialDialog.Builder(context).title(context.getResources().getString(R.string.loading))
                .content(context.getResources().getString(R.string.pleaseWait))
                .progress(true, 0)
                .cancelable(false)
                .show();
        APIService mApiService = ApiUtils.getApiService(appSessionManager.getUserDetails().get(ConstantValue.URL));
        mApiService.getReadMailData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERNAME)
                        , appSessionManager.getUserDetails().get(AppSessionManager.KEY_PASSWORD)
                        , appSessionManager.getUserDetails().get(AppSessionManager.KEY_CATEGORY)
                        , msgID
                        , "mail")
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (response.isSuccessful()) {
                            int errData = response.body().get(ConstantValue.api_res_key_error).getAsInt();
                            if (errData == 0) {

                            } else {
                                Toast.makeText(context, "Error!", Toast.LENGTH_SHORT).show();
                            }
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            Log.e("INBOXLIST", "Error :" + response.code());
                            Toast.makeText(context, "Error!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        dialog.dismiss();
                        Log.d("INBOXLIST", "onFailure: " + t.getMessage());
                        Toast.makeText(context, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}