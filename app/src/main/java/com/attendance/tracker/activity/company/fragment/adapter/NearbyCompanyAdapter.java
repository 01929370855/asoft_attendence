package com.attendance.tracker.activity.company.fragment.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.attendance.tracker.R;
import com.attendance.tracker.activity.NewCompanyList.CompanyReportList;
import com.attendance.tracker.activity.company.fragment.NearbyCompanyClickListener;
import com.attendance.tracker.activity.company.message.model.NearbyCompany;
import com.attendance.tracker.activity.company.message.model.NearbyCompanyList;
import com.attendance.tracker.interfaces.OnBlockListener;
import com.attendance.tracker.interfaces.OnUserClickListener;
import com.attendance.tracker.utils.AppSessionManager;

import java.util.ArrayList;

public class NearbyCompanyAdapter extends RecyclerView.Adapter<NearbyCompanyAdapter.ViewHolder> {
    private ArrayList<NearbyCompanyList> mData;
    private Context mActivity;
    private NearbyCompanyClickListener nearbyCompanyClickListener;


    AppSessionManager appSessionManager;

    // RecyclerView recyclerView;
    public NearbyCompanyAdapter(ArrayList<NearbyCompanyList> mData, Context mActivity) {
        this.mData = mData;
        this.mActivity = mActivity;
        appSessionManager = new AppSessionManager(mActivity);
    }

    @Override
    public NearbyCompanyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_near_company_list, parent, false);
        NearbyCompanyAdapter.ViewHolder viewHolder = new NearbyCompanyAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NearbyCompanyAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final NearbyCompanyList todayOfferModel = mData.get(position);

        if (todayOfferModel.getName().equals("")) {
            holder.name.setText("No User Name");

        } else {
            holder.name.setText("Authority:\t\t" + todayOfferModel.getName());

        }
        holder.number1.setText("Mob1:\t\t" + todayOfferModel.getMobile1());
        holder.company.setText("Company:\t\t" + todayOfferModel.getCompany());
        holder.mobile2.setText("Mob2:\t\t" + todayOfferModel.getMobile2());


        holder.mobile2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",  todayOfferModel.getMobile2(), null));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(intent);
            }
        });
        holder.number1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",  todayOfferModel.getMobile1(), null));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(intent);
            }
        });


        holder.imvMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nearbyCompanyClickListener.itemMessageClick(view,position);
            }
        });

        holder.imvFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nearbyCompanyClickListener.itemFbClick(view,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView,imvFacebook,imvMessage;
        public TextView name, number1, employess, company, mobile2, monthlycharge;


        public ViewHolder(View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.nameTV);
            this.number1 = itemView.findViewById(R.id.MobileTV);
            this.employess = itemView.findViewById(R.id.employeesTV);
            this.company = itemView.findViewById(R.id.companyTV);
            this.mobile2 = itemView.findViewById(R.id.number2TV);
            this.monthlycharge = itemView.findViewById(R.id.monthlychargeTV);
            this.imvFacebook = itemView.findViewById(R.id.imvFacebook);
            this.imvMessage = itemView.findViewById(R.id.imvMessage);


        }

    }

    public void SetItemClick(NearbyCompanyClickListener onCategoryItemClick) {
        this.nearbyCompanyClickListener = onCategoryItemClick;
    }




}