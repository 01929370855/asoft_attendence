package com.attendance.tracker.activity.company.message;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.attendance.tracker.R;
import com.attendance.tracker.activity.company.message.model.RecycleViewItemClickListener;
import com.attendance.tracker.activity.company.message.model.SearchDataModel;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> {

    private List<SearchDataModel> searchDataList;
    RecycleViewItemClickListener clickListener;
    Context mContext;

    public SearchAdapter(Context mContext, List<SearchDataModel> searchDataList
            , RecycleViewItemClickListener clickListener) {
        this.mContext = mContext;
        this.searchDataList = searchDataList;
        this.clickListener = clickListener;

    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewUserID;
        TextView textViewUserName;
        TextView textViewUserRank;
        CircleImageView userImage;

        MyViewHolder(View view) {
            super(view);
            textViewUserID = view.findViewById(R.id.search_user_id);
            textViewUserName = view.findViewById(R.id.search_user_name);
            textViewUserRank = view.findViewById(R.id.search_user_rank);
            userImage = view.findViewById(R.id.search_user_image);
        }
    }

    @Override
    public SearchAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_search_view, parent, false);
        return new SearchAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        SearchDataModel mModel = searchDataList.get(position);
        holder.textViewUserID.setText("User: " + mModel.getCompany());
        holder.textViewUserName.setText("Name: " + mModel.getName());
        holder.textViewUserRank.setText("Rank: " + mModel.getMobile1());
        AppSessionManager appSessionManager = new AppSessionManager(mContext);
       // String userImage = appSessionManager.getUserDetails().get(ConstantValue.URL) + mModel.getImage();

        Glide.with(mContext).load(R.drawable.ic_logo).apply(new RequestOptions().placeholder(R.drawable.ic_logo)
                .error(R.drawable.ic_logo)).into(holder.userImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return searchDataList.size();
    }
}
