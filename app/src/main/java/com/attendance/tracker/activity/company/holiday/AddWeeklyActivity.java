package com.attendance.tracker.activity.company.holiday;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.R;
import com.attendance.tracker.activity.company.holiday.adapter.HolidayOtherReportAdapter;
import com.attendance.tracker.activity.company.holiday.data.HolidayList;
import com.attendance.tracker.activity.company.holiday.data.HolidayReportModel;
import com.attendance.tracker.interfaces.OnBlockListener;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddWeeklyActivity extends AppCompatActivity implements OnBlockListener {
    CheckInternetConnection internetConnection;
    AppSessionManager appSessionManager;

    String  type;
    String userId;
    String monthID;
    String yearID;
    String DayID;
    String DayNameID;
    String empID = "00";
    String currentDate, currentMonth, currentYear,currentDayName;
    EditText node;
    LinearLayout lytDay,lytMonth,lytYear,lytEmployee,lytDayName,lytNote;
    //spinner
    String[] Month;
    String[] Years;
    String[] Day;
    RecyclerView rvList;
    HolidayOtherReportAdapter adapterOther;
    ArrayList<HolidayList> holidayLists = new ArrayList<>();
    CardView cv_day;
    TextView tv_day,title;
    boolean[] selectedDay;
    ArrayList<Integer> weekList = new ArrayList<>();
    String[] weekArray = {"Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday","Friday"};
    boolean  callReportApi = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_weekly);
        initVariable();
        initView();
        initFunc();
    }

    private void initVariable() {
        Intent mIntent = getIntent();
        type = mIntent.getStringExtra("hType");
        appSessionManager = new AppSessionManager(this);
        internetConnection = new CheckInternetConnection();
        userId = appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        Log.w("date", "" + formattedDate);
        String[] separated = formattedDate.split("-");

        currentDate = separated[0];
        currentMonth = separated[1]; //
        currentYear = separated[2]; //
        currentDayName = String.valueOf(android.text.format.DateFormat.format("EEE", c));

        selectedDay = new boolean[weekArray.length];



        Years = new String[]{currentYear, "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039",
                "2040", "2041", "2042", "2042"};
        Month = new String[]{currentMonth, "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        Day = new String[]{currentDate,"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18",
                "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30","31"};

    }

    private void initView() {
        Spinner sp_day = findViewById(R.id.sp_day);
        Spinner sp_month = findViewById(R.id.sp_month);
        Spinner sp_year = findViewById(R.id.sp_year);

         title = findViewById(R.id.title);
        title.setText(type+" Holiday");

        cv_day = findViewById(R.id.cv_day);
        tv_day = findViewById(R.id.tv_day);
        lytMonth = findViewById(R.id.lytMonth);
        lytYear = findViewById(R.id.lytYear);
        lytDay = findViewById(R.id.lytDay);
        lytDayName = findViewById(R.id.lytDayName);
        rvList = findViewById(R.id.rvList);
        tv_day.setText("Select Day");
        switch (type) {
            case "Weekly":
                lytDay.setVisibility(View.GONE);
                lytMonth.setVisibility(View.GONE);
                lytYear.setVisibility(View.GONE);
                findViewById(R.id.save).setVisibility(View.GONE);
                callWeeklyApi();
                break;
            case "Yearly":
                lytDayName.setVisibility(View.GONE);
                callYearlyApi();
                break;
        }

            //day Spinner

            sp_day.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    DayID = Day[i];
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            ArrayAdapter ad3 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Day);
            ad3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_day.setAdapter(ad3);

            //month Spinner
            sp_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    monthID = Month[i];
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            ArrayAdapter ad = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Month);
            ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_month.setAdapter(ad);

            //year Spinner

            sp_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    yearID = Years[i];
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            ArrayAdapter ad2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Years);
            ad2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_year.setAdapter(ad2);





            findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    submitData();
                }
            });

            findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            cv_day.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openWeekDialog();
                }
            });

    }

    private void initFunc() {

    }

    private void submitData() {
        callSubmitYearlyApi();
    }

    public void callSubmitWeeklyApi(String DayName) {
        if(DayName.equals("")){
            Toast.makeText(this, "Select a Day", Toast.LENGTH_SHORT).show();
        }else {
            if (internetConnection.isInternetAvailable(AddWeeklyActivity.this)) {
                APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
                final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                        .content(getResources().getString(R.string.pleaseWait))
                        .progress(true, 0)
                        .cancelable(false)
                        .show();
                mApiService.submitWeekly(userId,DayName).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (response.isSuccessful()) {
                            if (response.body().get("error").getAsInt() == 0) {
                                Toast.makeText(AddWeeklyActivity.this, response.body().get("error_report").getAsString(), Toast.LENGTH_SHORT).show();
                                callWeeklyApi();
                            } else if (response.body().get("error").getAsInt() == 1) {
                                Toast.makeText(AddWeeklyActivity.this, response.body().get("error_report").getAsString(), Toast.LENGTH_SHORT).show();
                            }
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        // progressBar.setVisibility(View.GONE);
                        Log.d("LOGIN", "onFailure: " + t.getMessage());
                        dialog.dismiss();
                    }
                });
            } else {
                Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
            }
        }


    }

    public void callSubmitYearlyApi() {
        if(DayID.equals("")){
            DayID = currentDate;
        }else if(monthID.equals("")){
            monthID = currentMonth;
        }else if(yearID.equals("")){
            yearID = currentYear;
        }else {
            if (internetConnection.isInternetAvailable(AddWeeklyActivity.this)) {
                APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
                final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                        .content(getResources().getString(R.string.pleaseWait))
                        .progress(true, 0)
                        .cancelable(false)
                        .show();
                mApiService.submitYearly(userId,DayID,monthID,yearID).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (response.isSuccessful()) {
                            if (response.body().get("error").getAsInt() == 0) {
                                Toast.makeText(AddWeeklyActivity.this, response.body().get("error_report").getAsString(), Toast.LENGTH_SHORT).show();
                                callYearlyApi();
                            } else if (response.body().get("error").getAsInt() == 1) {
                                Toast.makeText(AddWeeklyActivity.this, response.body().get("error_report").getAsString(), Toast.LENGTH_SHORT).show();
                            }

                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        // progressBar.setVisibility(View.GONE);
                        Log.d("LOGIN", "onFailure: " + t.getMessage());
                        dialog.dismiss();
                    }
                });
            } else {
                Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    private void callYearlyApi() {
        if (internetConnection.isInternetAvailable(AddWeeklyActivity.this)) {
            final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getHolidayYearly(userId,"1").enqueue(new Callback<HolidayReportModel>() {
                @Override
                public void onResponse(Call<HolidayReportModel> call, Response<HolidayReportModel> response) {
                    if (response.isSuccessful()) {
                        holidayLists.clear();
                        if (response.body().getError() == 0) {

                            holidayLists.addAll(response.body().getHolidayList());
                            LoadOtherDataList();
                        } else if (response.body().getError() == 1) {
                            Toast.makeText(AddWeeklyActivity.this, "No Data Found.", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<HolidayReportModel> call, Throwable t) {
                    // progressBar.setVisibility(View.GONE);
                    Log.d("LOGIN", "onFailure: " + t.getMessage());
                    dialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void callWeeklyApi() {
        if (internetConnection.isInternetAvailable(AddWeeklyActivity.this)) {
            final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            holidayLists.clear();
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getHolidayweekly(userId,"1").enqueue(new Callback<HolidayReportModel>() {
                @Override
                public void onResponse(Call<HolidayReportModel> call, Response<HolidayReportModel> response) {
                    if (response.isSuccessful()) {

                        if (response.body().getError() == 0) {
                            holidayLists.addAll(response.body().getHolidayList());
                            LoadOtherDataList();
                        } else if (response.body().getError() == 1) {
                            Toast.makeText(AddWeeklyActivity.this, "No Data Found.", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<HolidayReportModel> call, Throwable t) {
                    // progressBar.setVisibility(View.GONE);
                    Log.d("LOGIN", "onFailure: " + t.getMessage());
                    dialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }

    }

    private void LoadOtherDataList() {
        adapterOther = new HolidayOtherReportAdapter(holidayLists,getApplicationContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvList.setLayoutManager(layoutManager);
        rvList.setHasFixedSize(true);
        rvList.setAdapter(adapterOther);
        adapterOther.notifyDataSetChanged();
        adapterOther.setClickListener(this);
    }

    public void openWeekDialog() {
        // Initialize alert dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // set title
        builder.setTitle("Select Day");

        // set dialog non cancelable
        builder.setCancelable(false);

        builder.setMultiChoiceItems(weekArray, selectedDay, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                // check condition
                if (b) {
                    // when checkbox selected
                    // Add position  in lang list
                    weekList.add(i);
                    // Sort array list
                    Collections.sort(weekList);
                } else {
                    // when checkbox unselected
                    // Remove position from langList
                    weekList.remove(Integer.valueOf(i));
                }
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Initialize string builder
                StringBuilder stringBuilder = new StringBuilder();
                // use for loop
                for (int j = 0; j < weekList.size(); j++) {
                    // concat array value
                    stringBuilder.append(weekArray[weekList.get(j)]);


                    String day = weekArray[weekList.get(j)];
                    String shortday = String.valueOf(day.subSequence(0,3));

                   // Toast.makeText(AddWeeklyActivity.this, ""+shortday, Toast.LENGTH_SHORT).show();

                    callSubmitWeeklyApi(shortday);

                    // check condition
                    if (j != weekList.size() - 1) {
                        // When j value  not equal
                        // to lang list size - 1
                        // add comma
                      //  stringBuilder.append(", ");
                     //   Toast.makeText(AddWeeklyActivity.this, "sumitdone", Toast.LENGTH_SHORT).show();
                       //
                    }
                }
                // set text on textView
                //tv_day.setText(stringBuilder.toString());
                tv_day.setText("Select Day");
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // dismiss dialog
                dialogInterface.dismiss();
            }
        });
        builder.setNeutralButton("Clear All", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // use for loop
                for (int j = 0; j < selectedDay.length; j++) {
                    // remove all selection
                    selectedDay[j] = false;
                    // clear language list
                    weekList.clear();
                    // clear text view value
                    tv_day.setText("Select Day");
                }
            }
        });
        // show dialog
        builder.show();
    }


    @Override
    public void itemUserBlockClick(View view, int position) {

        callDeleteApi(holidayLists.get(position).getHolidayId());
    }

    private void callDeleteApi(String holiday) {
        if (internetConnection.isInternetAvailable(AddWeeklyActivity.this)) {
            final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.deleteHoliday(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERNAME),appSessionManager.getUserDetails().get(AppSessionManager.KEY_PASSWORD),
                    holiday).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.isSuccessful()) {
                        if (response.body().get("error").getAsInt() == 0){
                            Toast.makeText(AddWeeklyActivity.this, ""+response.body().get("error_report").getAsString(), Toast.LENGTH_SHORT).show();
                            callWeeklyApi();
                        }
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    // progressBar.setVisibility(View.GONE);
                    Log.d("LOGIN", "onFailure: " + t.getMessage());
                    dialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }
}