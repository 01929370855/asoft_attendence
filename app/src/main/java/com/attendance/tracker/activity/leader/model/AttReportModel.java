package com.attendance.tracker.activity.leader.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AttReportModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("error_report")
    @Expose
    private String errorReport;
    @SerializedName("total_present")
    @Expose
    private Integer totalPresent;
    @SerializedName("total_absent")
    @Expose
    private Integer totalAbsent;
    @SerializedName("date_list")
    @Expose
    private ArrayList<String> dateList = null;
    @SerializedName("report")
    @Expose
    private ArrayList<AttReportModelList> report = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getErrorReport() {
        return errorReport;
    }

    public void setErrorReport(String errorReport) {
        this.errorReport = errorReport;
    }

    public Integer getTotalPresent() {
        return totalPresent;
    }

    public void setTotalPresent(Integer totalPresent) {
        this.totalPresent = totalPresent;
    }

    public Integer getTotalAbsent() {
        return totalAbsent;
    }

    public void setTotalAbsent(Integer totalAbsent) {
        this.totalAbsent = totalAbsent;
    }

    public ArrayList<String> getDateList() {
        return dateList;
    }

    public void setDateList(ArrayList<String> dateList) {
        this.dateList = dateList;
    }

    public ArrayList<AttReportModelList> getReport() {
        return report;
    }

    public void setReport(ArrayList<AttReportModelList> report) {
        this.report = report;
    }
}
