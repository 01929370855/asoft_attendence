package com.attendance.tracker.activity.leader.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttReportModelList {
    @SerializedName("Day")
    @Expose
    private String day;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Inn")
    @Expose
    private String inn;
    @SerializedName("Out")
    @Expose
    private String out;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getOut() {
        return out;
    }

    public void setOut(String out) {
        this.out = out;
    }

}
