package com.attendance.tracker.activity.company.holiday.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HolidayList {

    @SerializedName("SL")
    @Expose
    private String sl;
    @SerializedName("holiday_id")
    @Expose
    private String holidayId;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Post_Time")
    @Expose
    private String postTime;

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getHolidayId() {
        return holidayId;
    }

    public void setHolidayId(String holidayId) {
        this.holidayId = holidayId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPostTime() {
        return postTime;
    }

    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }
}
