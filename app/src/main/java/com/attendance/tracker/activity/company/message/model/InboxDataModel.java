package com.attendance.tracker.activity.company.message.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InboxDataModel {
    @SerializedName("error")
    private Integer error;
    @SerializedName("error_report")
    private String errorReport;
    @SerializedName("inbox")
    private List<InboxDataListModel> inbox = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getErrorReport() {
        return errorReport;
    }

    public void setErrorReport(String errorReport) {
        this.errorReport = errorReport;
    }

    public List<InboxDataListModel> getInbox() {
        return inbox;
    }

    public void setInbox(List<InboxDataListModel> inbox) {
        this.inbox = inbox;
    }
}
