package com.attendance.tracker.activity.company;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.cursoradapter.widget.CursorAdapter;
import androidx.cursoradapter.widget.SimpleCursorAdapter;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.SearchManager;
import android.content.Intent;
import android.database.MatrixCursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.attendance.tracker.R;
import com.attendance.tracker.activity.company.message.SearchAdapter;
import com.attendance.tracker.activity.company.message.model.RecycleViewItemClickListener;
import com.attendance.tracker.activity.company.message.model.SearchDataModel;
import com.attendance.tracker.activity.company.message.model.SearchModel;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchUserMailActivity extends AppCompatActivity implements RecycleViewItemClickListener {
    AppSessionManager appSessionManager;
    CheckInternetConnection checkInternetConnection;

    SearchView searchView;
    List<String> suggestionsList;
    CursorAdapter suggestionAdapter;


    Spinner searchCatagorySpinner;
    String selectedUserCatagory = "Employee";

    SearchAdapter mSearchAdapter;
    RecyclerView recyclerView;
    List<SearchDataModel> finalSearchList = new ArrayList<>();

    List<String> arrList = new ArrayList<>();

    Intent getData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user_mail);
        searchView = findViewById(R.id.search_view);
        searchCatagorySpinner = findViewById(R.id.search_catagory_spinner);
        recyclerView = findViewById(R.id.search_list_recyclerview);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        Toolbar myToolbar = findViewById(R.id.search_toolbar);
        setSupportActionBar(myToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Search Company");
        }

        appSessionManager = new AppSessionManager(SearchUserMailActivity.this);
        getData = getIntent();
        int formType = getData.getIntExtra("backResult", 0);
        createSearchCatagorySpinner(formType);
        createSearchView();
        setResltListView();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    void createSearchCatagorySpinner(int formTypes) {
        if (formTypes == 100){
            arrList.add("Member");
        } else {
            arrList.add("Admin");
            arrList.add("Agent");
            arrList.add("Member");
        }
        ArrayAdapter spinnerAdapter = new ArrayAdapter(this, R.layout.row_spinner_search, arrList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        searchCatagorySpinner.setAdapter(spinnerAdapter);
        searchCatagorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("selected user type ", "" + arrList.get(i));
                selectedUserCatagory = arrList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    void createSearchView() {
        EditText searchEditText = (EditText) searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.white));
        searchEditText.setHintTextColor(getResources().getColor(R.color.white));

        suggestionAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_1,
                null,
                new String[]{SearchManager.SUGGEST_COLUMN_TEXT_1},
                new int[]{android.R.id.text1},
                0);
        suggestionsList = new ArrayList<>();
        searchView.setSuggestionsAdapter(suggestionAdapter);

        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                searchView.setQuery(suggestionsList.get(position), false);
                searchView.clearFocus();
                Log.e("suggestionClicked", ": " + suggestionsList.get(position));
                getSearchList(suggestionsList.get(position), true);
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                getSearchList(newText, true);
                return false;
            }
        });

        View closeButton = searchView.findViewById(androidx.appcompat.R.id.search_close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText et = (EditText) findViewById(R.id.search_src_text);
                et.setText("");
                finalSearchList.clear();
                mSearchAdapter.notifyDataSetChanged();
            }
        });
    }

    void getSearchList(String suggestionStr, final Boolean suggestoinClicked) {

        final APIService mService = ApiUtils.getApiService(ConstantValue.URL);
        mService.searchUser(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID), suggestionStr)
                .enqueue(new Callback<SearchModel>() {

                    @Override
                    public void onResponse(Call<SearchModel> call, Response<SearchModel> response) {
                        if (response.isSuccessful()) {
                            suggestionsList.clear();
                            for (SearchDataModel mOdel : response.body().getReport()) {
                                String str = mOdel.getName();
                                suggestionsList.add(str);
                            }
                            if (suggestoinClicked) {
                                finalSearchList.clear();
                                finalSearchList.addAll(response.body().getReport());
                                mSearchAdapter.notifyDataSetChanged();
                                return;
                            }
                            String[] columns = {
                                    BaseColumns._ID,
                                    SearchManager.SUGGEST_COLUMN_TEXT_1,
                                    SearchManager.SUGGEST_COLUMN_INTENT_DATA
                            };

                            MatrixCursor cursor = new MatrixCursor(columns);

                            for (int i = 0; i < suggestionsList.size(); i++) {
                                String[] tmp = {Integer.toString(i), suggestionsList.get(i), suggestionsList.get(i)};
                                cursor.addRow(tmp);
                            }
                            suggestionAdapter.swapCursor(cursor);
                        } else {
                            int statusCode = response.code();
                            Log.e("fail ", "fail to download by retrofit");
                        }
                    }

                    @Override
                    public void onFailure(Call<SearchModel> call, Throwable t) {
                        Log.e("error ", "error to download by retrofit");
                    }
                });
    }

    void setResltListView() {
        mSearchAdapter = new SearchAdapter(this, finalSearchList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mSearchAdapter);
    }

    @Override
    public void onItemClick(int position) {
        SearchDataModel sData = finalSearchList.get(position);
        Intent output = new Intent();
        output.putExtra("name", sData.getName());
        output.putExtra("category", "member");
        output.putExtra("id", sData.getId());
        setResult(RESULT_OK, output);
        finish();

//        if (null == getCallingActivity()) {
//            Intent intent = new Intent(this, SearchDetailsActivity.class);
//            Bundle bundle = new Bundle();
//            bundle.putParcelable("search_result", Parcels.wrap(sData));
//            intent.putExtra("PNAME", "SearchActivity");
//            intent.putExtras(bundle);
//            startActivity(intent);
//        } else {
//            Intent output = new Intent();
//            output.putExtra("name", sData.getName());
//            output.putExtra("category", "member");
//            output.putExtra("id", sData.getId());
//            setResult(RESULT_OK, output);
//            finish();
//        }
    }
}
