package com.attendance.tracker.activity.company.message;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.attendance.tracker.R;
import com.attendance.tracker.activity.company.message.model.CallFromMessageList;
import com.attendance.tracker.activity.company.message.model.OutboxDataListModel;

import java.util.List;

public class OutboxMessageAdapter extends RecyclerView.Adapter<OutboxMessageAdapter.MyViewHolder> {

    private Context context;
    private List<OutboxDataListModel> obdlModel;
    private CallFromMessageList callFromMessageList;

    public OutboxMessageAdapter(Context context, List<OutboxDataListModel> obdlModel, CallFromMessageList callFromMessageList) {
        this.context = context;
        this.obdlModel = obdlModel;
        this.callFromMessageList = callFromMessageList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textViewSenderName;
        TextView textViewSendingTime;
        TextView textViewMessageBody;

        public MyViewHolder(View itemView) {
            super(itemView);
           // ButterKnife.bind(this, itemView);

            textViewSenderName = itemView.findViewById(R.id.messege_sender);
            textViewSendingTime = itemView.findViewById(R.id.messege_send_time);
            textViewMessageBody = itemView.findViewById(R.id.messege_content);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_outbox_messagelist, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final OutboxDataListModel outboxDataListModel = obdlModel.get(position);
        holder.textViewSenderName.setText(outboxDataListModel.getReceiverName());
        holder.textViewSendingTime.setText(outboxDataListModel.getTime());
        holder.textViewMessageBody.setText(outboxDataListModel.getSubject());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callFromMessageList.onItemClickListner(outboxDataListModel.getMailId()
                        , outboxDataListModel.getReceiverId(), outboxDataListModel.getReceiverCategory()
                        , outboxDataListModel.getSubject(), outboxDataListModel.getReceiverName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return obdlModel.size();
    }
}

