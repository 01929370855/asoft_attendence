package com.attendance.tracker.activity.company.holiday.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.attendance.tracker.R;
import com.attendance.tracker.activity.company.holiday.data.HolidayPersonalList;
import com.attendance.tracker.interfaces.OnBlockListener;
import com.attendance.tracker.utils.AppSessionManager;

import java.util.ArrayList;

public class HolidayPersonalAdapter extends RecyclerView.Adapter<HolidayPersonalAdapter.ViewHolder> {
    private ArrayList<HolidayPersonalList> mData;
    private Context mActivity;
    AppSessionManager appSessionManager;
    OnBlockListener blockListener;

    // RecyclerView recyclerView;
    public HolidayPersonalAdapter(ArrayList<HolidayPersonalList> mData, Context mActivity) {
        this.mData = mData;
        this.mActivity = mActivity;
        appSessionManager = new AppSessionManager(mActivity);
    }

    @Override
    public HolidayPersonalAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_holiday_personal, parent, false);
        HolidayPersonalAdapter.ViewHolder viewHolder = new HolidayPersonalAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HolidayPersonalAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final HolidayPersonalList todayOfferModel = mData.get(position);


        holder.sl.setText("" + todayOfferModel.getSl());
        holder.name.setText("Day:\t\t" + todayOfferModel.getTitle());
        holder.mobile.setText("Reason:\t\t" + todayOfferModel.getReason());
        holder.join.setText("Date:\t\t" + todayOfferModel.getPostTime());

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blockListener.itemUserBlockClick(v,position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name,mobile,join,sl;
        public ImageView remove;


        public ViewHolder(View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.nameTV);
            this.mobile = itemView.findViewById(R.id.mobileTV);
            this.join = itemView.findViewById(R.id.joindateTV);
            this.sl = itemView.findViewById(R.id.sl);
            this.remove = itemView.findViewById(R.id.remove);




        }

    }

    public void setClickListener( OnBlockListener blockListener){
        this.blockListener = blockListener;

    }



}