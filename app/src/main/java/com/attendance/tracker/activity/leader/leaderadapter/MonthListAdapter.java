package com.attendance.tracker.activity.leader.leaderadapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.attendance.tracker.R;
import com.attendance.tracker.data.DatewiseGroupReportList;
import com.attendance.tracker.data.MonthlyReportList;
import com.attendance.tracker.interfaces.OnUserClickListener;
import com.attendance.tracker.utils.AppSessionManager;

import java.util.ArrayList;

public class MonthListAdapter extends RecyclerView.Adapter<MonthListAdapter.ViewHolder> {
    private ArrayList<MonthlyReportList> mData;
    private Context mActivity;
    OnUserClickListener onCategoryItemClick;


    AppSessionManager appSessionManager;

    // RecyclerView recyclerView;
    public MonthListAdapter(ArrayList<MonthlyReportList> mData, Context mActivity) {
        this.mData = mData;
        this.mActivity = mActivity;
        appSessionManager = new AppSessionManager(mActivity);
    }

    @Override
    public MonthListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_month_list, parent, false);
        MonthListAdapter.ViewHolder viewHolder = new MonthListAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MonthListAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final MonthlyReportList todayOfferModel = mData.get(position);


        holder.employeename.setText("Name:\t\t" + todayOfferModel.getName());
        holder.status.setText("Status:\t\t" + todayOfferModel.getStatus());
        holder.intime.setText("Present:\t\t" + todayOfferModel.getPresent());
        holder.outtime.setText("Absent:\t\t" + todayOfferModel.getAbsent());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCategoryItemClick.itemUserClick(view,position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView employeename,intime,outtime,status;



        public ViewHolder(View itemView) {
            super(itemView);
            this.employeename = itemView.findViewById(R.id.employenameTV);
            this.intime = itemView.findViewById(R.id.presentTV);
            this.outtime = itemView.findViewById(R.id.absentTV);
            this.status = itemView.findViewById(R.id.satusTV);




        }

    }
    public void SetItemClick(OnUserClickListener onCategoryItemClick) {
        this.onCategoryItemClick = onCategoryItemClick;
    }



}