package com.attendance.tracker.activity.company.fragment;

import android.view.View;

public interface NearbyCompanyClickListener {
    void itemMessageClick(View view, int position);
    void itemFbClick(View view, int position);


}
