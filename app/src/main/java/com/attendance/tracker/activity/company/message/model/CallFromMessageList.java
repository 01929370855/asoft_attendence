package com.attendance.tracker.activity.company.message.model;

public interface CallFromMessageList {
    public void onItemClickListner(String messageID, String receiverId, String receiverCategory, String mailSubject, String receiverName);

}
