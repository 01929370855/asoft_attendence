package com.attendance.tracker.activity.company.message.model;

public interface RecycleViewItemClickListener {
    public void onItemClick(int position);
}
