package com.attendance.tracker.activity.company.message;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.R;
import com.attendance.tracker.activity.company.SearchActivity;
import com.attendance.tracker.activity.company.SearchUserMailActivity;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.attendance.tracker.utils.TransientDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComposeMailActivity extends AppCompatActivity implements View.OnClickListener {
    AppSessionManager appSessionManager;
    CheckInternetConnection checkInternetConnection;
    TransientDialog transientDialog;

    TextView textViewSendTo;
    Button buttonAdminList;
    ImageView imageViewSendTo;
    EditText editTextMailSubject;
    EditText editTextMailBody;

    String actionSelect = "";
    String receiverId = "";
    String receiverCategory = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose_mail);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.toolbar_color));
        }
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar_composeMain);
        setSupportActionBar(myToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Compose Mail");
        }

        initView();

        appSessionManager = new AppSessionManager(ComposeMailActivity.this);
        checkInternetConnection = new CheckInternetConnection();
        transientDialog = new TransientDialog(ComposeMailActivity.this);
        textViewSendTo.setOnClickListener(this);
        imageViewSendTo.setOnClickListener(this);
//        buttonAdminList.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        actionSelect = getIntent().getStringExtra("ACTIONIDENTY");
        if (actionSelect.equals("MAILFORWARD")) {
            editTextMailSubject.setText(bundle.getString("MAILSUBJECT"));
            editTextMailBody.setText(getIntent().getStringExtra("MAILBODY"));
        } else if (actionSelect.equals("MAILREPLY")) {
            receiverId = bundle.getString("RECEIVERID");
            receiverCategory = bundle.getString("RECEIVERCATEGORY");
            textViewSendTo.setText(bundle.getString("SENDERNAME"));
            editTextMailSubject.setText(bundle.getString("MAILSUBJECT"));
        } else if (actionSelect.equals("search_result")) {
            receiverId = bundle.getString("RECEIVERID");
            receiverCategory = bundle.getString("RECEIVERCATEGORY");
            textViewSendTo.setText(bundle.getString("SENDERNAME"));
        } else if (actionSelect.equals("ADMINLIST")){
            receiverId = bundle.getString("RECEIVERID");
            receiverCategory = bundle.getString("RECEIVERCATEGORY");
            textViewSendTo.setText(bundle.getString("RECEIVERNAME"));
        } else if (actionSelect.equals("direct")){
            receiverId = bundle.getString("RECEIVERID");
            receiverCategory = bundle.getString("RECEIVERCATEGORY");
            textViewSendTo.setText(bundle.getString("RECEIVERNAME"));
        }
        // Log.d("COMPOSEMAIL", getIntent().getStringExtra("ACTIONIDENTY") + " // " + bundle.getString("MESSAGEID"));
    }

    private void initView() {
        textViewSendTo = findViewById(R.id.txt_composeMail_SendTo);
        buttonAdminList = findViewById(R.id.btn_composeMail_AdminList);
        imageViewSendTo = findViewById(R.id.img_composeMail_SendTo);
        editTextMailSubject = findViewById(R.id.et_composeMail_Subject);
        editTextMailBody = findViewById(R.id.et_composeMail_body);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_composeMail_SendTo:
            case R.id.img_composeMail_SendTo:
                Intent goSearchListIntent = new Intent(this, SearchUserMailActivity.class);
                goSearchListIntent.putExtra("backResult", 0);
                startActivityForResult(goSearchListIntent, 420);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 420) {
            if (resultCode == RESULT_OK) {
                receiverId = data.getStringExtra("id");
                receiverCategory = data.getStringExtra("category");
                textViewSendTo.setText(data.getStringExtra("name"));

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.compose_mail_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ComposeMailActivity.this.finish();
                return true;
            case R.id.action_send:
                if (authenticateMail()) {
                    sendMailToUser(receiverId, receiverCategory, editTextMailSubject.getText().toString().trim(), editTextMailBody.getText().toString().trim());
                }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            ComposeMailActivity.this.finish();
            return true;
        }
        return false;
    }


    void sendMailToUser(String rcvrID, String rcvrCategory, String mailSub, String mailBody) {
        if (checkInternetConnection.isInternetAvailable(ComposeMailActivity.this)) {
            final MaterialDialog dialog = new MaterialDialog.Builder(this)
                    .title("Sending ....")
                    .content("Please Wait")
                    .progress(true, 0)
                    .show();

            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getMailSendData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERNAME)
                            , appSessionManager.getUserDetails().get(AppSessionManager.KEY_PASSWORD)
                            , appSessionManager.getUserDetails().get(AppSessionManager.KEY_CATEGORY)
                            , rcvrID
                            , rcvrCategory
                            , mailSub
                            , mailBody)
                    .enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            if (response.isSuccessful()) {
                                dialog.dismiss();
                                String mReport = response.body().get(ConstantValue.api_res_key_error_report).getAsString().toLowerCase();
                                if (mReport.equals("successful")) {
                                    transientDialog.showTransientDialogWithOutAction("Success...", "Message sent Successfully..");

                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            ComposeMailActivity.this.finish();
                                        }
                                    }, 2500);
                                }
                            } else {
                                dialog.dismiss();
                                Log.e("COMPOSEMAIL", "Error :" + response.code());
                                Toast.makeText(ComposeMailActivity.this, "Error!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            dialog.dismiss();
                            Log.d("COMPOSEMAIL", "onFailure: " + t.getMessage());
                            Toast.makeText(ComposeMailActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            Snackbar.make(getWindow().getDecorView().getRootView(), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }

    Boolean authenticateMail() {
        if (textViewSendTo.getText().toString().trim().isEmpty()) {
            transientDialog.showTransientDialogWithOutAction("Error...", "Please Select Receiver");
            return false;
        }
        if (editTextMailSubject.getText().toString().trim().isEmpty()) {
            transientDialog.showTransientDialogWithOutAction("Error...", "Message Subject is Empty");
            return false;
        }
        if (editTextMailBody.getText().toString().isEmpty()) {
            transientDialog.showTransientDialogWithOutAction("Error...", "Message Body is Empty");
            return false;
        }
        return true;
    }

}
