package com.attendance.tracker.activity.company.holiday;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.R;
import com.attendance.tracker.data.ProfileData;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddHolidayActivity extends AppCompatActivity {
    CheckInternetConnection internetConnection;
    AppSessionManager appSessionManager;

    String  type;
    String monthID;
    String yearID;
    String DayID;
    String DayNameID;
    String empID = "00";
    String currentDate, currentMonth, currentYear,currentDayName;
    EditText node;
    LinearLayout lytDay,lytMonth,lytYear,lytEmployee,lytDayName,lytNote;
    //spinner
    String[] Month;
    String[] Years;
    String[] Day;
    String[] WeekDay;
    List<String> employeeName = new ArrayList<>();
    List<String> eID = new ArrayList<>();
    String userId = "";
    TextView title;
    Spinner sp_employee;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_holiday);

        initVariable();
        initListener();

    }

    private void initVariable() {
        Intent mIntent = getIntent();
        type = mIntent.getStringExtra("hType");
        appSessionManager = new AppSessionManager(this);
        internetConnection = new CheckInternetConnection();
        userId = appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID);
        //start datepicker
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        Log.w("date", "" + formattedDate);
        String[] separated = formattedDate.split("-");

        currentDate = separated[0];
        currentMonth = separated[1]; //
        currentYear = separated[2]; //
        currentDayName = String.valueOf(android.text.format.DateFormat.format("EEE", c));

        Years = new String[]{currentYear, "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039",
                "2040", "2041", "2042", "2042"};
        Month = new String[]{currentMonth, "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        Day = new String[]{currentDate,"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18",
                "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30","31"};

        WeekDay = new String[]{currentDayName, "Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
        getProfileData(userId,"0","");
    }


    private void initListener() {
        Spinner sp_day = findViewById(R.id.sp_day);
        Spinner sp_month = findViewById(R.id.sp_month);
        Spinner sp_year = findViewById(R.id.sp_year);

        Spinner sp_DayName = findViewById(R.id.sp_DayName);
        sp_employee = findViewById(R.id.sp_employee);

        title = findViewById(R.id.title);
        title.setText("Add "+type+" Holiday");
        node = findViewById(R.id.node);
        lytDay = findViewById(R.id.lytDay);
        lytMonth = findViewById(R.id.lytMonth);
        lytYear = findViewById(R.id.lytYear);
        lytEmployee = findViewById(R.id.lytEmployee);
        lytDayName = findViewById(R.id.lytDayName);
        lytNote = findViewById(R.id.lytNote);

        switch (type){
            case  "Personal":
                lytDayName.setVisibility(View.GONE);
                break;
            case "Weekly":
                lytDay.setVisibility(View.GONE);
                lytMonth.setVisibility(View.GONE);
                lytYear.setVisibility(View.GONE);
                lytEmployee.setVisibility(View.GONE);
                lytNote.setVisibility(View.GONE);
                break;
            case "Yearly":
                lytEmployee.setVisibility(View.GONE);
                lytNote.setVisibility(View.GONE);
                lytDayName.setVisibility(View.GONE);
                break;
        }


        //day Spinner

        sp_day.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                DayID = Day[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ArrayAdapter ad3 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Day);
        ad3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_day.setAdapter(ad3);

        //month Spinner
        sp_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                monthID = Month[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ArrayAdapter ad = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Month);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_month.setAdapter(ad);

        //year Spinner

        sp_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                yearID = Years[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ArrayAdapter ad2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Years);
        ad2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_year.setAdapter(ad2);



        //Day Name Spinner

        sp_DayName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                DayNameID = WeekDay[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ArrayAdapter ad4 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, WeekDay);
        ad4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_DayName.setAdapter(ad4);

        //employee Spinner

        sp_employee.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                empID = eID.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });




        findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               submitData();
            }
        });

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void submitData() {
        switch (type){
            case  "Personal":
                callPersonalApi();
                break;
            case "Weekly":
                callWeeklyApi();
                break;
            case "Yearly":
                callYearlyApi();
                break;
        }
    }

    public void getProfileData(String userId, String type,String search) {
        if (internetConnection.isInternetAvailable(AddHolidayActivity.this)) {
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getProfileList(userId, type,search).enqueue(new Callback<ProfileData>() {
                @Override
                public void onResponse(Call<ProfileData> call, Response<ProfileData> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getError() == 0) {
                            employeeName.add("Select Employee");
                            eID.add("00");
                           for (int i = 0;i < response.body().getReport().size(); i++){
                               employeeName.add(response.body().getReport().get(i).getName());
                               eID.add(response.body().getReport().get(i).getId());
                               loadData();
                           }
                        } else if (response.body().getError() == 1) {
                            Toast.makeText(AddHolidayActivity.this, "No Data Found.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ProfileData> call, Throwable t) {
                   // progressBar.setVisibility(View.GONE);
                    Log.d("LOGIN", "onFailure: " + t.getMessage());
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void loadData() {
        ArrayAdapter ad5 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, employeeName);
        ad5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_employee.setAdapter(ad5);
    }


    public void callWeeklyApi() {
        if(DayNameID.equals("")){
            DayNameID= currentDayName;
        }else {
            if (internetConnection.isInternetAvailable(AddHolidayActivity.this)) {
                APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
                final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                        .content(getResources().getString(R.string.pleaseWait))
                        .progress(true, 0)
                        .cancelable(false)
                        .show();
                mApiService.submitWeekly(userId,DayNameID).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (response.isSuccessful()) {
                            if (response.body().get("error").getAsInt() == 0) {
                                Toast.makeText(AddHolidayActivity.this, response.body().get("error_report").getAsString(), Toast.LENGTH_SHORT).show();
                            } else if (response.body().get("error").getAsInt() == 1) {
                                Toast.makeText(AddHolidayActivity.this, response.body().get("error_report").getAsString(), Toast.LENGTH_SHORT).show();
                            }
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        // progressBar.setVisibility(View.GONE);
                        Log.d("LOGIN", "onFailure: " + t.getMessage());
                        dialog.dismiss();
                    }
                });
            } else {
                Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
            }
        }


    }

    public void callYearlyApi() {
        if(DayID.equals("")){
            DayID = currentDate;
        }else if(monthID.equals("")){
            monthID = currentMonth;
        }else if(yearID.equals("")){
            yearID = currentYear;
        }else {
            if (internetConnection.isInternetAvailable(AddHolidayActivity.this)) {
                APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
                final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                        .content(getResources().getString(R.string.pleaseWait))
                        .progress(true, 0)
                        .cancelable(false)
                        .show();
                mApiService.submitYearly(userId,DayID,monthID,yearID).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (response.isSuccessful()) {
                            if (response.body().get("error").getAsInt() == 0) {
                                Toast.makeText(AddHolidayActivity.this, response.body().get("error_report").getAsString(), Toast.LENGTH_SHORT).show();
                            } else if (response.body().get("error").getAsInt() == 1) {
                                Toast.makeText(AddHolidayActivity.this, response.body().get("error_report").getAsString(), Toast.LENGTH_SHORT).show();
                            }

                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        // progressBar.setVisibility(View.GONE);
                        Log.d("LOGIN", "onFailure: " + t.getMessage());
                        dialog.dismiss();
                    }
                });
            } else {
                Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
            }
        }
    }
    public void callPersonalApi() {
        if(DayID.equals("")){
            DayID = currentDate;
        }else if(monthID.equals("")){
            monthID = currentMonth;
        }else if(yearID.equals("")){
            yearID = currentYear;
        }else if(empID.equals("00")){
            Toast.makeText(this, "Select Employee", Toast.LENGTH_SHORT).show();
        }else if(node.getText().toString().equals("")){
            Toast.makeText(this, "Enter Note", Toast.LENGTH_SHORT).show();
        }else {
            String hints = node.getText().toString();
            if (internetConnection.isInternetAvailable(AddHolidayActivity.this)) {
                APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
                final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                        .content(getResources().getString(R.string.pleaseWait))
                        .progress(true, 0)
                        .cancelable(false)
                        .show();
                mApiService.submitPersonal(userId,DayID,monthID,yearID,hints,empID).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (response.isSuccessful()) {
                            if (response.body().get("error").getAsInt() == 0) {

                                Toast.makeText(AddHolidayActivity.this, response.body().get("error_report").getAsString(), Toast.LENGTH_SHORT).show();
                            } else if (response.body().get("error").getAsInt() == 1) {
                                Toast.makeText(AddHolidayActivity.this, response.body().get("error_report").getAsString(), Toast.LENGTH_SHORT).show();
                            }

                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        // progressBar.setVisibility(View.GONE);
                        Log.d("LOGIN", "onFailure: " + t.getMessage());
                        dialog.dismiss();
                    }
                });
            } else {
                Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
            }
        }


    }

}