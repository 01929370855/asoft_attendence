package com.attendance.tracker.activity.company.message;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.attendance.tracker.R;
import com.attendance.tracker.activity.company.message.fragment.InboxFragment;
import com.attendance.tracker.activity.company.message.fragment.OutboxFragment;
import com.attendance.tracker.utils.AppSessionManager;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class MessageActivity extends AppCompatActivity {
    AppSessionManager appSessionManager;

    private View mView;
    ViewPager viewPager;
    TabLayout tabs;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        viewPager=findViewById(R.id.message_viewpager);
        tabs=findViewById(R.id.message_tabs);
        appSessionManager = new AppSessionManager(getApplicationContext());
        Log.e("UserCategory", appSessionManager.getUserDetails().get(AppSessionManager.KEY_CATEGORY));
        setupViewPager(viewPager);
        tabs.setupWithViewPager(viewPager);


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {

        MessagePagerAdapter adapter = new MessagePagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new InboxFragment(), " Inbox ");
        adapter.addFragment(new OutboxFragment(), " Outbox ");
        viewPager.setAdapter(adapter);

    }


    static class MessagePagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public MessagePagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
