package com.attendance.tracker.activity.notice.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.R;
import com.attendance.tracker.activity.company.holiday.AddWeeklyActivity;
import com.attendance.tracker.activity.company.holiday.adapter.HolidayOtherReportAdapter;
import com.attendance.tracker.activity.company.holiday.data.HolidayReportModel;
import com.attendance.tracker.activity.notice.adapter.NoticeAdapter;
import com.attendance.tracker.activity.notice.model.NoticeList;
import com.attendance.tracker.activity.notice.model.NoticeModel;
import com.attendance.tracker.interfaces.OnBlockListener;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NoticeActivity extends AppCompatActivity implements OnBlockListener {
    AppSessionManager appSessionManager;
    CheckInternetConnection internetConnection;
    RecyclerView rvNoticeList;
    NoticeAdapter noticeAdapter;
    ArrayList<NoticeList> noticeLists = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);
        initVariable();
        initView();
        initFunc();
        initListener();

    }



    private void initVariable() {
        internetConnection = new CheckInternetConnection();
        appSessionManager  = new AppSessionManager(this);
    }

    private void initView() {
        rvNoticeList = findViewById(R.id.rv_notificationList);
    }

    private void initFunc() {
        callNoticeApi(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID));
       // callNoticeApi("96");
    }
    private void initListener() {
        findViewById(R.id.back).setOnClickListener(v -> finish());
    }


    private void callNoticeApi(String userId) {
        if (internetConnection.isInternetAvailable(NoticeActivity.this)) {
            final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            noticeLists.clear();
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getNoticeList(userId).enqueue(new Callback<NoticeModel>() {
                @Override
                public void onResponse(Call<NoticeModel> call, Response<NoticeModel> response) {
                    if (response.isSuccessful()) {

                        if (response.body().getError() == 0) {
                            noticeLists.addAll(response.body().getNoticeList());
                            LoadOtherDataList();
                        } else if (response.body().getError() == 1) {
                            Toast.makeText(NoticeActivity.this, "No Data Found.", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<NoticeModel> call, Throwable t) {
                    // progressBar.setVisibility(View.GONE);
                    Log.d("LOGIN", "onFailure: " + t.getMessage());
                    dialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }

    }

    private void LoadOtherDataList() {
        noticeAdapter = new NoticeAdapter(noticeLists,getApplicationContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvNoticeList.setLayoutManager(layoutManager);
        rvNoticeList.setHasFixedSize(true);
        rvNoticeList.setAdapter(noticeAdapter);
        noticeAdapter.notifyDataSetChanged();
        noticeAdapter.setClickListener(this);
    }

    @Override
    public void itemUserBlockClick(View view, int position) {
        Intent mIntent = new Intent(this,NoticeDetailsActivity.class);
        mIntent.putExtra("noticeID",noticeLists.get(position).getNoticeId());
        startActivity(mIntent);
    }
}