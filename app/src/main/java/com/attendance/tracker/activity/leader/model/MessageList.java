package com.attendance.tracker.activity.leader.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MessageList {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("error_report")
    @Expose
    private String errorReport;
    @SerializedName("task_reply_list")
    @Expose
    private ArrayList<ReplyList> task_reply_list = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getErrorReport() {
        return errorReport;
    }

    public void setErrorReport(String errorReport) {
        this.errorReport = errorReport;
    }

    public ArrayList<ReplyList> getTask_reply_list() {
        return task_reply_list;
    }

    public void setTask_reply_list(ArrayList<ReplyList> task_reply_list) {
        this.task_reply_list = task_reply_list;
    }
}
