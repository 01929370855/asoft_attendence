package com.attendance.tracker.activity.company.holiday;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.R;
import com.attendance.tracker.activity.company.holiday.adapter.HolidayPersonalAdapter;
import com.attendance.tracker.activity.company.holiday.data.HolidayPersonalList;
import com.attendance.tracker.activity.company.holiday.data.HolidayPersonalModel;
import com.attendance.tracker.data.ProfileData;
import com.attendance.tracker.interfaces.OnBlockListener;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HolidayPersonalReportActivity extends AppCompatActivity implements OnBlockListener {
    CheckInternetConnection internetConnection;
    AppSessionManager appSessionManager;
    String empID = "00";
    List<String> employeeName = new ArrayList<>();
    List<String> eID = new ArrayList<>();
    Spinner sp_employee,sp_Type;
    String[] type;
    String userId = "";
    LinearLayout lytEmployee ;
    int typeId = 1 ;
    ArrayList<HolidayPersonalList> personalLists = new ArrayList<>();
    HolidayPersonalAdapter adapterPersonal;
    RecyclerView rvPersonal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holiday_report);
        initVariable();
        initView();
        initListener();
    }
    private void initVariable() {
        appSessionManager = new AppSessionManager(this);
        internetConnection = new CheckInternetConnection();
        userId = appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID);
        getProfileData(userId,"1","");
    }
    private void initView() {
        sp_employee = findViewById(R.id.sp_employee);
        lytEmployee = findViewById(R.id.lytEmployee);
        rvPersonal = findViewById(R.id.rvPersonal);
    }

    private void initListener() {

        //employee Spinner
        sp_employee.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                empID = eID.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitData();
            }
        });

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void submitData() {
        if (!empID.equals("00")){
            callPersonalApi(String.valueOf(typeId),empID);
        }else {
            Toast.makeText(this, "Please select Employee", Toast.LENGTH_SHORT).show();
        }
    }



    private void callPersonalApi(String typeId,String empID) {

        if (internetConnection.isInternetAvailable(HolidayPersonalReportActivity.this)) {
            final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getHolidayPersonal(userId, typeId,empID).enqueue(new Callback<HolidayPersonalModel>() {
                @Override
                public void onResponse(Call<HolidayPersonalModel> call, Response<HolidayPersonalModel> response) {
                    if (response.isSuccessful()) {
                        personalLists.clear();
                        if (response.body().getError() == 0) {
                            personalLists.addAll(response.body().getHolidayList());
                            LoadPersonalDataList();
                        } else if (response.body().getError() == 1) {
                            Toast.makeText(HolidayPersonalReportActivity.this, "No Data Found.", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<HolidayPersonalModel> call, Throwable t) {
                    // progressBar.setVisibility(View.GONE);
                    Log.d("LOGIN", "onFailure: " + t.getMessage());
                    dialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }


    }

    private void LoadPersonalDataList() {
        adapterPersonal = new HolidayPersonalAdapter(personalLists,getApplicationContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvPersonal.setLayoutManager(layoutManager);
        rvPersonal.setHasFixedSize(true);
        rvPersonal.setAdapter(adapterPersonal);
        adapterPersonal.notifyDataSetChanged();
        adapterPersonal.setClickListener(this);
    }

    public void getProfileData(String userId, String type,String search) {
        if (internetConnection.isInternetAvailable(HolidayPersonalReportActivity.this)) {
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getProfileList(userId, type,search).enqueue(new Callback<ProfileData>() {
                @Override
                public void onResponse(Call<ProfileData> call, Response<ProfileData> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getError() == 0) {
                            employeeName.add("Select Employee Name");
                            eID.add("00");
                            for (int i = 0;i < response.body().getReport().size(); i++){
                                employeeName.add(response.body().getReport().get(i).getName());
                                eID.add(response.body().getReport().get(i).getId());


                            }
                            callPersonalApi(String.valueOf(typeId),response.body().getReport().get(1).getId());
                            loadData();
                        } else if (response.body().getError() == 1) {
                            Toast.makeText(HolidayPersonalReportActivity.this, "No Data Found.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ProfileData> call, Throwable t) {
                    // progressBar.setVisibility(View.GONE);
                    Log.d("LOGIN", "onFailure: " + t.getMessage());
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void loadData() {
        ArrayAdapter ad5 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, employeeName);
        ad5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_employee.setAdapter(ad5);
    }

    @Override
    public void itemUserBlockClick(View view, int position) {
        callDeleteApi(personalLists.get(position).getHolidayId());
    }

    private void callDeleteApi(String holiday) {
        if (internetConnection.isInternetAvailable(HolidayPersonalReportActivity.this)) {
            final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.deleteHoliday(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERNAME),
                    appSessionManager.getUserDetails().get(AppSessionManager.KEY_PASSWORD),
                    holiday).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.isSuccessful()) {
                        if (response.body().get("error").getAsInt() == 0){
                            Toast.makeText(HolidayPersonalReportActivity.this, ""+response.body().get("error_report").getAsString(), Toast.LENGTH_SHORT).show();
                            callPersonalApi(String.valueOf(typeId),empID);
                        }
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    // progressBar.setVisibility(View.GONE);
                    Log.d("LOGIN", "onFailure: " + t.getMessage());
                    dialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }

}