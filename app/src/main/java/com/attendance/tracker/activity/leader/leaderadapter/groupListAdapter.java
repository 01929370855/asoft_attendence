package com.attendance.tracker.activity.leader.leaderadapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.attendance.tracker.R;
import com.attendance.tracker.activity.NewCompanyList.CompanyReportList;
import com.attendance.tracker.data.DatewiseGroupReportList;
import com.attendance.tracker.interfaces.OnBlockListener;
import com.attendance.tracker.interfaces.OnUserClickListener;
import com.attendance.tracker.utils.AppSessionManager;

import java.util.ArrayList;

public class groupListAdapter extends RecyclerView.Adapter<groupListAdapter.ViewHolder> {
    private ArrayList<DatewiseGroupReportList> mData;
    private Context mActivity;


    AppSessionManager appSessionManager;

    // RecyclerView recyclerView;
    public groupListAdapter(ArrayList<DatewiseGroupReportList> mData, Context mActivity) {
        this.mData = mData;
        this.mActivity = mActivity;
        appSessionManager = new AppSessionManager(mActivity);
    }

    @Override
    public groupListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_group_list, parent, false);
        groupListAdapter.ViewHolder viewHolder = new groupListAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(groupListAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final DatewiseGroupReportList todayOfferModel = mData.get(position);


        holder.employeename.setText("Name:\t\t" + todayOfferModel.getName());
        holder.status.setText("Status:\t\t" + todayOfferModel.getStatus());
        holder.intime.setText("In Time:\t\t" + todayOfferModel.getInn());
        holder.outtime.setText("Out Time:\t\t" + todayOfferModel.getOut());



    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView employeename,intime,outtime,status;


        public ViewHolder(View itemView) {
            super(itemView);
            this.employeename = itemView.findViewById(R.id.employenameTV);
            this.intime = itemView.findViewById(R.id.inTV);
            this.outtime = itemView.findViewById(R.id.outTV);
            this.status = itemView.findViewById(R.id.satusTV);



        }

    }



}