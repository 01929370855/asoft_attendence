package com.attendance.tracker.activity.leader;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.MainActivity;
import com.attendance.tracker.R;
import com.attendance.tracker.activity.ProfileDetails.ProfileDetailsData;
import com.attendance.tracker.activity.leader.leaderadapter.CalenderAdapter;
import com.attendance.tracker.activity.leader.leaderadapter.DayAdapter;
import com.attendance.tracker.activity.leader.leaderadapter.ItemClick;
import com.attendance.tracker.activity.leader.model.AttReportModel;
import com.attendance.tracker.activity.leader.model.AttReportModelList;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.smarteist.autoimageslider.SliderView;
import com.smarteist.autoimageslider.SliderViewAdapter;

import org.naishadhparmar.zcustomcalendar.CustomCalendar;
import org.naishadhparmar.zcustomcalendar.OnDateSelectedListener;
import org.naishadhparmar.zcustomcalendar.Property;
import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DateWiseSingleReportActivity extends AppCompatActivity implements ItemClick {
    CheckInternetConnection internetConnection;
    AppSessionManager appSessionManager;
    DatePickerDialog picker;
    TextView tv_start_date, totalAbsent, totalPresent;
    TextView hederTitle;
    String employeId, type;
    String monthID;
    String yearID;
    String currentDate, currentMonth, currentYear;
    LinearLayout noteLyt;

    GridView calenderView, DayView;

    //spinner
    String[] Month ;
    String[] Years;
    CardView cvLayoutAbsent,cvLayoutHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_wise_single_report);

        initVariable();
        initListener();

        // tv_start_date = findViewById(R.id.tv_start_date);
        calenderView = findViewById(R.id.gridView);
        DayView = findViewById(R.id.gridViewDay);
        totalPresent = findViewById(R.id.totalPresent);
        totalAbsent = findViewById(R.id.totalAbsent);
        noteLyt = findViewById(R.id.noteLyt);
        cvLayoutAbsent = findViewById(R.id.cvLayoutAbsent);
        cvLayoutHome = findViewById(R.id.cvLayoutHome);

        cvLayoutAbsent.setBackgroundResource(R.drawable.bg_shape);
        cvLayoutHome.setBackgroundResource(R.drawable.bg_shape_shape);
    }

    private void initVariable() {
        Intent mIntent = getIntent();
        employeId = mIntent.getStringExtra("userId");
        type = mIntent.getStringExtra("type");
        appSessionManager = new AppSessionManager(this);
        internetConnection = new CheckInternetConnection();
        //start datepicker
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        Log.w("date",""+formattedDate);
        String[] separated = formattedDate.split("-");

        currentDate = separated[0];
        currentMonth =  separated[1]; //
        currentYear =  separated[2]; //

        Years = new String[]{currentYear,"2022","2023","2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039",
                "2040", "2041", "2042", "2042"};
        Month = new String[]{currentMonth, "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
//        tv_start_date.setText(currentMonth+"-"+currentYear);
        getCalenderData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID), employeId, currentMonth, currentYear);

        //
      //  loadSlider();
    }


    private void initListener() {
        // findViewById(R.id.lytStartDate).setOnClickListener(view -> showStartDatePicker());

        Spinner day_Month = findViewById(R.id.sp_Month);
        Spinner day_Year = findViewById(R.id.sp_year);
        tv_start_date = findViewById(R.id.tv_start_date);


        day_Month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                monthID = Month[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        ArrayAdapter ad
                = new ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                Month);


        // set simple layout resource file
        // for each item of spinner
        ad.setDropDownViewResource(
                android.R.layout
                        .simple_spinner_dropdown_item);

        // Set the ArrayAdapter (ad) data on the
        // Spinner which binds data to spinner
        day_Month.setAdapter(ad);

        day_Year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                yearID = Years[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        ArrayAdapter ad2
                = new ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                Years);


        // set simple layout resource file
        // for each item of spinner
        ad2.setDropDownViewResource(
                android.R.layout
                        .simple_spinner_dropdown_item);

        // Set the ArrayAdapter (ad) data on the
        // Spinner which binds data to spinner
        day_Year.setAdapter(ad2);

        findViewById(R.id.btnSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (monthID == null) {
                    Toast.makeText(DateWiseSingleReportActivity.this, "Select Month", Toast.LENGTH_SHORT).show();
                } else if (yearID == null) {
                    Toast.makeText(DateWiseSingleReportActivity.this, "Select Month", Toast.LENGTH_SHORT).show();
                } else {
                    getCalenderData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID), employeId, monthID, yearID);
                }
            }
        });

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //  getCalenderData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID),employeId,monthID,yearID);
    }


    public void getCalenderData(String userID, String empID, String month, String year) {
        if (internetConnection.isInternetAvailable(getApplicationContext())) {
            // progressBar.setVisibility(View.VISIBLE);
            final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getSingleReport(userID, empID, month, year).enqueue(new Callback<AttReportModel>() {
                @Override
                public void onResponse(Call<AttReportModel> call, Response<AttReportModel> response) {
                    if (response.isSuccessful()) {
                        if (response.isSuccessful())
                            //  progressBar.setVisibility(View.GONE);
                            loadData(response.body().getReport());
                        loadDayData(response.body().getDateList());
                        noteLyt.setVisibility(View.VISIBLE);
                        totalAbsent.setText("Absent: " + response.body().getTotalAbsent());
                        totalPresent.setText("Present: " + response.body().getTotalPresent());

                        dialog.dismiss();
                    }
                }


                @Override
                public void onFailure(Call<AttReportModel> call, Throwable t) {
                    t.printStackTrace();
                    // progressBar.setVisibility(View.GONE);
                    dialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void loadDayData(ArrayList<String> dateList) {
        DayAdapter adapterDay = new DayAdapter(this, dateList);
        DayView.setAdapter(adapterDay);
    }

    private void loadData(ArrayList<AttReportModelList> report) {
        CalenderAdapter adapter = new CalenderAdapter(this, report);
        calenderView.setAdapter(adapter);
        adapter.setClick(this);
    }


    @Override
    public void onClick(View view, String in, String out) {
        showPopUp(in, out);
    }

    private void showPopUp(String in, String out) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(this);
        View view = layoutInflaterAndroid.inflate(R.layout.item_popup, null);
        TextView intime = view.findViewById(R.id.intime);
        TextView outime = view.findViewById(R.id.outime);
        intime.setText("In Time: " + in);
        outime.setText("Out Time: " + out);
        builder.setView(view);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();


        alertDialog.show();


//
//        view.findViewById(R.id.yesButton).setOnClickListener(v -> onBackPressed());
//        view.findViewById(R.id.nobutton).setOnClickListener(v -> alertDialog.dismiss());

    }

//    public void loadSlider(){
//        ArrayList<SliderData> sliderDataArrayList = new ArrayList<>();
//        SliderView sliderView = findViewById(R.id.slider);
//        sliderDataArrayList.add(new SliderData(R.drawable.s1));
//        sliderDataArrayList.add(new SliderData(R.drawable.s2));
//        sliderDataArrayList.add(new SliderData(R.drawable.s3));
//        sliderDataArrayList.add(new SliderData(R.drawable.s4));
//        SliderAdapter adapter = new SliderAdapter(this, sliderDataArrayList);
//        sliderView.setAutoCycleDirection(SliderView.LAYOUT_DIRECTION_LTR);
//        sliderView.setSliderAdapter(adapter);
//        sliderView.setScrollTimeInSec(3);
//        sliderView.setAutoCycle(true);
//        sliderView.startAutoCycle();
//    }
}

//class SliderData {
//
//    // image url is used to
//    // store the url of image
//    private int imgUrl;
//
//    // Constructor method.
//    public SliderData(int imgUrl) {
//        this.imgUrl = imgUrl;
//    }
//
//    // Getter method
//    public int getImgUrl() {
//        return imgUrl;
//    }
//
//    // Setter method
//    public void setImgUrl(int imgUrl) {
//        this.imgUrl = imgUrl;
//    }
//}
//class SliderAdapter extends SliderViewAdapter<SliderAdapter.SliderAdapterViewHolder> {
//
//    // list for storing urls of images.
//    private final List<SliderData> mSliderItems;
//
//    // Constructor
//    public SliderAdapter(Context context, ArrayList<SliderData> sliderDataArrayList) {
//        this.mSliderItems = sliderDataArrayList;
//    }
//
//    // We are inflating the slider_layout
//    // inside on Create View Holder method.
//    @Override
//    public SliderAdapterViewHolder onCreateViewHolder(ViewGroup parent) {
//        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.slider_layout, null);
//        return new SliderAdapterViewHolder(inflate);
//    }
//
//    // Inside on bind view holder we will
//    // set data to item of Slider View.
//    @Override
//    public void onBindViewHolder(SliderAdapterViewHolder viewHolder, final int position) {
//
//        final SliderData sliderItem = mSliderItems.get(position);
//
//        // Glide is use to load image
//        // from url in your imageview.
////        Glide.with(viewHolder.itemView)
////                .load(sliderItem.getImgUrl())
////                .fitCenter()
////                .into(viewHolder.imageViewBackground);
//
//        viewHolder.imageViewBackground.setImageResource(sliderItem.getImgUrl());
//    }
//
//    // this method will return
//    // the count of our list.
//    @Override
//    public int getCount() {
//        return mSliderItems.size();
//    }
//
//    static class SliderAdapterViewHolder extends SliderViewAdapter.ViewHolder {
//        // Adapter class for initializing
//        // the views of our slider view.
//        View itemView;
//        ImageView imageViewBackground;
//
//        public SliderAdapterViewHolder(View itemView) {
//            super(itemView);
//            imageViewBackground = itemView.findViewById(R.id.myimage);
//            this.itemView = itemView;
//        }
//    }
//}