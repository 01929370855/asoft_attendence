package com.attendance.tracker.activity.company.holiday.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.attendance.tracker.R;
import com.attendance.tracker.activity.company.holiday.data.HolidayList;
import com.attendance.tracker.interfaces.OnBlockListener;


import java.util.ArrayList;

public class HolidayOtherReportAdapter extends RecyclerView.Adapter<HolidayOtherReportAdapter.ViewHolder> {
    private ArrayList<HolidayList> mData;
    private Context mActivity;
    OnBlockListener blockListener;
    // RecyclerView recyclerView;
    public HolidayOtherReportAdapter(ArrayList<HolidayList> mData, Context mActivity) {
        this.mData = mData;
        this.mActivity = mActivity;
    }

    @Override
    public HolidayOtherReportAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_holiday_other, parent, false);
        HolidayOtherReportAdapter.ViewHolder viewHolder = new HolidayOtherReportAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HolidayOtherReportAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final HolidayList todayOfferModel = mData.get(position);


        holder.sl.setText(""+ todayOfferModel.getSl());
        holder.name.setText("Day:\t\t" + todayOfferModel.getTitle());
        holder.mobile.setText("Date:\t\t" + todayOfferModel.getPostTime());
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blockListener.itemUserBlockClick(v,position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name,mobile,sl;
        ImageView remove;


        public ViewHolder(View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.nameTV);
            this.mobile = itemView.findViewById(R.id.mobileTV);
            this.sl = itemView.findViewById(R.id.sl);
            this.remove = itemView.findViewById(R.id.remove);
        }

    }


    public void setClickListener( OnBlockListener blockListener){
        this.blockListener = blockListener;

    }

}