package com.attendance.tracker.activity.leader;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.R;
import com.attendance.tracker.activity.leader.leaderadapter.CalenderAdapter;
import com.attendance.tracker.activity.leader.leaderadapter.DayAdapter;
import com.attendance.tracker.activity.leader.leaderadapter.ItemClick;
import com.attendance.tracker.activity.leader.leaderadapter.groupListAdapter;
import com.attendance.tracker.adapter.CompanyListAdapter;
import com.attendance.tracker.data.DateWiseGroupReportResponse;
import com.attendance.tracker.data.DatewiseGroupReportList;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.google.android.material.snackbar.Snackbar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DateWiseGroupReportActivity extends AppCompatActivity  {
    CheckInternetConnection internetConnection;
    AppSessionManager appSessionManager;
    DatePickerDialog picker;
    TextView tv_start_date, totalAbsent,totalPresent;
     groupListAdapter adapter;
    TextView hederTitle;
    String employeId, type;
    String monthID;
    String dayID;
    String yearID;
    String currentDate,currentMonth,currentYear;
    LinearLayout noteLyt;
    private RecyclerView userList;
    GridView calenderView,DayView;
    String[] Month;
    String[] day;
    String[] Years;
    Spinner day_Month;
    Spinner day_Year;
    Spinner day_day;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_wise_group_report);




        initVariable();


        totalPresent = findViewById(R.id.totalPresent);
        totalAbsent = findViewById(R.id.totalAbsent);
        userList = findViewById(R.id.rv_userList);
         day_Month = findViewById(R.id.sp_Month);
         day_Year = findViewById(R.id.sp_year);
         day_day = findViewById(R.id.sp_day);
        tv_start_date = findViewById(R.id.tv_start_date);
        initListener();

    }
    private void initVariable() {
        Intent mIntent = getIntent();
        employeId = mIntent.getStringExtra("userId");
        type = mIntent.getStringExtra("type");
        appSessionManager = new AppSessionManager(this);
        internetConnection = new CheckInternetConnection();
    }



    private void initListener() {
       // findViewById(R.id.lytStartDate).setOnClickListener(view -> showStartDatePicker());




        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        Log.w("date",""+formattedDate);
        String[] separated = formattedDate.split("-");

        currentDate = separated[0];
        currentMonth =  separated[1]; //
        currentYear =  separated[2]; //

        getCalenderData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID),""+currentDate,""+currentMonth,""+currentYear);

        //spinner
        Month = new String[]{currentMonth, "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        day = new String[]{currentDate, "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"};
        Years = new String[]{currentYear, "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039",
                "2040", "2041", "2042", "2042"};
        //for day pick
        day_day.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                dayID=day[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        ArrayAdapter aday
                = new ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                day);
        aday.setDropDownViewResource(
                android.R.layout
                        .simple_spinner_dropdown_item);
        day_day.setAdapter(aday);

        //end


        day_Month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                monthID=Month[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        ArrayAdapter ad
                = new ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                Month);



        // set simple layout resource file
        // for each item of spinner
        ad.setDropDownViewResource(
                android.R.layout
                        .simple_spinner_dropdown_item);

        // Set the ArrayAdapter (ad) data on the
        // Spinner which binds data to spinner
        day_Month.setAdapter(ad);

        day_Year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                yearID=Years[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        ArrayAdapter ad2
                = new ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                Years);



        // set simple layout resource file
        // for each item of spinner
        ad2.setDropDownViewResource(
                android.R.layout
                        .simple_spinner_dropdown_item);

        // Set the ArrayAdapter (ad) data on the
        // Spinner which binds data to spinner
        day_Year.setAdapter(ad2);

        findViewById(R.id.btnSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (monthID == null){
                    Toast.makeText(DateWiseGroupReportActivity.this, "Select Month", Toast.LENGTH_SHORT).show();
                }else if(yearID == null ){
                    Toast.makeText(DateWiseGroupReportActivity.this, "Select Year", Toast.LENGTH_SHORT).show();
                }else if(dayID == null ) {
                    Toast.makeText(DateWiseGroupReportActivity.this, "Select Day", Toast.LENGTH_SHORT).show();
                }

                else {
                    getCalenderData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID),dayID,monthID,yearID);
                }
            }
        });

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

      //  getCalenderData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID),employeId,monthID,yearID);
    }


    public void getCalenderData(String userID,String dayid,String month,String year) {
        if (internetConnection.isInternetAvailable(getApplicationContext())) {
            // progressBar.setVisibility(View.VISIBLE);
            final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getGroupReport(userID,dayid,month,year).enqueue(new Callback<DateWiseGroupReportResponse>() {
                @Override
                public void onResponse(Call<DateWiseGroupReportResponse> call, Response<DateWiseGroupReportResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.isSuccessful())
                            //  progressBar.setVisibility(View.GONE);
                            loadData(response.body().getReport());
                        totalAbsent.setText("Absent: " + response.body().getTotalAbsent());
                        totalPresent.setText("Present: " + response.body().getTotalPresent());


                        dialog.dismiss();
                    }
                }


                @Override
                public void onFailure(Call<DateWiseGroupReportResponse> call, Throwable t) {
                    t.printStackTrace();
                    // progressBar.setVisibility(View.GONE);
                    dialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }



    private void loadData(ArrayList<DatewiseGroupReportList> report) {
        adapter = new groupListAdapter(report,getApplicationContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        userList.setLayoutManager(layoutManager);
        userList.setHasFixedSize(true);
        userList.setAdapter(adapter);
        //adapter.notifyDataSetChanged();
        //adapter.SetItemClick(this);


    }
}