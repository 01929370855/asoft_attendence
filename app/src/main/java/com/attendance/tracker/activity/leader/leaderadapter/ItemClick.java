package com.attendance.tracker.activity.leader.leaderadapter;

import android.view.View;

public interface ItemClick {
    void onClick(View view, String in, String out);
}
