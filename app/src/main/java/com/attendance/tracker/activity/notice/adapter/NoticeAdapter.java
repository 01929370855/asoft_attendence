package com.attendance.tracker.activity.notice.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.attendance.tracker.R;
import com.attendance.tracker.activity.notice.model.NoticeList;
import com.attendance.tracker.interfaces.OnBlockListener;

import java.util.ArrayList;

public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.ViewHolder> {
    private ArrayList<NoticeList> mData;
    private Context mActivity;
    OnBlockListener blockListener;

    // RecyclerView recyclerView;
    public NoticeAdapter(ArrayList<NoticeList> mData, Context mActivity) {
        this.mData = mData;
        this.mActivity = mActivity;
    }

    @Override
    public NoticeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_notice, parent, false);
        NoticeAdapter.ViewHolder viewHolder = new NoticeAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NoticeAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final NoticeList todayOfferModel = mData.get(position);


        holder.sl.setText("" + todayOfferModel.getNoticeId());
        holder.name.setText("Title:\t\t" + todayOfferModel.getTitle());
        holder.mobile.setText("Date:\t\t" + todayOfferModel.getPostTime());

        if (todayOfferModel.getRead().equals("0")){
            holder.lytMain.setBackgroundColor(mActivity.getResources().getColor(R.color.cardbg));
        }else if (
                todayOfferModel.getRead().equals("1") ){
            holder.lytMain.setBackgroundColor(mActivity.getResources().getColor(R.color.white));

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blockListener.itemUserBlockClick(v, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, mobile, sl;
        LinearLayout lytMain;


        public ViewHolder(View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.nameTV);
            this.mobile = itemView.findViewById(R.id.mobileTV);
            this.sl = itemView.findViewById(R.id.sl);
            this.lytMain = itemView.findViewById(R.id.lytMain);
        }

    }


    public void setClickListener(OnBlockListener blockListener) {
        this.blockListener = blockListener;

    }

}