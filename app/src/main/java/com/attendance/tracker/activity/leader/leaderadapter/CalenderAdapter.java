package com.attendance.tracker.activity.leader.leaderadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.attendance.tracker.R;
import com.attendance.tracker.activity.leader.model.AttReportModelList;

import java.util.ArrayList;

public class CalenderAdapter extends ArrayAdapter<AttReportModelList> {

    ItemClick itemClick;
    public CalenderAdapter(@NonNull Context context, ArrayList<AttReportModelList> courseModelArrayList) {
        super(context, 0, courseModelArrayList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listitemView = convertView;
        if (listitemView == null) {
            // Layout Inflater inflates each item to be displayed in GridView.
            listitemView = LayoutInflater.from(getContext()).inflate(R.layout.absent_view, parent, false);
        }

        AttReportModelList courseModel = getItem(position);
        TextView courseTV = listitemView.findViewById(R.id.text_view);
        CardView cardView = listitemView.findViewById(R.id.cvLayout);


        courseTV.setText(courseModel.getDay());

        if (courseModel.getStatus().equals("Absent")){
            cardView.setBackgroundResource(R.drawable.bg_shape);

              courseTV.setTextColor(getContext().getResources().getColor(R.color.white));
          //  courseTV.setBackgroundResource(R.color.red_400);
        }else if (courseModel.getStatus().equals("Present")){
            courseTV.setTextColor(getContext().getResources().getColor(R.color.white));
            //courseTV.setBackgroundResource(R.color.green_400);
            cardView.setBackgroundResource(R.drawable.bg_shape_shape);

        }else {
            courseTV.setTextColor(getContext().getResources().getColor(R.color.white));
            cardView.setBackgroundResource(R.drawable.bg_shape_shape);
        }

        courseTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClick.onClick(v,courseModel.getInn(),courseModel.getOut());
            }
        });



        return listitemView;
    }

    public void setClick(ItemClick itemClick){
        this.itemClick = itemClick;
    }
}

