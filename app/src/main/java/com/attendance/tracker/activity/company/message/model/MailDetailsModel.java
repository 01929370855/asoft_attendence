package com.attendance.tracker.activity.company.message.model;

import com.google.gson.annotations.SerializedName;

public class MailDetailsModel {
    @SerializedName("error")
    private Integer error;
    @SerializedName("error_report")
    private String errorReport;
    @SerializedName("mail_id")
    private String mailId;
    @SerializedName("sender_id")
    private String senderId;
    @SerializedName("receiver_id")
    private String receiverId;
    @SerializedName("sender_category")
    private String senderCategory;
    @SerializedName("receiver_category")
    private String receiverCategory;
    @SerializedName("sender_name")
    private String senderName;
    @SerializedName("sending_time")
    private String sendingTime;
    @SerializedName("mail_read")
    private String mailRead;
    @SerializedName("subject")
    private String subject;
    @SerializedName("message")
    private String message;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getErrorReport() {
        return errorReport;
    }

    public void setErrorReport(String errorReport) {
        this.errorReport = errorReport;
    }

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getSenderCategory() {
        return senderCategory;
    }

    public void setSenderCategory(String senderCategory) {
        this.senderCategory = senderCategory;
    }

    public String getReceiverCategory() {
        return receiverCategory;
    }

    public void setReceiverCategory(String receiverCategory) {
        this.receiverCategory = receiverCategory;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSendingTime() {
        return sendingTime;
    }

    public void setSendingTime(String sendingTime) {
        this.sendingTime = sendingTime;
    }

    public String getMailRead() {
        return mailRead;
    }

    public void setMailRead(String mailRead) {
        this.mailRead = mailRead;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
