package com.attendance.tracker.activity.company.message.model;

import com.google.gson.annotations.SerializedName;

public class OutboxDataListModel {
    @SerializedName("mail_id")
    private String mailId;
    @SerializedName("sender_id")
    private String senderId;
    @SerializedName("receiver_id")
    private String receiverId;
    @SerializedName("sender_category")
    private String senderCategory;
    @SerializedName("receiver_category")
    private String receiverCategory;
    @SerializedName("subject")
    private String subject;
    @SerializedName("sender_name")
    private String senderName;
    @SerializedName("receiver_name")
    private String receiverName;
    @SerializedName("time")
    private String time;
    @SerializedName("read")
    private String read;

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getSenderCategory() {
        return senderCategory;
    }

    public void setSenderCategory(String senderCategory) {
        this.senderCategory = senderCategory;
    }

    public String getReceiverCategory() {
        return receiverCategory;
    }

    public void setReceiverCategory(String receiverCategory) {
        this.receiverCategory = receiverCategory;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRead() {
        return read;
    }

    public void setRead(String read) {
        this.read = read;
    }
}
