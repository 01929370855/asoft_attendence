package com.attendance.tracker.activity.company.message.model;

public interface CallFromInboxMessageList {
    public void onItemClickListner(String mailID, String senderId, String senderCategory, String mailSubject, String mailSenderName);
}
