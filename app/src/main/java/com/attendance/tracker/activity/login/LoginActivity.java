package com.attendance.tracker.activity.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.ForgotpassActivity;
import com.attendance.tracker.MainActivity;
import com.attendance.tracker.R;
import com.attendance.tracker.activity.company.CompanyActivity;
import com.attendance.tracker.activity.master.CreateCompanyActivity;
import com.attendance.tracker.activity.master.MasterActivity;
import com.attendance.tracker.activity.splash.SplashActivity;
import com.attendance.tracker.activity.user.MapTestUserActivity;
import com.attendance.tracker.activity.user.UserMainActivity;
import com.attendance.tracker.agent.AgentDashboardActivity;
import com.attendance.tracker.agent.SalesReportActivity;
import com.attendance.tracker.data.LoginData;
import com.attendance.tracker.data.SalesReport;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.installations.FirebaseInstallations;
import com.google.firebase.installations.InstallationTokenResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private EditText et_name,et_password;
    CheckInternetConnection internetConnection;
    AppSessionManager appSessionManager;
    AppCompatImageView map,man;
    boolean isPasswordVisible = false;
    ImageView passwordView;
    AppCompatTextView createAccount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVariable();
        initView();
        initFunc();
        initListener();

    }

    private void initVariable() {
        internetConnection = new CheckInternetConnection();
        appSessionManager = new AppSessionManager(this);
    }

    private void initView() {


        setContentView(R.layout.activity_login);
/*        FirebaseInstallations.getInstance().getToken(false).addOnCompleteListener(new OnCompleteListener<InstallationTokenResult>() {
            @Override
            public void onComplete(@NonNull Task<InstallationTokenResult> task) {

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("device_token", task.getResult().getToken());
                editor.apply();
            }
        });*/

        et_name = findViewById(R.id.et_name);
        et_password = findViewById(R.id.et_password);
        passwordView = findViewById(R.id.showPassword);
      //  et_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent();
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                startActivity(intent);
            }
        }

//        if (appSessionManager.isShowPolicy()){
//            showPrivacyPolicy();
//        }


    }

    private void initFunc() {

    }

    private void initListener() {
        findViewById(R.id.login).setOnClickListener(view -> gotoNext());

        findViewById(R.id.showPassword).setOnClickListener(view -> showPassword());

        findViewById(R.id.forgotpassTV).setOnClickListener(view -> gotoForgotPassword());

        findViewById(R.id.tvRegister).setOnClickListener(view -> gotoRegistration());

    }

    private void gotoForgotPassword() {
        startActivity(new Intent(this, ForgotpassActivity.class));
    }

    private void gotoRegistration() {
        //startActivity(new Intent(this, CreateCompanyActivity.class));
        Intent nIntent = new Intent(this,CreateCompanyActivity.class);
        nIntent.putExtra("source","isLogin");
        startActivity(nIntent);
    }

    private void showPassword() {
        if (isPasswordVisible) {
            String pass = et_password.getText().toString();
            et_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
            et_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            et_password.setText(pass);
            et_password.setSelection(pass.length());
            passwordView.setImageResource(R.drawable.eye);

        } else {
            String pass = et_password.getText().toString();
            et_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            et_password.setInputType(InputType.TYPE_CLASS_TEXT);
            et_password.setText(pass);
            et_password.setSelection(pass.length());
            passwordView.setImageResource(R.drawable.hidden);

        }
        isPasswordVisible= !isPasswordVisible;

    }

    private void gotoNext() {
        if (et_name.getText().toString().equals("")){
            Toast.makeText(this, "Enter Your User Name", Toast.LENGTH_SHORT).show();

        }else if (et_password.getText().toString().equals("")){
            Toast.makeText(this, "Enter Your Password", Toast.LENGTH_SHORT).show();

        }else {
            String userName = et_name.getText().toString();
            String userPassword = et_password.getText().toString();

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            String deviceToken = prefs.getString("device_token", null);
            Log.d("device_token",deviceToken);
            callLoginApi(userName,userPassword,deviceToken);
        }
    }

    private void callLoginApi(String userName, String userPassword,String deviceToken) {
        if (internetConnection.isInternetAvailable(LoginActivity.this)) {
            final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();

            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.goLogin(userName,userPassword,deviceToken).enqueue(new Callback<LoginData>() {
                @Override
                public void onResponse(Call<LoginData> call, Response<LoginData> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getError() == 0) {
                            dialog.dismiss();
                            appSessionManager.createLoginSession(""+response.body().getId(),""+userName,
                                    ""+userPassword,""+response.body().getCatagoryType(),""+response.body().getMobile(),
                                    ""+response.body().getAddress(),""+response.body().getImageUrl(),response.body().getCompanyId(),response.body().getLeaderId());

                            switch (response.body().getCatagoryType()){
                                case "0":
                                    gotoUser();
                                    break;
                                case "1":
                                    gotoLeader();
                                    break;
                                case "2":
                                    gotoCompany();
                                    break;
                                case "3":
                                    gotoMasterAdmin();
                                    break;
                                case "4":
                                    gotoAgent();
                                    break;
                                default:
                                    break;
                            }

                            finish();


                        } else if (response.body().getError() == 1) {
                            dialog.dismiss();
                            Toast.makeText(LoginActivity.this, "Wrong login information.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<LoginData> call, Throwable t) {
                    dialog.dismiss();
                    Log.d("LOGIN", "onFailure: " + t.getMessage());
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }

    }

    private void gotoAgent() {
        startActivity(new Intent(this, AgentDashboardActivity.class));

    }

    private void gotoCompany() {
        startActivity(new Intent(this,CompanyActivity.class));
    }
    private void gotoUser() {
/*        startActivity(new Intent(this, UserMainActivity.class));
        finish();*/


        Intent mIntent = new Intent(this, MapTestUserActivity.class);
        // mIntent.putExtra("userId",appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID));
        startActivity(mIntent);
    }
    private void gotoMasterAdmin() {
        startActivity(new Intent(this, MasterActivity.class));
    }
    private void gotoLeader() {
       // startActivity(new Intent(this, MainActivity.class));
        Intent mIntent = new Intent(this, MapTestUserActivity.class);
        startActivity(mIntent);
    }
    private void showPrivacyPolicy() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final AlertDialog popupCon = builder.create();
        LayoutInflater layoutInflater = getLayoutInflater();
        View customView = layoutInflater.inflate(R.layout.privecy_dialog, null);
        WebView webView = customView.findViewById(R.id.webview);
        TextView agree = customView.findViewById(R.id.agree);
        TextView cancel = customView.findViewById(R.id.cancel);

        webView.loadUrl("file:///android_asset/privacy_policy.html");

        agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupCon.dismiss();

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0);
            }
        });


        popupCon.setCancelable(false);
        popupCon.setView(customView);
        popupCon.show();
    }

}
