package com.attendance.tracker.activity.notice.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.R;
import com.attendance.tracker.activity.notice.model.NoticeDetails;
import com.attendance.tracker.activity.notice.model.NoticeModel;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NoticeDetailsActivity extends AppCompatActivity {
    AppSessionManager appSessionManager;
    CheckInternetConnection internetConnection;
    String noticeID;
    TextView date,title,details;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_details);
        initVariable();
        initView();
        initFunc();
    }

    private void initView() {
        date = findViewById(R.id.date);
        title = findViewById(R.id.title);
        details = findViewById(R.id.details);
    }

    private void initFunc() {
        callNoticeDetailsApi(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID),noticeID);


        findViewById(R.id.back).setOnClickListener(v -> finish());
    }

    private void initVariable() {
        appSessionManager = new AppSessionManager(this);
        internetConnection = new CheckInternetConnection();
        noticeID = getIntent().getStringExtra("noticeID");
    }

    private void callNoticeDetailsApi(String userId,String noticeId) {
        if (internetConnection.isInternetAvailable(NoticeDetailsActivity.this)) {
            final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();

            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getNoticeDetails(userId,noticeId).enqueue(new Callback<NoticeDetails>() {
                @Override
                public void onResponse(Call<NoticeDetails> call, Response<NoticeDetails> response) {
                    if (response.isSuccessful()) {

                        if (response.body().getError() == 0) {
                            date.setText("Date: "+response.body().getDates());
                            title.setText("Title: "+ response.body().getTitle());
                            details.setText(" "+response.body().getDetails());


                        } else if (response.body().getError() == 1) {
                            Toast.makeText(NoticeDetailsActivity.this, "No Data Found.", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<NoticeDetails> call, Throwable t) {
                    // progressBar.setVisibility(View.GONE);
                    Log.d("LOGIN", "onFailure: " + t.getMessage());
                    dialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }

    }

}