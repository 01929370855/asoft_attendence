package com.attendance.tracker.activity.leader.leaderadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.attendance.tracker.R;
import com.attendance.tracker.activity.leader.model.AttReportModelList;

import java.util.ArrayList;

public class DayAdapter extends ArrayAdapter<String> {

    public DayAdapter(@NonNull Context context, ArrayList<String> courseModelArrayList) {
        super(context, 0, courseModelArrayList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listitemView = convertView;
        if (listitemView == null) {
            // Layout Inflater inflates each item to be displayed in GridView.
            listitemView = LayoutInflater.from(getContext()).inflate(R.layout.default_view, parent, false);
        }

        String courseModel = getItem(position);
        TextView courseTV = listitemView.findViewById(R.id.text_view);
        CardView cv = listitemView.findViewById(R.id.lytde);
        cv.setBackgroundResource(R.drawable.bg_shape_shape);

        courseTV.setText(courseModel);

        return listitemView;
    }


}
