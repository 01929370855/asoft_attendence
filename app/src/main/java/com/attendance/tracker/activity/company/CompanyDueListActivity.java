package com.attendance.tracker.activity.company;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import android.app.Dialog;
import android.os.Bundle;

import com.attendance.tracker.R;
import com.attendance.tracker.activity.AttendanceReport.AttendanceviewPagerAdapter;
import com.google.android.material.tabs.TabLayout;

public class CompanyDueListActivity extends AppCompatActivity {
    private DrawerLayout drawer;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_due_list);


        tabLayout = findViewById(R.id.tabLayoutID);
        viewPager = findViewById(R.id.viewPager);
        tabLayout.setupWithViewPager(viewPager);

        ComDueAdapter viewPagerAdapter = new ComDueAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(0, true);

        findViewById(R.id.back).setOnClickListener(view -> finish());

    }




}
