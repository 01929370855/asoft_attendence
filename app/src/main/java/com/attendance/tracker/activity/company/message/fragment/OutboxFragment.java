package com.attendance.tracker.activity.company.message.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.R;
import com.attendance.tracker.activity.company.message.ComposeMailActivity;
import com.attendance.tracker.activity.company.message.MailDetailsActivity;
import com.attendance.tracker.activity.company.message.OutboxMessageAdapter;
import com.attendance.tracker.activity.company.message.model.CallFromMessageList;
import com.attendance.tracker.activity.company.message.model.OutboxDataListModel;
import com.attendance.tracker.activity.company.message.model.OutboxDataModel;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OutboxFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, CallFromMessageList {

    private AppSessionManager appSessionManager;
    private CheckInternetConnection checkInternetConnection;
    private View mView;

    SwipeRefreshLayout swipeLayout;
    RecyclerView recyclerViewOutbox;
    FloatingActionButton fabCompose;

    private List<OutboxDataListModel> odlModel = new ArrayList<>();
    private OutboxMessageAdapter outboxMessageAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView =  inflater.inflate(R.layout.fragment_outbox, container, false);
        appSessionManager = new AppSessionManager(getActivity());
        checkInternetConnection = new CheckInternetConnection();
        initView();
        swipeLayout.setOnRefreshListener(this);
        fabCompose.setOnClickListener(this);
        initializedListFields();
        return mView;
    }

    private void initView() {
        swipeLayout =mView.findViewById(R.id.outbox_swipe_container);
        recyclerViewOutbox =mView.findViewById(R.id.recycler_view_for_outbox);
        fabCompose =mView.findViewById(R.id.fab_compose_mail_outbox);
    }

    void initializedListFields() {
        outboxMessageAdapter = new OutboxMessageAdapter(getActivity(), odlModel, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewOutbox.setLayoutManager(mLayoutManager);
        recyclerViewOutbox.setItemAnimator(new DefaultItemAnimator());
        recyclerViewOutbox.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        recyclerViewOutbox.setAdapter(outboxMessageAdapter);
    }

    @Override
    public void onClick(View mView) {
        switch (mView.getId()) {
            case R.id.fab_compose_mail_outbox:
                Bundle composeBundle = new Bundle();
                composeBundle.putString("ACTIONIDENTY", "BLANK");
                Intent goMailCompose = new Intent(getActivity(), ComposeMailActivity.class);
                goMailCompose.putExtras(composeBundle);
                startActivity(goMailCompose);
                break;
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeLayout.setRefreshing(false);
                downloadOutboxData();
            }
        }, 2000);
    }

    @Override
    public void onResume() {
        super.onResume();
        downloadOutboxData();
    }

    void downloadOutboxData() {
        if (checkInternetConnection.isInternetAvailable(getActivity())) {
            final MaterialDialog dialog = new MaterialDialog.Builder(getActivity()).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();

            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getOutboxData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERNAME)
                            , appSessionManager.getUserDetails().get(AppSessionManager.KEY_PASSWORD)
                            , appSessionManager.getUserDetails().get(AppSessionManager.KEY_CATEGORY)
                            , "100")
                    .enqueue(new Callback<OutboxDataModel>() {
                        @Override
                        public void onResponse(Call<OutboxDataModel> call, Response<OutboxDataModel> response) {
                            if (response.isSuccessful()) {
                                dialog.dismiss();
                                odlModel.clear();
                                odlModel.addAll(response.body().getOutbox());
                                outboxMessageAdapter.notifyDataSetChanged();
                            } else {
                                dialog.dismiss();
                                Log.e("OUTBOXLIST", "Error :" + response.code());
                                Toast.makeText(getActivity(), "Error!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<OutboxDataModel> call, Throwable t) {
                            dialog.dismiss();
                            Log.d("OUTBOXLIST", "onFailure: " + t.getMessage());
                            Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else {
            Snackbar.make(getView(), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onItemClickListner(String messageID, String receiverId, String receiverCategory, String mailSubject, String senderName) {
        Bundle bundle = new Bundle();
        bundle.putString("MSGCATEGORY", "outbox");
        bundle.putString("MESSAGEID", messageID);
        bundle.putString("RECEIVERID", receiverId);
        bundle.putString("RECEIVERCATEGORY", receiverCategory);
        bundle.putString("MAILSUBJECT", mailSubject);
        bundle.putString("SENDERNAME", senderName);
        Intent goDetails = new Intent(getActivity(), MailDetailsActivity.class);
        goDetails.putExtras(bundle);
        startActivity(goDetails);
    }
}
