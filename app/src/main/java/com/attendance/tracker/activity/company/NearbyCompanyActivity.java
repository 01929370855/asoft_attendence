package com.attendance.tracker.activity.company;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.R;
import com.attendance.tracker.activity.NewCompanyList.CompanyListResponse;
import com.attendance.tracker.activity.NewCompanyList.CompanyReportList;
import com.attendance.tracker.activity.NewCompanyList.NewCompanyListActivity;
import com.attendance.tracker.activity.company.fragment.NearbyCompanyClickListener;
import com.attendance.tracker.activity.company.fragment.adapter.NearbyCompanyAdapter;
import com.attendance.tracker.activity.company.message.ComposeMailActivity;
import com.attendance.tracker.activity.company.message.MessageActivity;
import com.attendance.tracker.activity.company.message.model.NearbyCompany;
import com.attendance.tracker.activity.company.message.model.NearbyCompanyList;
import com.attendance.tracker.adapter.CompanyListAdapter;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NearbyCompanyActivity extends AppCompatActivity implements NearbyCompanyClickListener {
    SeekBar seekBar;
    TextView tvProgress;
    private ArrayList<NearbyCompanyList> userDataList;
    private CheckInternetConnection internetConnection;
    private AppSessionManager appSessionManager;
    private NearbyCompanyAdapter userAdapter;
    private RecyclerView comList;
    private FusedLocationProviderClient fusedLocationClient;
    static String userExitLat, userExitLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby);
        initVariable();
        initView();
        initFunc();

    }

    private void initVariable() {
        userDataList = new ArrayList<>();
        internetConnection = new CheckInternetConnection();
        appSessionManager = new AppSessionManager(this);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            userExitLat = String.valueOf(location.getLatitude());
                            userExitLng = String.valueOf(location.getLongitude());
                            getCompanyData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID),userExitLat+","+userExitLng, String.valueOf(50));

                        }
                    }
                });
    }

    private void initView() {
        seekBar = findViewById(R.id.seekBar);
        comList = findViewById(R.id.comList);
        tvProgress = findViewById(R.id.tvProgress);



    }



    private void initFunc() {

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                tvProgress.setText(""+progress);
                userDataList.clear();
                getCompanyData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID),userExitLat+","+userExitLng,""+progress);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
              //  Toast.makeText(getApplicationContext(),"seekbar touch started!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //Toast.makeText(getApplicationContext(),"seekbar touch stopped!", Toast.LENGTH_SHORT).show();
            }
        });

        LoadData();

    }
    public void getCompanyData(String userId, String geo,String raduis) {
        if (internetConnection.isInternetAvailable(NearbyCompanyActivity.this)) {
            final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getNearbyCompanyList(userId, geo,raduis).enqueue(new Callback<NearbyCompany>() {
                @Override
                public void onResponse(Call<NearbyCompany> call, Response<NearbyCompany> response) {
                    NearbyCompany companyListResponse=response.body();
                    String total_company= String.valueOf(companyListResponse.getTotalComapny());
                    if (response.isSuccessful()) {



                        if (response.body().getError() == 0) {
                            userDataList.clear();
                            userDataList.addAll(response.body().getReport());
                            userAdapter.notifyDataSetChanged();
                          //  total.setText("Total Company:\t\t"+total_company);


                        } else if (response.body().getError() == 1) {
                            Toast.makeText(NearbyCompanyActivity.this, "No Data Found.", Toast.LENGTH_SHORT).show();
                        }

                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<NearbyCompany> call, Throwable t) {
                    dialog.dismiss();
                    Log.d("LOGIN", "onFailure: " + t.getMessage());
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void LoadData() {
        userAdapter = new NearbyCompanyAdapter(userDataList,getApplicationContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        comList.setLayoutManager(layoutManager);
        comList.setHasFixedSize(true);
        comList.setAdapter(userAdapter);

        userAdapter.SetItemClick(this);
    }

    @Override
    public void itemMessageClick(View view, int position) {
//        Intent newIntent = new Intent(this,MessageActivity.class);
////        newIntent.putExtra("userId",userDataList.get(position).getId());
////        newIntent.putExtra("userName",userDataList.get(position).getName());
//        startActivity(newIntent);
        Intent goMailCompose = new Intent(this, ComposeMailActivity.class);
        goMailCompose.putExtra("ACTIONIDENTY", "direct");
        goMailCompose.putExtra("RECEIVERNAME", userDataList.get(position).getName());
        goMailCompose.putExtra("RECEIVERCATEGORY", "0");
        goMailCompose.putExtra("RECEIVERID", userDataList.get(position).getId());
        startActivity(goMailCompose);
    }

    @Override
    public void itemFbClick(View view, int position) {
        if (!userDataList.get(position).getFb().equals("")){
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(userDataList.get(position).getFb())));
            getFacebookPageURL(this,userDataList.get(position).getFb());
        }else {
            Toast.makeText(this, "Wrong Url", Toast.LENGTH_SHORT).show();
        }
    }
    public String getFacebookPageURL(Context context,String url) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + url;
            } else { //older versions of fb app
                return "fb://page/" + url;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return url; //normal web url
        }
    }
}