package com.attendance.tracker.activity.company.message.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.R;
import com.attendance.tracker.activity.company.message.MailDetailsActivity;
import com.attendance.tracker.activity.company.message.ComposeMailActivity;
import com.attendance.tracker.activity.company.message.InboxMessageAdapter;
import com.attendance.tracker.activity.company.message.model.CallFromInboxMessageList;
import com.attendance.tracker.activity.company.message.model.InboxDataListModel;
import com.attendance.tracker.activity.company.message.model.InboxDataModel;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class InboxFragment extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, CallFromInboxMessageList {


    AppSessionManager appSessionManager;
    CheckInternetConnection checkInternetConnection;
    View mView;

    SwipeRefreshLayout refreshLayoutInbox;
    RecyclerView recyclerViewInbox;
    FloatingActionButton fabComposemail;

    private List<InboxDataListModel> idlistModel = new ArrayList<>();
    private InboxMessageAdapter imAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView =  inflater.inflate(R.layout.fragment_inbox, container, false);
        appSessionManager = new AppSessionManager(getActivity());
        checkInternetConnection = new CheckInternetConnection();
        initView();
        initializedListFields();
        return mView;
    }

    private void initView() {
        refreshLayoutInbox = mView.findViewById(R.id.inbox_swipe_container);
        recyclerViewInbox = mView.findViewById(R.id.recycler_view_for_inbox);
        fabComposemail = mView.findViewById(R.id.fab_compose_mail);

        fabComposemail.setOnClickListener(this);
        refreshLayoutInbox.setOnRefreshListener(this);
    }

    void initializedListFields() {
        imAdapter = new InboxMessageAdapter(getActivity(), idlistModel, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewInbox.setLayoutManager(mLayoutManager);
        recyclerViewInbox.setItemAnimator(new DefaultItemAnimator());
        recyclerViewInbox.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        recyclerViewInbox.setAdapter(imAdapter);
    }

    @Override
    public void onClick(View mView) {
        switch (mView.getId()) {
            case R.id.fab_compose_mail:
                Bundle composeBundle = new Bundle();
                composeBundle.putString("ACTIONIDENTY", "BLANK");
                Intent goMailCompose = new Intent(getActivity(), ComposeMailActivity.class);
                goMailCompose.putExtras(composeBundle);
                startActivity(goMailCompose);
                break;
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                refreshLayoutInbox.setRefreshing(false);
                downloadInboxData();
            }
        }, 2000);
    }

    @Override
    public void onResume() {
        super.onResume();
        downloadInboxData();
    }

    void downloadInboxData() {
        if (checkInternetConnection.isInternetAvailable(getActivity())) {
            final MaterialDialog dialog = new MaterialDialog.Builder(getActivity()).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getInboxData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERNAME)
                            , appSessionManager.getUserDetails().get(AppSessionManager.KEY_PASSWORD)
                            , appSessionManager.getUserDetails().get(AppSessionManager.KEY_CATEGORY)
                            , "100")
                    .enqueue(new Callback<InboxDataModel>() {
                        @Override
                        public void onResponse(Call<InboxDataModel> call, Response<InboxDataModel> response) {
                            if (response.isSuccessful()) {
                                dialog.dismiss();
                                idlistModel.clear();
                                idlistModel.addAll(response.body().getInbox());
                                imAdapter.notifyDataSetChanged();
                            } else {
                                dialog.dismiss();
                                Log.e("INBOX", "Error :" + response.code());
                                Toast.makeText(getActivity(), "Error!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<InboxDataModel> call, Throwable t) {
                            dialog.dismiss();
                            Log.d("INBOX", "onFailure: " + t.getMessage());
                            Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            Snackbar.make(getView(), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onItemClickListner(String mailID, String senderId, String senderCategory, String mailSubject, String mailSenderName) {
        Bundle bundle = new Bundle();
        bundle.putString("MSGCATEGORY", "inbox");
        bundle.putString("MESSAGEID", mailID);
        bundle.putString("RECEIVERID", senderId);
        bundle.putString("RECEIVERCATEGORY", senderCategory);
        bundle.putString("MAILSUBJECT", mailSubject);
        bundle.putString("SENDERNAME", mailSenderName);
        Intent goDetails = new Intent(getActivity(), MailDetailsActivity.class);
        goDetails.putExtras(bundle);
        startActivity(goDetails);
    }
}
