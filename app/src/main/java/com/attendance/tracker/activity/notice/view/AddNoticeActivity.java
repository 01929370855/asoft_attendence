package com.attendance.tracker.activity.notice.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.R;
import com.attendance.tracker.activity.company.CreateLeaderActivity;
import com.attendance.tracker.data.JoinResponse;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNoticeActivity extends AppCompatActivity {
    AppSessionManager appSessionManager;
    CheckInternetConnection internetConnection ;
    TextInputEditText ntitle,nDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notice);
        initVariable();
        initView();
        initFunc();

    }

    private void initVariable() {
        appSessionManager = new AppSessionManager(this);
        internetConnection = new CheckInternetConnection();
    }

    private void initView() {
        ntitle = findViewById(R.id.ntitle);
        nDetails = findViewById(R.id.nDetails);
    }

    private void initFunc() {
        findViewById(R.id.back).setOnClickListener(v -> finish());

        findViewById(R.id.save).setOnClickListener(v -> validatedData());
    }

    private void validatedData() {

        if (ntitle.getText().toString().equals("")){
            ntitle.setError("Enter Notice Title");
            ntitle.requestFocus();
        }else if (nDetails.getText().toString().equals("")){
            nDetails.setError("Enter Notice Title");
            nDetails.requestFocus();
        }else {

            String title = ntitle.getText().toString().trim();
            String nDetail = nDetails.getText().toString().trim();

            callSubmitApi(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID),title,nDetail);



        }

    }

    private void callSubmitApi(String userID, String title, String nDetail) {

        if (internetConnection.isInternetAvailable(AddNoticeActivity.this)) {
            final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();

            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.submitNotice(userID,title,nDetail).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.isSuccessful()) {
                        if (response.body().get("error").getAsInt() == 0) {
                            dialog.dismiss();
                            resetData();
                            Toast.makeText(AddNoticeActivity.this, response.body().get("error_report").getAsString(), Toast.LENGTH_SHORT).show();
                           // finish();
                        } else if (response.body().get("error").getAsInt() == 1) {
                            dialog.dismiss();
                            Toast.makeText(AddNoticeActivity.this, response.body().get("error_report").getAsString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    dialog.dismiss();
                    Log.d("LOGIN", "onFailure: " + t.getMessage());
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void resetData() {
        ntitle.setText("");
        nDetails.setText("");
    }
}