package com.attendance.tracker.activity.company.message.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OutboxDataModel {
    @SerializedName("error")
    private Integer error;
    @SerializedName("error_report")
    private String errorReport;
    @SerializedName("outbox")
    private List<OutboxDataListModel> outbox = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getErrorReport() {
        return errorReport;
    }

    public void setErrorReport(String errorReport) {
        this.errorReport = errorReport;
    }

    public List<OutboxDataListModel> getOutbox() {
        return outbox;
    }

    public void setOutbox(List<OutboxDataListModel> outbox) {
        this.outbox = outbox;
    }
}
