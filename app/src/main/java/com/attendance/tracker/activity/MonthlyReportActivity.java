package com.attendance.tracker.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.attendance.tracker.R;
import com.attendance.tracker.activity.leader.DateWiseGroupReportActivity;
import com.attendance.tracker.activity.leader.DateWiseSingleReportActivity;
import com.attendance.tracker.activity.leader.leaderadapter.MonthListAdapter;
import com.attendance.tracker.activity.leader.leaderadapter.groupListAdapter;
import com.attendance.tracker.data.DateWiseGroupReportResponse;
import com.attendance.tracker.data.DatewiseGroupReportList;
import com.attendance.tracker.data.MonthlyReportList;
import com.attendance.tracker.data.MonthlyreportResponse;
import com.attendance.tracker.data.ProfileList;
import com.attendance.tracker.interfaces.OnUserClickListener;
import com.attendance.tracker.network.APIService;
import com.attendance.tracker.network.ApiUtil.ApiUtils;
import com.attendance.tracker.network.ConstantValue;
import com.attendance.tracker.utils.AppSessionManager;
import com.attendance.tracker.utils.CheckInternetConnection;
import com.google.android.material.snackbar.Snackbar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MonthlyReportActivity extends AppCompatActivity implements OnUserClickListener {
    CheckInternetConnection internetConnection;
    AppSessionManager appSessionManager;
    DatePickerDialog picker;
    TextView tv_start_date, totalAbsent,totalPresent;
    MonthListAdapter adapter;
    TextView hederTitle;
    String employeId, type;
    private ArrayList<MonthlyReportList> userDataList;
    String monthID;
    String dayID;
    String yearID;
    String currentDate,currentMonth,currentYear;
    LinearLayout noteLyt;
    private RecyclerView userList;
    GridView calenderView,DayView;
    ArrayList<MonthlyReportList> report = new ArrayList<>();
    //spinner
    String[] Month;
    String[] Years;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monthly_report);

        initVariable();
        initListener();

        //tv_start_date = findViewById(R.id.tv_start_date);
       // calenderView = findViewById(R.id.gridView);
       // DayView = findViewById(R.id.gridViewDay);
       // noteLyt = findViewById(R.id.noteLyt);
        userList = findViewById(R.id.rv_userList);
    }
    private void initVariable() {
        Intent mIntent = getIntent();
        employeId = mIntent.getStringExtra("userId");
        type = mIntent.getStringExtra("type");
        appSessionManager = new AppSessionManager(this);
        internetConnection = new CheckInternetConnection();

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        Log.w("date",""+formattedDate);
        String[] separated = formattedDate.split("-");

        currentDate = separated[0];
        currentMonth =  separated[1]; //
        currentYear =  separated[2]; //

        Years = new String[]{currentYear,"2022", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039",
                "2040", "2041", "2042", "2042"};
        Month = new String[]{currentMonth, "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
//        tv_start_date.setText(currentMonth+"-"+currentYear);
        getCalenderData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID),currentMonth,currentYear);
    }



    private void initListener() {
       // findViewById(R.id.lytStartDate).setOnClickListener(view -> showStartDatePicker());

        Spinner day_Month = findViewById(R.id.sp_Month);
        Spinner day_Year = findViewById(R.id.sp_year);

       // tv_start_date = findViewById(R.id.tv_start_date);

        day_Year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //Years[].indexOf(currentYear);
                yearID=Years[i];

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ArrayAdapter ad2
                = new ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                Years);

        ad2.setDropDownViewResource(
                android.R.layout
                        .simple_spinner_dropdown_item);
        day_Year.setAdapter(ad2);

        day_Month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                monthID=Month[i];

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        ArrayAdapter ad
                = new ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                Month);

        ad.setDropDownViewResource(
                android.R.layout
                        .simple_spinner_dropdown_item);
        day_Month.setAdapter(ad);


        findViewById(R.id.btnSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (monthID == null){
                    Toast.makeText(MonthlyReportActivity.this, "Select Month", Toast.LENGTH_SHORT).show();
                }else if(yearID == null ){
                    Toast.makeText(MonthlyReportActivity.this, "Select Year", Toast.LENGTH_SHORT).show();
                }

                else {
                    getCalenderData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID),monthID,yearID);
                }
            }
        });

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

      //  getCalenderData(appSessionManager.getUserDetails().get(AppSessionManager.KEY_USERID),employeId,monthID,yearID);
    }


    public void getCalenderData(String userID,String month,String year) {
        if (internetConnection.isInternetAvailable(getApplicationContext())) {
            // progressBar.setVisibility(View.VISIBLE);
            report.clear();
            final MaterialDialog dialog = new MaterialDialog.Builder(this).title(getResources().getString(R.string.loading))
                    .content(getResources().getString(R.string.pleaseWait))
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
            APIService mApiService = ApiUtils.getApiService(ConstantValue.URL);
            mApiService.getMonthReport(userID,month,year).enqueue(new Callback<MonthlyreportResponse>() {
                @Override
                public void onResponse(Call<MonthlyreportResponse> call, Response<MonthlyreportResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.isSuccessful())
                            //  progressBar.setVisibility(View.GONE);
                            report.addAll(response.body().getReport());
                            loadData();

                        dialog.dismiss();
                    }
                }


                @Override
                public void onFailure(Call<MonthlyreportResponse> call, Throwable t) {
                    t.printStackTrace();
                    // progressBar.setVisibility(View.GONE);
                    dialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(android.R.id.content), "(*_*) Internet connection problem!", Snackbar.LENGTH_SHORT).show();
        }
    }



    private void loadData() {
        adapter = new MonthListAdapter(report,getApplicationContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        userList.setLayoutManager(layoutManager);
        userList.setHasFixedSize(true);
        userList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        adapter.SetItemClick(this);

    }

    @Override
    public void itemUserClick(View view, int position) {
        Intent mIntent = new Intent(MonthlyReportActivity.this, DateWiseSingleReportActivity.class);
        mIntent.putExtra("userId", report.get(position).getId());
//        mIntent.putExtra("type", userType);
        startActivity(mIntent);
    }
}