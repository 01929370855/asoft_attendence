package com.attendance.tracker.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TaskReplyReportResponse {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("error_report")
    @Expose
    private String errorReport;
    @SerializedName("task_reply_list")
    @Expose
    private ArrayList<TaskReplyList> taskReplyList = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getErrorReport() {
        return errorReport;
    }

    public void setErrorReport(String errorReport) {
        this.errorReport = errorReport;
    }

    public ArrayList<TaskReplyList> getTaskReplyList() {
        return taskReplyList;
    }

    public void setTaskReplyList(ArrayList<TaskReplyList> taskReplyList) {
        this.taskReplyList = taskReplyList;
    }
}