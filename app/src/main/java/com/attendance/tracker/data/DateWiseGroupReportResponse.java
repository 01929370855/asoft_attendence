package com.attendance.tracker.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DateWiseGroupReportResponse {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("error_report")
    @Expose
    private String errorReport;
    @SerializedName("total_present")
    @Expose
    private Integer totalPresent;
    @SerializedName("total_absent")
    @Expose
    private Integer totalAbsent;
    @SerializedName("report")
    @Expose
    private ArrayList<DatewiseGroupReportList> report = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getErrorReport() {
        return errorReport;
    }

    public void setErrorReport(String errorReport) {
        this.errorReport = errorReport;
    }

    public Integer getTotalPresent() {
        return totalPresent;
    }

    public void setTotalPresent(Integer totalPresent) {
        this.totalPresent = totalPresent;
    }

    public Integer getTotalAbsent() {
        return totalAbsent;
    }

    public void setTotalAbsent(Integer totalAbsent) {
        this.totalAbsent = totalAbsent;
    }

    public ArrayList<DatewiseGroupReportList> getReport() {
        return report;
    }

    public void setReport(ArrayList<DatewiseGroupReportList> report) {
        this.report = report;
    }
}
