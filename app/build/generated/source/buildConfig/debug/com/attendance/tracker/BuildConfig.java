/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.attendance.tracker;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.attendance.tracker";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 8;
  public static final String VERSION_NAME = "1.0.8";
  // Field from build type: debug
  public static final String BASE_URL = "http://attendance-tracker.com/";
}
