// Generated by view binder compiler. Do not edit!
package com.attendance.tracker.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.attendance.tracker.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class FragmentWeeklyBinding implements ViewBinding {
  @NonNull
  private final LinearLayout rootView;

  @NonNull
  public final AppCompatButton btnSearch;

  @NonNull
  public final LinearLayout lytStartDate;

  @NonNull
  public final RecyclerView reportList;

  @NonNull
  public final Spinner spDay;

  @NonNull
  public final TextView tvStartDate;

  private FragmentWeeklyBinding(@NonNull LinearLayout rootView, @NonNull AppCompatButton btnSearch,
      @NonNull LinearLayout lytStartDate, @NonNull RecyclerView reportList, @NonNull Spinner spDay,
      @NonNull TextView tvStartDate) {
    this.rootView = rootView;
    this.btnSearch = btnSearch;
    this.lytStartDate = lytStartDate;
    this.reportList = reportList;
    this.spDay = spDay;
    this.tvStartDate = tvStartDate;
  }

  @Override
  @NonNull
  public LinearLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static FragmentWeeklyBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static FragmentWeeklyBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.fragment_weekly, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static FragmentWeeklyBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.btnSearch;
      AppCompatButton btnSearch = ViewBindings.findChildViewById(rootView, id);
      if (btnSearch == null) {
        break missingId;
      }

      id = R.id.lytStartDate;
      LinearLayout lytStartDate = ViewBindings.findChildViewById(rootView, id);
      if (lytStartDate == null) {
        break missingId;
      }

      id = R.id.reportList;
      RecyclerView reportList = ViewBindings.findChildViewById(rootView, id);
      if (reportList == null) {
        break missingId;
      }

      id = R.id.sp_day;
      Spinner spDay = ViewBindings.findChildViewById(rootView, id);
      if (spDay == null) {
        break missingId;
      }

      id = R.id.tv_start_date;
      TextView tvStartDate = ViewBindings.findChildViewById(rootView, id);
      if (tvStartDate == null) {
        break missingId;
      }

      return new FragmentWeeklyBinding((LinearLayout) rootView, btnSearch, lytStartDate, reportList,
          spDay, tvStartDate);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
