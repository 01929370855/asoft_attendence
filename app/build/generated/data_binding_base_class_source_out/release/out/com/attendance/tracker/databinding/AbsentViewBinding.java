// Generated by view binder compiler. Do not edit!
package com.attendance.tracker.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
<<<<<<< HEAD
=======
import androidx.cardview.widget.CardView;
>>>>>>> 4cc1ae07b952f451f6c2bbeaf937a5b492a37b93
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.attendance.tracker.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class AbsentViewBinding implements ViewBinding {
  @NonNull
  private final RelativeLayout rootView;

  @NonNull
<<<<<<< HEAD
  public final TextView textView;

  private AbsentViewBinding(@NonNull RelativeLayout rootView, @NonNull TextView textView) {
    this.rootView = rootView;
=======
  public final CardView cvLayout;

  @NonNull
  public final TextView textView;

  private AbsentViewBinding(@NonNull RelativeLayout rootView, @NonNull CardView cvLayout,
      @NonNull TextView textView) {
    this.rootView = rootView;
    this.cvLayout = cvLayout;
>>>>>>> 4cc1ae07b952f451f6c2bbeaf937a5b492a37b93
    this.textView = textView;
  }

  @Override
  @NonNull
  public RelativeLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static AbsentViewBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static AbsentViewBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.absent_view, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static AbsentViewBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
<<<<<<< HEAD
=======
      id = R.id.cvLayout;
      CardView cvLayout = ViewBindings.findChildViewById(rootView, id);
      if (cvLayout == null) {
        break missingId;
      }

>>>>>>> 4cc1ae07b952f451f6c2bbeaf937a5b492a37b93
      id = R.id.text_view;
      TextView textView = ViewBindings.findChildViewById(rootView, id);
      if (textView == null) {
        break missingId;
      }

<<<<<<< HEAD
      return new AbsentViewBinding((RelativeLayout) rootView, textView);
=======
      return new AbsentViewBinding((RelativeLayout) rootView, cvLayout, textView);
>>>>>>> 4cc1ae07b952f451f6c2bbeaf937a5b492a37b93
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
