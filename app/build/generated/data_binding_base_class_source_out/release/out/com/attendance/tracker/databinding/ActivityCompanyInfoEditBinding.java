// Generated by view binder compiler. Do not edit!
package com.attendance.tracker.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.attendance.tracker.R;
import com.google.android.material.textfield.TextInputEditText;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityCompanyInfoEditBinding implements ViewBinding {
  @NonNull
  private final RelativeLayout rootView;

  @NonNull
  public final TextInputEditText CompanyNameET;

  @NonNull
  public final TextInputEditText ETaddress;

  @NonNull
  public final TextInputEditText ETemail;

  @NonNull
  public final TextInputEditText ETfather;

  @NonNull
  public final TextInputEditText ETnid;

  @NonNull
  public final TextInputEditText ETno1;

  @NonNull
  public final TextInputEditText ETno2;

  @NonNull
  public final CardView addressLayout;

  @NonNull
  public final AppCompatTextView addressTV;

  @NonNull
  public final AppCompatImageView back;

  @NonNull
  public final AppCompatTextView companyName;

  @NonNull
  public final CardView companyNameLayout;

  @NonNull
  public final CardView emailLayout;

  @NonNull
  public final AppCompatTextView emailTV;

  @NonNull
  public final CardView fatherLayout;

  @NonNull
  public final AppCompatTextView fatherNameTV;

  @NonNull
  public final CardView mobNoLayout;

  @NonNull
  public final CardView mobNoLayout2;

  @NonNull
  public final CardView nidLayout;

  @NonNull
  public final AppCompatTextView nidTV;

  @NonNull
  public final AppCompatTextView no1TV;

  @NonNull
  public final AppCompatTextView no2TV;

  @NonNull
  public final RelativeLayout one;

  @NonNull
  public final AppCompatButton save;

  @NonNull
  public final View view;

  private ActivityCompanyInfoEditBinding(@NonNull RelativeLayout rootView,
      @NonNull TextInputEditText CompanyNameET, @NonNull TextInputEditText ETaddress,
      @NonNull TextInputEditText ETemail, @NonNull TextInputEditText ETfather,
      @NonNull TextInputEditText ETnid, @NonNull TextInputEditText ETno1,
      @NonNull TextInputEditText ETno2, @NonNull CardView addressLayout,
      @NonNull AppCompatTextView addressTV, @NonNull AppCompatImageView back,
      @NonNull AppCompatTextView companyName, @NonNull CardView companyNameLayout,
      @NonNull CardView emailLayout, @NonNull AppCompatTextView emailTV,
      @NonNull CardView fatherLayout, @NonNull AppCompatTextView fatherNameTV,
      @NonNull CardView mobNoLayout, @NonNull CardView mobNoLayout2, @NonNull CardView nidLayout,
      @NonNull AppCompatTextView nidTV, @NonNull AppCompatTextView no1TV,
      @NonNull AppCompatTextView no2TV, @NonNull RelativeLayout one, @NonNull AppCompatButton save,
      @NonNull View view) {
    this.rootView = rootView;
    this.CompanyNameET = CompanyNameET;
    this.ETaddress = ETaddress;
    this.ETemail = ETemail;
    this.ETfather = ETfather;
    this.ETnid = ETnid;
    this.ETno1 = ETno1;
    this.ETno2 = ETno2;
    this.addressLayout = addressLayout;
    this.addressTV = addressTV;
    this.back = back;
    this.companyName = companyName;
    this.companyNameLayout = companyNameLayout;
    this.emailLayout = emailLayout;
    this.emailTV = emailTV;
    this.fatherLayout = fatherLayout;
    this.fatherNameTV = fatherNameTV;
    this.mobNoLayout = mobNoLayout;
    this.mobNoLayout2 = mobNoLayout2;
    this.nidLayout = nidLayout;
    this.nidTV = nidTV;
    this.no1TV = no1TV;
    this.no2TV = no2TV;
    this.one = one;
    this.save = save;
    this.view = view;
  }

  @Override
  @NonNull
  public RelativeLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityCompanyInfoEditBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityCompanyInfoEditBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_company_info_edit, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityCompanyInfoEditBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.CompanyNameET;
      TextInputEditText CompanyNameET = ViewBindings.findChildViewById(rootView, id);
      if (CompanyNameET == null) {
        break missingId;
      }

      id = R.id.ETaddress;
      TextInputEditText ETaddress = ViewBindings.findChildViewById(rootView, id);
      if (ETaddress == null) {
        break missingId;
      }

      id = R.id.ETemail;
      TextInputEditText ETemail = ViewBindings.findChildViewById(rootView, id);
      if (ETemail == null) {
        break missingId;
      }

      id = R.id.ETfather;
      TextInputEditText ETfather = ViewBindings.findChildViewById(rootView, id);
      if (ETfather == null) {
        break missingId;
      }

      id = R.id.ETnid;
      TextInputEditText ETnid = ViewBindings.findChildViewById(rootView, id);
      if (ETnid == null) {
        break missingId;
      }

      id = R.id.ETno1;
      TextInputEditText ETno1 = ViewBindings.findChildViewById(rootView, id);
      if (ETno1 == null) {
        break missingId;
      }

      id = R.id.ETno2;
      TextInputEditText ETno2 = ViewBindings.findChildViewById(rootView, id);
      if (ETno2 == null) {
        break missingId;
      }

      id = R.id.address_layout;
      CardView addressLayout = ViewBindings.findChildViewById(rootView, id);
      if (addressLayout == null) {
        break missingId;
      }

      id = R.id.addressTV;
      AppCompatTextView addressTV = ViewBindings.findChildViewById(rootView, id);
      if (addressTV == null) {
        break missingId;
      }

      id = R.id.back;
      AppCompatImageView back = ViewBindings.findChildViewById(rootView, id);
      if (back == null) {
        break missingId;
      }

      id = R.id.company_name;
      AppCompatTextView companyName = ViewBindings.findChildViewById(rootView, id);
      if (companyName == null) {
        break missingId;
      }

      id = R.id.company_name_layout;
      CardView companyNameLayout = ViewBindings.findChildViewById(rootView, id);
      if (companyNameLayout == null) {
        break missingId;
      }

      id = R.id.email_layout;
      CardView emailLayout = ViewBindings.findChildViewById(rootView, id);
      if (emailLayout == null) {
        break missingId;
      }

      id = R.id.emailTV;
      AppCompatTextView emailTV = ViewBindings.findChildViewById(rootView, id);
      if (emailTV == null) {
        break missingId;
      }

      id = R.id.father_layout;
      CardView fatherLayout = ViewBindings.findChildViewById(rootView, id);
      if (fatherLayout == null) {
        break missingId;
      }

      id = R.id.father_nameTV;
      AppCompatTextView fatherNameTV = ViewBindings.findChildViewById(rootView, id);
      if (fatherNameTV == null) {
        break missingId;
      }

      id = R.id.mob_no_layout;
      CardView mobNoLayout = ViewBindings.findChildViewById(rootView, id);
      if (mobNoLayout == null) {
        break missingId;
      }

      id = R.id.mob_no_layout2;
      CardView mobNoLayout2 = ViewBindings.findChildViewById(rootView, id);
      if (mobNoLayout2 == null) {
        break missingId;
      }

      id = R.id.nid_layout;
      CardView nidLayout = ViewBindings.findChildViewById(rootView, id);
      if (nidLayout == null) {
        break missingId;
      }

      id = R.id.nidTV;
      AppCompatTextView nidTV = ViewBindings.findChildViewById(rootView, id);
      if (nidTV == null) {
        break missingId;
      }

      id = R.id.no1TV;
      AppCompatTextView no1TV = ViewBindings.findChildViewById(rootView, id);
      if (no1TV == null) {
        break missingId;
      }

      id = R.id.no2TV;
      AppCompatTextView no2TV = ViewBindings.findChildViewById(rootView, id);
      if (no2TV == null) {
        break missingId;
      }

      id = R.id.one;
      RelativeLayout one = ViewBindings.findChildViewById(rootView, id);
      if (one == null) {
        break missingId;
      }

      id = R.id.save;
      AppCompatButton save = ViewBindings.findChildViewById(rootView, id);
      if (save == null) {
        break missingId;
      }

      id = R.id.view;
      View view = ViewBindings.findChildViewById(rootView, id);
      if (view == null) {
        break missingId;
      }

      return new ActivityCompanyInfoEditBinding((RelativeLayout) rootView, CompanyNameET, ETaddress,
          ETemail, ETfather, ETnid, ETno1, ETno2, addressLayout, addressTV, back, companyName,
          companyNameLayout, emailLayout, emailTV, fatherLayout, fatherNameTV, mobNoLayout,
          mobNoLayout2, nidLayout, nidTV, no1TV, no2TV, one, save, view);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
