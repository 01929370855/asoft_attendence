// Generated by view binder compiler. Do not edit!
package com.attendance.tracker.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.attendance.tracker.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityNearbyBinding implements ViewBinding {
  @NonNull
  private final LinearLayout rootView;

  @NonNull
  public final AppCompatImageView back;

  @NonNull
  public final RecyclerView comList;

  @NonNull
  public final RelativeLayout one;

  @NonNull
  public final SeekBar seekBar;

  @NonNull
  public final AppCompatTextView tvProgress;

  private ActivityNearbyBinding(@NonNull LinearLayout rootView, @NonNull AppCompatImageView back,
      @NonNull RecyclerView comList, @NonNull RelativeLayout one, @NonNull SeekBar seekBar,
      @NonNull AppCompatTextView tvProgress) {
    this.rootView = rootView;
    this.back = back;
    this.comList = comList;
    this.one = one;
    this.seekBar = seekBar;
    this.tvProgress = tvProgress;
  }

  @Override
  @NonNull
  public LinearLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityNearbyBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityNearbyBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_nearby, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityNearbyBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.back;
      AppCompatImageView back = ViewBindings.findChildViewById(rootView, id);
      if (back == null) {
        break missingId;
      }

      id = R.id.comList;
      RecyclerView comList = ViewBindings.findChildViewById(rootView, id);
      if (comList == null) {
        break missingId;
      }

      id = R.id.one;
      RelativeLayout one = ViewBindings.findChildViewById(rootView, id);
      if (one == null) {
        break missingId;
      }

      id = R.id.seekBar;
      SeekBar seekBar = ViewBindings.findChildViewById(rootView, id);
      if (seekBar == null) {
        break missingId;
      }

      id = R.id.tvProgress;
      AppCompatTextView tvProgress = ViewBindings.findChildViewById(rootView, id);
      if (tvProgress == null) {
        break missingId;
      }

      return new ActivityNearbyBinding((LinearLayout) rootView, back, comList, one, seekBar,
          tvProgress);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
