// Generated by view binder compiler. Do not edit!
package com.attendance.tracker.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.attendance.tracker.R;
import com.google.android.material.textfield.TextInputEditText;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityCreateCompanyNewBinding implements ViewBinding {
  @NonNull
  private final RelativeLayout rootView;

  @NonNull
  public final TextInputEditText CompanyNameET;

  @NonNull
  public final TextInputEditText ETCost;

  @NonNull
  public final TextInputEditText ETUserName;

  @NonNull
  public final TextInputEditText ETaddress;

  @NonNull
  public final TextInputEditText ETconfirmpass;

  @NonNull
  public final TextInputEditText ETemail;

  @NonNull
  public final TextInputEditText ETemployee;

  @NonNull
  public final TextInputEditText ETmob2;

  @NonNull
  public final TextInputEditText ETno1;

  @NonNull
  public final TextInputEditText ETpass;

  @NonNull
  public final CardView addressLayout;

  @NonNull
  public final AppCompatTextView addressTV;

  @NonNull
  public final AppCompatImageView back;

  @NonNull
  public final AppCompatTextView catTV;

  @NonNull
  public final CardView categoryLayout;

  @NonNull
  public final AppCompatTextView companyName;

  @NonNull
  public final CardView companyNameLayout;

  @NonNull
  public final AppCompatTextView confirmPassTV;

  @NonNull
  public final LinearLayout costLayout;

  @NonNull
  public final AppCompatTextView costTV;

  @NonNull
  public final CardView countryLayout;

  @NonNull
  public final AppCompatTextView countryTV;

  @NonNull
  public final LinearLayout cpasslayout;

  @NonNull
  public final CardView districtLayout;

  @NonNull
  public final AppCompatTextView districtTV;

  @NonNull
  public final CardView divLayout;

  @NonNull
  public final AppCompatTextView divTV;

  @NonNull
  public final CardView emailLayout;

  @NonNull
  public final AppCompatTextView emailTV;

  @NonNull
  public final CardView employeeLayout;

  @NonNull
  public final AppCompatTextView employeeTV;

  @NonNull
  public final TextInputEditText etReferCode;

  @NonNull
  public final CardView mobNo2;

  @NonNull
  public final CardView mobNoLayout;

  @NonNull
  public final AppCompatTextView no1TV;

  @NonNull
  public final AppCompatTextView no2TV;

  @NonNull
  public final RelativeLayout one;

  @NonNull
  public final LinearLayout passlayout;

  @NonNull
  public final AppCompatTextView passoneTV;

  @NonNull
  public final AppCompatTextView refer;

  @NonNull
  public final CardView referCode;

  @NonNull
  public final AppCompatButton save;

  @NonNull
  public final ImageView showPassword;

  @NonNull
  public final ImageView showPassword2;

  @NonNull
  public final Spinner spCategory;

  @NonNull
  public final Spinner spCountry;

  @NonNull
  public final Spinner spDistrict;

  @NonNull
  public final Spinner spDivision;

  @NonNull
  public final Spinner spThana;

  @NonNull
  public final CardView thanaLayout;

  @NonNull
  public final AppCompatTextView thanaTV;

  @NonNull
  public final CardView userNameLayout;

  @NonNull
  public final AppCompatTextView userNameTV;

  @NonNull
  public final View view;

  private ActivityCreateCompanyNewBinding(@NonNull RelativeLayout rootView,
      @NonNull TextInputEditText CompanyNameET, @NonNull TextInputEditText ETCost,
      @NonNull TextInputEditText ETUserName, @NonNull TextInputEditText ETaddress,
      @NonNull TextInputEditText ETconfirmpass, @NonNull TextInputEditText ETemail,
      @NonNull TextInputEditText ETemployee, @NonNull TextInputEditText ETmob2,
      @NonNull TextInputEditText ETno1, @NonNull TextInputEditText ETpass,
      @NonNull CardView addressLayout, @NonNull AppCompatTextView addressTV,
      @NonNull AppCompatImageView back, @NonNull AppCompatTextView catTV,
      @NonNull CardView categoryLayout, @NonNull AppCompatTextView companyName,
      @NonNull CardView companyNameLayout, @NonNull AppCompatTextView confirmPassTV,
      @NonNull LinearLayout costLayout, @NonNull AppCompatTextView costTV,
      @NonNull CardView countryLayout, @NonNull AppCompatTextView countryTV,
      @NonNull LinearLayout cpasslayout, @NonNull CardView districtLayout,
      @NonNull AppCompatTextView districtTV, @NonNull CardView divLayout,
      @NonNull AppCompatTextView divTV, @NonNull CardView emailLayout,
      @NonNull AppCompatTextView emailTV, @NonNull CardView employeeLayout,
      @NonNull AppCompatTextView employeeTV, @NonNull TextInputEditText etReferCode,
      @NonNull CardView mobNo2, @NonNull CardView mobNoLayout, @NonNull AppCompatTextView no1TV,
      @NonNull AppCompatTextView no2TV, @NonNull RelativeLayout one,
      @NonNull LinearLayout passlayout, @NonNull AppCompatTextView passoneTV,
      @NonNull AppCompatTextView refer, @NonNull CardView referCode, @NonNull AppCompatButton save,
      @NonNull ImageView showPassword, @NonNull ImageView showPassword2,
      @NonNull Spinner spCategory, @NonNull Spinner spCountry, @NonNull Spinner spDistrict,
      @NonNull Spinner spDivision, @NonNull Spinner spThana, @NonNull CardView thanaLayout,
      @NonNull AppCompatTextView thanaTV, @NonNull CardView userNameLayout,
      @NonNull AppCompatTextView userNameTV, @NonNull View view) {
    this.rootView = rootView;
    this.CompanyNameET = CompanyNameET;
    this.ETCost = ETCost;
    this.ETUserName = ETUserName;
    this.ETaddress = ETaddress;
    this.ETconfirmpass = ETconfirmpass;
    this.ETemail = ETemail;
    this.ETemployee = ETemployee;
    this.ETmob2 = ETmob2;
    this.ETno1 = ETno1;
    this.ETpass = ETpass;
    this.addressLayout = addressLayout;
    this.addressTV = addressTV;
    this.back = back;
    this.catTV = catTV;
    this.categoryLayout = categoryLayout;
    this.companyName = companyName;
    this.companyNameLayout = companyNameLayout;
    this.confirmPassTV = confirmPassTV;
    this.costLayout = costLayout;
    this.costTV = costTV;
    this.countryLayout = countryLayout;
    this.countryTV = countryTV;
    this.cpasslayout = cpasslayout;
    this.districtLayout = districtLayout;
    this.districtTV = districtTV;
    this.divLayout = divLayout;
    this.divTV = divTV;
    this.emailLayout = emailLayout;
    this.emailTV = emailTV;
    this.employeeLayout = employeeLayout;
    this.employeeTV = employeeTV;
    this.etReferCode = etReferCode;
    this.mobNo2 = mobNo2;
    this.mobNoLayout = mobNoLayout;
    this.no1TV = no1TV;
    this.no2TV = no2TV;
    this.one = one;
    this.passlayout = passlayout;
    this.passoneTV = passoneTV;
    this.refer = refer;
    this.referCode = referCode;
    this.save = save;
    this.showPassword = showPassword;
    this.showPassword2 = showPassword2;
    this.spCategory = spCategory;
    this.spCountry = spCountry;
    this.spDistrict = spDistrict;
    this.spDivision = spDivision;
    this.spThana = spThana;
    this.thanaLayout = thanaLayout;
    this.thanaTV = thanaTV;
    this.userNameLayout = userNameLayout;
    this.userNameTV = userNameTV;
    this.view = view;
  }

  @Override
  @NonNull
  public RelativeLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityCreateCompanyNewBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityCreateCompanyNewBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_create_company_new, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityCreateCompanyNewBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.CompanyNameET;
      TextInputEditText CompanyNameET = ViewBindings.findChildViewById(rootView, id);
      if (CompanyNameET == null) {
        break missingId;
      }

      id = R.id.ETCost;
      TextInputEditText ETCost = ViewBindings.findChildViewById(rootView, id);
      if (ETCost == null) {
        break missingId;
      }

      id = R.id.ETUser_name;
      TextInputEditText ETUserName = ViewBindings.findChildViewById(rootView, id);
      if (ETUserName == null) {
        break missingId;
      }

      id = R.id.ETaddress;
      TextInputEditText ETaddress = ViewBindings.findChildViewById(rootView, id);
      if (ETaddress == null) {
        break missingId;
      }

      id = R.id.ETconfirmpass;
      TextInputEditText ETconfirmpass = ViewBindings.findChildViewById(rootView, id);
      if (ETconfirmpass == null) {
        break missingId;
      }

      id = R.id.ETemail;
      TextInputEditText ETemail = ViewBindings.findChildViewById(rootView, id);
      if (ETemail == null) {
        break missingId;
      }

      id = R.id.ETemployee;
      TextInputEditText ETemployee = ViewBindings.findChildViewById(rootView, id);
      if (ETemployee == null) {
        break missingId;
      }

      id = R.id.ETmob2;
      TextInputEditText ETmob2 = ViewBindings.findChildViewById(rootView, id);
      if (ETmob2 == null) {
        break missingId;
      }

      id = R.id.ETno1;
      TextInputEditText ETno1 = ViewBindings.findChildViewById(rootView, id);
      if (ETno1 == null) {
        break missingId;
      }

      id = R.id.ETpass;
      TextInputEditText ETpass = ViewBindings.findChildViewById(rootView, id);
      if (ETpass == null) {
        break missingId;
      }

      id = R.id.address_layout;
      CardView addressLayout = ViewBindings.findChildViewById(rootView, id);
      if (addressLayout == null) {
        break missingId;
      }

      id = R.id.addressTV;
      AppCompatTextView addressTV = ViewBindings.findChildViewById(rootView, id);
      if (addressTV == null) {
        break missingId;
      }

      id = R.id.back;
      AppCompatImageView back = ViewBindings.findChildViewById(rootView, id);
      if (back == null) {
        break missingId;
      }

      id = R.id.catTV;
      AppCompatTextView catTV = ViewBindings.findChildViewById(rootView, id);
      if (catTV == null) {
        break missingId;
      }

      id = R.id.category_layout;
      CardView categoryLayout = ViewBindings.findChildViewById(rootView, id);
      if (categoryLayout == null) {
        break missingId;
      }

      id = R.id.company_name;
      AppCompatTextView companyName = ViewBindings.findChildViewById(rootView, id);
      if (companyName == null) {
        break missingId;
      }

      id = R.id.company_name_layout;
      CardView companyNameLayout = ViewBindings.findChildViewById(rootView, id);
      if (companyNameLayout == null) {
        break missingId;
      }

      id = R.id.confirm_passTV;
      AppCompatTextView confirmPassTV = ViewBindings.findChildViewById(rootView, id);
      if (confirmPassTV == null) {
        break missingId;
      }

      id = R.id.cost_layout;
      LinearLayout costLayout = ViewBindings.findChildViewById(rootView, id);
      if (costLayout == null) {
        break missingId;
      }

      id = R.id.costTV;
      AppCompatTextView costTV = ViewBindings.findChildViewById(rootView, id);
      if (costTV == null) {
        break missingId;
      }

      id = R.id.country_layout;
      CardView countryLayout = ViewBindings.findChildViewById(rootView, id);
      if (countryLayout == null) {
        break missingId;
      }

      id = R.id.countryTV;
      AppCompatTextView countryTV = ViewBindings.findChildViewById(rootView, id);
      if (countryTV == null) {
        break missingId;
      }

      id = R.id.cpasslayout;
      LinearLayout cpasslayout = ViewBindings.findChildViewById(rootView, id);
      if (cpasslayout == null) {
        break missingId;
      }

      id = R.id.district_layout;
      CardView districtLayout = ViewBindings.findChildViewById(rootView, id);
      if (districtLayout == null) {
        break missingId;
      }

      id = R.id.districtTV;
      AppCompatTextView districtTV = ViewBindings.findChildViewById(rootView, id);
      if (districtTV == null) {
        break missingId;
      }

      id = R.id.div_layout;
      CardView divLayout = ViewBindings.findChildViewById(rootView, id);
      if (divLayout == null) {
        break missingId;
      }

      id = R.id.divTV;
      AppCompatTextView divTV = ViewBindings.findChildViewById(rootView, id);
      if (divTV == null) {
        break missingId;
      }

      id = R.id.email_layout;
      CardView emailLayout = ViewBindings.findChildViewById(rootView, id);
      if (emailLayout == null) {
        break missingId;
      }

      id = R.id.emailTV;
      AppCompatTextView emailTV = ViewBindings.findChildViewById(rootView, id);
      if (emailTV == null) {
        break missingId;
      }

      id = R.id.employee_layout;
      CardView employeeLayout = ViewBindings.findChildViewById(rootView, id);
      if (employeeLayout == null) {
        break missingId;
      }

      id = R.id.employeeTV;
      AppCompatTextView employeeTV = ViewBindings.findChildViewById(rootView, id);
      if (employeeTV == null) {
        break missingId;
      }

      id = R.id.et_referCode;
      TextInputEditText etReferCode = ViewBindings.findChildViewById(rootView, id);
      if (etReferCode == null) {
        break missingId;
      }

      id = R.id.mob_no_2;
      CardView mobNo2 = ViewBindings.findChildViewById(rootView, id);
      if (mobNo2 == null) {
        break missingId;
      }

      id = R.id.mob_no_layout;
      CardView mobNoLayout = ViewBindings.findChildViewById(rootView, id);
      if (mobNoLayout == null) {
        break missingId;
      }

      id = R.id.no1TV;
      AppCompatTextView no1TV = ViewBindings.findChildViewById(rootView, id);
      if (no1TV == null) {
        break missingId;
      }

      id = R.id.no2TV;
      AppCompatTextView no2TV = ViewBindings.findChildViewById(rootView, id);
      if (no2TV == null) {
        break missingId;
      }

      id = R.id.one;
      RelativeLayout one = ViewBindings.findChildViewById(rootView, id);
      if (one == null) {
        break missingId;
      }

      id = R.id.passlayout;
      LinearLayout passlayout = ViewBindings.findChildViewById(rootView, id);
      if (passlayout == null) {
        break missingId;
      }

      id = R.id.passoneTV;
      AppCompatTextView passoneTV = ViewBindings.findChildViewById(rootView, id);
      if (passoneTV == null) {
        break missingId;
      }

      id = R.id.refer;
      AppCompatTextView refer = ViewBindings.findChildViewById(rootView, id);
      if (refer == null) {
        break missingId;
      }

      id = R.id.refer_code;
      CardView referCode = ViewBindings.findChildViewById(rootView, id);
      if (referCode == null) {
        break missingId;
      }

      id = R.id.save;
      AppCompatButton save = ViewBindings.findChildViewById(rootView, id);
      if (save == null) {
        break missingId;
      }

      id = R.id.showPassword;
      ImageView showPassword = ViewBindings.findChildViewById(rootView, id);
      if (showPassword == null) {
        break missingId;
      }

      id = R.id.showPassword2;
      ImageView showPassword2 = ViewBindings.findChildViewById(rootView, id);
      if (showPassword2 == null) {
        break missingId;
      }

      id = R.id.sp_category;
      Spinner spCategory = ViewBindings.findChildViewById(rootView, id);
      if (spCategory == null) {
        break missingId;
      }

      id = R.id.sp_country;
      Spinner spCountry = ViewBindings.findChildViewById(rootView, id);
      if (spCountry == null) {
        break missingId;
      }

      id = R.id.sp_district;
      Spinner spDistrict = ViewBindings.findChildViewById(rootView, id);
      if (spDistrict == null) {
        break missingId;
      }

      id = R.id.sp_division;
      Spinner spDivision = ViewBindings.findChildViewById(rootView, id);
      if (spDivision == null) {
        break missingId;
      }

      id = R.id.sp_thana;
      Spinner spThana = ViewBindings.findChildViewById(rootView, id);
      if (spThana == null) {
        break missingId;
      }

      id = R.id.thana_layout;
      CardView thanaLayout = ViewBindings.findChildViewById(rootView, id);
      if (thanaLayout == null) {
        break missingId;
      }

      id = R.id.thanaTV;
      AppCompatTextView thanaTV = ViewBindings.findChildViewById(rootView, id);
      if (thanaTV == null) {
        break missingId;
      }

      id = R.id.user_name_layout;
      CardView userNameLayout = ViewBindings.findChildViewById(rootView, id);
      if (userNameLayout == null) {
        break missingId;
      }

      id = R.id.userNameTV;
      AppCompatTextView userNameTV = ViewBindings.findChildViewById(rootView, id);
      if (userNameTV == null) {
        break missingId;
      }

      id = R.id.view;
      View view = ViewBindings.findChildViewById(rootView, id);
      if (view == null) {
        break missingId;
      }

      return new ActivityCreateCompanyNewBinding((RelativeLayout) rootView, CompanyNameET, ETCost,
          ETUserName, ETaddress, ETconfirmpass, ETemail, ETemployee, ETmob2, ETno1, ETpass,
          addressLayout, addressTV, back, catTV, categoryLayout, companyName, companyNameLayout,
          confirmPassTV, costLayout, costTV, countryLayout, countryTV, cpasslayout, districtLayout,
          districtTV, divLayout, divTV, emailLayout, emailTV, employeeLayout, employeeTV,
          etReferCode, mobNo2, mobNoLayout, no1TV, no2TV, one, passlayout, passoneTV, refer,
          referCode, save, showPassword, showPassword2, spCategory, spCountry, spDistrict,
          spDivision, spThana, thanaLayout, thanaTV, userNameLayout, userNameTV, view);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
