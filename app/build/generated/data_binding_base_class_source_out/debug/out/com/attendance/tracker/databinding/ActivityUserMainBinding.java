// Generated by view binder compiler. Do not edit!
package com.attendance.tracker.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.attendance.tracker.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityUserMainBinding implements ViewBinding {
  @NonNull
  private final RelativeLayout rootView;

  @NonNull
  public final CardView cvChnagepass;

  @NonNull
  public final CardView cvDateWise;

  @NonNull
  public final CardView cvEmpAttendance;

  @NonNull
  public final CardView cvGoToTask;

  @NonNull
  public final CardView cvGotoMap;

  @NonNull
  public final CardView cvManualAttID;

  @NonNull
  public final CardView cvShareApp;

  @NonNull
  public final CardView cvSupport;

  @NonNull
  public final CircleImageView img;

  @NonNull
  public final ImageView img1;

  @NonNull
  public final ImageView img10;

  @NonNull
  public final ImageView img2;

  @NonNull
  public final ImageView img22;

  @NonNull
  public final ImageView img3;

  @NonNull
  public final ImageView img4;

  @NonNull
  public final ImageView img7;

  @NonNull
  public final ImageView img8;

  @NonNull
  public final ImageView logo;

  @NonNull
  public final ImageView logout;

  @NonNull
  public final LinearLayout lytUserData;

  @NonNull
  public final LinearLayout one;

  @NonNull
  public final ImageView profile;

  @NonNull
  public final AppCompatTextView tv1;

  @NonNull
  public final AppCompatTextView tv2;

  @NonNull
  public final AppCompatTextView tv22;

  @NonNull
  public final AppCompatTextView tv3;

  @NonNull
  public final AppCompatTextView tv4;

  @NonNull
  public final AppCompatTextView tv7;

  @NonNull
  public final AppCompatTextView tv8;

  @NonNull
  public final AppCompatTextView tv9;

  @NonNull
  public final AppCompatTextView tvemployee;

  @NonNull
  public final TextView userMobile;

  @NonNull
  public final TextView userName;

  private ActivityUserMainBinding(@NonNull RelativeLayout rootView, @NonNull CardView cvChnagepass,
      @NonNull CardView cvDateWise, @NonNull CardView cvEmpAttendance, @NonNull CardView cvGoToTask,
      @NonNull CardView cvGotoMap, @NonNull CardView cvManualAttID, @NonNull CardView cvShareApp,
      @NonNull CardView cvSupport, @NonNull CircleImageView img, @NonNull ImageView img1,
      @NonNull ImageView img10, @NonNull ImageView img2, @NonNull ImageView img22,
      @NonNull ImageView img3, @NonNull ImageView img4, @NonNull ImageView img7,
      @NonNull ImageView img8, @NonNull ImageView logo, @NonNull ImageView logout,
      @NonNull LinearLayout lytUserData, @NonNull LinearLayout one, @NonNull ImageView profile,
      @NonNull AppCompatTextView tv1, @NonNull AppCompatTextView tv2,
      @NonNull AppCompatTextView tv22, @NonNull AppCompatTextView tv3,
      @NonNull AppCompatTextView tv4, @NonNull AppCompatTextView tv7,
      @NonNull AppCompatTextView tv8, @NonNull AppCompatTextView tv9,
      @NonNull AppCompatTextView tvemployee, @NonNull TextView userMobile,
      @NonNull TextView userName) {
    this.rootView = rootView;
    this.cvChnagepass = cvChnagepass;
    this.cvDateWise = cvDateWise;
    this.cvEmpAttendance = cvEmpAttendance;
    this.cvGoToTask = cvGoToTask;
    this.cvGotoMap = cvGotoMap;
    this.cvManualAttID = cvManualAttID;
    this.cvShareApp = cvShareApp;
    this.cvSupport = cvSupport;
    this.img = img;
    this.img1 = img1;
    this.img10 = img10;
    this.img2 = img2;
    this.img22 = img22;
    this.img3 = img3;
    this.img4 = img4;
    this.img7 = img7;
    this.img8 = img8;
    this.logo = logo;
    this.logout = logout;
    this.lytUserData = lytUserData;
    this.one = one;
    this.profile = profile;
    this.tv1 = tv1;
    this.tv2 = tv2;
    this.tv22 = tv22;
    this.tv3 = tv3;
    this.tv4 = tv4;
    this.tv7 = tv7;
    this.tv8 = tv8;
    this.tv9 = tv9;
    this.tvemployee = tvemployee;
    this.userMobile = userMobile;
    this.userName = userName;
  }

  @Override
  @NonNull
  public RelativeLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityUserMainBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityUserMainBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_user_main, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityUserMainBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.cv_chnagepass;
      CardView cvChnagepass = ViewBindings.findChildViewById(rootView, id);
      if (cvChnagepass == null) {
        break missingId;
      }

      id = R.id.cvDateWise;
      CardView cvDateWise = ViewBindings.findChildViewById(rootView, id);
      if (cvDateWise == null) {
        break missingId;
      }

      id = R.id.cvEmpAttendance;
      CardView cvEmpAttendance = ViewBindings.findChildViewById(rootView, id);
      if (cvEmpAttendance == null) {
        break missingId;
      }

      id = R.id.cvGoToTask;
      CardView cvGoToTask = ViewBindings.findChildViewById(rootView, id);
      if (cvGoToTask == null) {
        break missingId;
      }

      id = R.id.cvGotoMap;
      CardView cvGotoMap = ViewBindings.findChildViewById(rootView, id);
      if (cvGotoMap == null) {
        break missingId;
      }

      id = R.id.cv_manualAttID;
      CardView cvManualAttID = ViewBindings.findChildViewById(rootView, id);
      if (cvManualAttID == null) {
        break missingId;
      }

      id = R.id.cvShareApp;
      CardView cvShareApp = ViewBindings.findChildViewById(rootView, id);
      if (cvShareApp == null) {
        break missingId;
      }

      id = R.id.cv_support;
      CardView cvSupport = ViewBindings.findChildViewById(rootView, id);
      if (cvSupport == null) {
        break missingId;
      }

      id = R.id.img;
      CircleImageView img = ViewBindings.findChildViewById(rootView, id);
      if (img == null) {
        break missingId;
      }

      id = R.id.img1;
      ImageView img1 = ViewBindings.findChildViewById(rootView, id);
      if (img1 == null) {
        break missingId;
      }

      id = R.id.img10;
      ImageView img10 = ViewBindings.findChildViewById(rootView, id);
      if (img10 == null) {
        break missingId;
      }

      id = R.id.img2;
      ImageView img2 = ViewBindings.findChildViewById(rootView, id);
      if (img2 == null) {
        break missingId;
      }

      id = R.id.img22;
      ImageView img22 = ViewBindings.findChildViewById(rootView, id);
      if (img22 == null) {
        break missingId;
      }

      id = R.id.img3;
      ImageView img3 = ViewBindings.findChildViewById(rootView, id);
      if (img3 == null) {
        break missingId;
      }

      id = R.id.img4;
      ImageView img4 = ViewBindings.findChildViewById(rootView, id);
      if (img4 == null) {
        break missingId;
      }

      id = R.id.img7;
      ImageView img7 = ViewBindings.findChildViewById(rootView, id);
      if (img7 == null) {
        break missingId;
      }

      id = R.id.img8;
      ImageView img8 = ViewBindings.findChildViewById(rootView, id);
      if (img8 == null) {
        break missingId;
      }

      id = R.id.logo;
      ImageView logo = ViewBindings.findChildViewById(rootView, id);
      if (logo == null) {
        break missingId;
      }

      id = R.id.logout;
      ImageView logout = ViewBindings.findChildViewById(rootView, id);
      if (logout == null) {
        break missingId;
      }

      id = R.id.lyt_userData;
      LinearLayout lytUserData = ViewBindings.findChildViewById(rootView, id);
      if (lytUserData == null) {
        break missingId;
      }

      id = R.id.one;
      LinearLayout one = ViewBindings.findChildViewById(rootView, id);
      if (one == null) {
        break missingId;
      }

      id = R.id.profile;
      ImageView profile = ViewBindings.findChildViewById(rootView, id);
      if (profile == null) {
        break missingId;
      }

      id = R.id.tv1;
      AppCompatTextView tv1 = ViewBindings.findChildViewById(rootView, id);
      if (tv1 == null) {
        break missingId;
      }

      id = R.id.tv2;
      AppCompatTextView tv2 = ViewBindings.findChildViewById(rootView, id);
      if (tv2 == null) {
        break missingId;
      }

      id = R.id.tv22;
      AppCompatTextView tv22 = ViewBindings.findChildViewById(rootView, id);
      if (tv22 == null) {
        break missingId;
      }

      id = R.id.tv3;
      AppCompatTextView tv3 = ViewBindings.findChildViewById(rootView, id);
      if (tv3 == null) {
        break missingId;
      }

      id = R.id.tv4;
      AppCompatTextView tv4 = ViewBindings.findChildViewById(rootView, id);
      if (tv4 == null) {
        break missingId;
      }

      id = R.id.tv7;
      AppCompatTextView tv7 = ViewBindings.findChildViewById(rootView, id);
      if (tv7 == null) {
        break missingId;
      }

      id = R.id.tv8;
      AppCompatTextView tv8 = ViewBindings.findChildViewById(rootView, id);
      if (tv8 == null) {
        break missingId;
      }

      id = R.id.tv9;
      AppCompatTextView tv9 = ViewBindings.findChildViewById(rootView, id);
      if (tv9 == null) {
        break missingId;
      }

      id = R.id.tvemployee;
      AppCompatTextView tvemployee = ViewBindings.findChildViewById(rootView, id);
      if (tvemployee == null) {
        break missingId;
      }

      id = R.id.userMobile;
      TextView userMobile = ViewBindings.findChildViewById(rootView, id);
      if (userMobile == null) {
        break missingId;
      }

      id = R.id.userName;
      TextView userName = ViewBindings.findChildViewById(rootView, id);
      if (userName == null) {
        break missingId;
      }

      return new ActivityUserMainBinding((RelativeLayout) rootView, cvChnagepass, cvDateWise,
          cvEmpAttendance, cvGoToTask, cvGotoMap, cvManualAttID, cvShareApp, cvSupport, img, img1,
          img10, img2, img22, img3, img4, img7, img8, logo, logout, lytUserData, one, profile, tv1,
          tv2, tv22, tv3, tv4, tv7, tv8, tv9, tvemployee, userMobile, userName);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
