// Generated by view binder compiler. Do not edit!
package com.attendance.tracker.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.attendance.tracker.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ItemGroupListBinding implements ViewBinding {
  @NonNull
  private final CardView rootView;

  @NonNull
  public final TextView employenameTV;

  @NonNull
  public final TextView inTV;

  @NonNull
  public final LinearLayout one;

  @NonNull
  public final TextView outTV;

  @NonNull
  public final TextView satusTV;

  private ItemGroupListBinding(@NonNull CardView rootView, @NonNull TextView employenameTV,
      @NonNull TextView inTV, @NonNull LinearLayout one, @NonNull TextView outTV,
      @NonNull TextView satusTV) {
    this.rootView = rootView;
    this.employenameTV = employenameTV;
    this.inTV = inTV;
    this.one = one;
    this.outTV = outTV;
    this.satusTV = satusTV;
  }

  @Override
  @NonNull
  public CardView getRoot() {
    return rootView;
  }

  @NonNull
  public static ItemGroupListBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ItemGroupListBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.item_group_list, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ItemGroupListBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.employenameTV;
      TextView employenameTV = ViewBindings.findChildViewById(rootView, id);
      if (employenameTV == null) {
        break missingId;
      }

      id = R.id.inTV;
      TextView inTV = ViewBindings.findChildViewById(rootView, id);
      if (inTV == null) {
        break missingId;
      }

      id = R.id.one;
      LinearLayout one = ViewBindings.findChildViewById(rootView, id);
      if (one == null) {
        break missingId;
      }

      id = R.id.outTV;
      TextView outTV = ViewBindings.findChildViewById(rootView, id);
      if (outTV == null) {
        break missingId;
      }

      id = R.id.satusTV;
      TextView satusTV = ViewBindings.findChildViewById(rootView, id);
      if (satusTV == null) {
        break missingId;
      }

      return new ItemGroupListBinding((CardView) rootView, employenameTV, inTV, one, outTV,
          satusTV);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
