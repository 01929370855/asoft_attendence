// Generated by view binder compiler. Do not edit!
package com.attendance.tracker.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.attendance.tracker.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ItemNoticeBinding implements ViewBinding {
  @NonNull
  private final CardView rootView;

  @NonNull
  public final LinearLayout lytMain;

  @NonNull
  public final TextView mobileTV;

  @NonNull
  public final TextView nameTV;

  @NonNull
  public final TextView sl;

  private ItemNoticeBinding(@NonNull CardView rootView, @NonNull LinearLayout lytMain,
      @NonNull TextView mobileTV, @NonNull TextView nameTV, @NonNull TextView sl) {
    this.rootView = rootView;
    this.lytMain = lytMain;
    this.mobileTV = mobileTV;
    this.nameTV = nameTV;
    this.sl = sl;
  }

  @Override
  @NonNull
  public CardView getRoot() {
    return rootView;
  }

  @NonNull
  public static ItemNoticeBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ItemNoticeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.item_notice, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ItemNoticeBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.lytMain;
      LinearLayout lytMain = ViewBindings.findChildViewById(rootView, id);
      if (lytMain == null) {
        break missingId;
      }

      id = R.id.mobileTV;
      TextView mobileTV = ViewBindings.findChildViewById(rootView, id);
      if (mobileTV == null) {
        break missingId;
      }

      id = R.id.nameTV;
      TextView nameTV = ViewBindings.findChildViewById(rootView, id);
      if (nameTV == null) {
        break missingId;
      }

      id = R.id.sl;
      TextView sl = ViewBindings.findChildViewById(rootView, id);
      if (sl == null) {
        break missingId;
      }

      return new ItemNoticeBinding((CardView) rootView, lytMain, mobileTV, nameTV, sl);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
