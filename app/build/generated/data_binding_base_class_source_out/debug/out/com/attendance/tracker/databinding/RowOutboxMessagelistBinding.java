// Generated by view binder compiler. Do not edit!
package com.attendance.tracker.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.attendance.tracker.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class RowOutboxMessagelistBinding implements ViewBinding {
  @NonNull
  private final CardView rootView;

  @NonNull
  public final TextView messegeContent;

  @NonNull
  public final TextView messegeSendTime;

  @NonNull
  public final TextView messegeSender;

  private RowOutboxMessagelistBinding(@NonNull CardView rootView, @NonNull TextView messegeContent,
      @NonNull TextView messegeSendTime, @NonNull TextView messegeSender) {
    this.rootView = rootView;
    this.messegeContent = messegeContent;
    this.messegeSendTime = messegeSendTime;
    this.messegeSender = messegeSender;
  }

  @Override
  @NonNull
  public CardView getRoot() {
    return rootView;
  }

  @NonNull
  public static RowOutboxMessagelistBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static RowOutboxMessagelistBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.row_outbox_messagelist, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static RowOutboxMessagelistBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.messege_content;
      TextView messegeContent = ViewBindings.findChildViewById(rootView, id);
      if (messegeContent == null) {
        break missingId;
      }

      id = R.id.messege_send_time;
      TextView messegeSendTime = ViewBindings.findChildViewById(rootView, id);
      if (messegeSendTime == null) {
        break missingId;
      }

      id = R.id.messege_sender;
      TextView messegeSender = ViewBindings.findChildViewById(rootView, id);
      if (messegeSender == null) {
        break missingId;
      }

      return new RowOutboxMessagelistBinding((CardView) rootView, messegeContent, messegeSendTime,
          messegeSender);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
