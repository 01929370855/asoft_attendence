// Generated by view binder compiler. Do not edit!
package com.attendance.tracker.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.attendance.tracker.R;
import com.google.android.material.textfield.TextInputEditText;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityLeaderJoinBinding implements ViewBinding {
  @NonNull
  private final RelativeLayout rootView;

  @NonNull
  public final TextInputEditText ETconfirmpass;

  @NonNull
  public final TextInputEditText ETpass;

  @NonNull
  public final AppCompatImageView back;

  @NonNull
  public final AppCompatTextView companyName;

  @NonNull
  public final CardView companyNameLayout;

  @NonNull
  public final CardView companyNid;

  @NonNull
  public final AppCompatTextView confirmPassTV;

  @NonNull
  public final CardView countryLayout;

  @NonNull
  public final AppCompatTextView countryTV;

  @NonNull
  public final LinearLayout cpasslayout;

  @NonNull
  public final CardView districtLayout;

  @NonNull
  public final AppCompatTextView districtTV;

  @NonNull
  public final CardView divLayout;

  @NonNull
  public final AppCompatTextView divTV;

  @NonNull
  public final TextInputEditText email;

  @NonNull
  public final TextInputEditText father;

  @NonNull
  public final LinearLayout lytLeader;

  @NonNull
  public final CardView mobNo2;

  @NonNull
  public final CardView mobNoLayout;

  @NonNull
  public final TextInputEditText mother;

  @NonNull
  public final TextInputEditText name;

  @NonNull
  public final TextInputEditText nid;

  @NonNull
  public final AppCompatTextView no1TV;

  @NonNull
  public final AppCompatTextView no2TV;

  @NonNull
  public final TextInputEditText number;

  @NonNull
  public final RelativeLayout one;

  @NonNull
  public final LinearLayout passlayout;

  @NonNull
  public final AppCompatTextView passoneTV;

  @NonNull
  public final LinearLayout root2;

  @NonNull
  public final AppCompatButton save;

  @NonNull
  public final ImageView showPassword;

  @NonNull
  public final ImageView showPassword2;

  @NonNull
  public final Spinner spCountry;

  @NonNull
  public final Spinner spDistrict;

  @NonNull
  public final Spinner spDivision;

  @NonNull
  public final Spinner spLeaderList;

  @NonNull
  public final Spinner spThana;

  @NonNull
  public final Spinner spType;

  @NonNull
  public final CardView thanaLayout;

  @NonNull
  public final AppCompatTextView thanaTV;

  @NonNull
  public final View view;

  private ActivityLeaderJoinBinding(@NonNull RelativeLayout rootView,
      @NonNull TextInputEditText ETconfirmpass, @NonNull TextInputEditText ETpass,
      @NonNull AppCompatImageView back, @NonNull AppCompatTextView companyName,
      @NonNull CardView companyNameLayout, @NonNull CardView companyNid,
      @NonNull AppCompatTextView confirmPassTV, @NonNull CardView countryLayout,
      @NonNull AppCompatTextView countryTV, @NonNull LinearLayout cpasslayout,
      @NonNull CardView districtLayout, @NonNull AppCompatTextView districtTV,
      @NonNull CardView divLayout, @NonNull AppCompatTextView divTV,
      @NonNull TextInputEditText email, @NonNull TextInputEditText father,
      @NonNull LinearLayout lytLeader, @NonNull CardView mobNo2, @NonNull CardView mobNoLayout,
      @NonNull TextInputEditText mother, @NonNull TextInputEditText name,
      @NonNull TextInputEditText nid, @NonNull AppCompatTextView no1TV,
      @NonNull AppCompatTextView no2TV, @NonNull TextInputEditText number,
      @NonNull RelativeLayout one, @NonNull LinearLayout passlayout,
      @NonNull AppCompatTextView passoneTV, @NonNull LinearLayout root2,
      @NonNull AppCompatButton save, @NonNull ImageView showPassword,
      @NonNull ImageView showPassword2, @NonNull Spinner spCountry, @NonNull Spinner spDistrict,
      @NonNull Spinner spDivision, @NonNull Spinner spLeaderList, @NonNull Spinner spThana,
      @NonNull Spinner spType, @NonNull CardView thanaLayout, @NonNull AppCompatTextView thanaTV,
      @NonNull View view) {
    this.rootView = rootView;
    this.ETconfirmpass = ETconfirmpass;
    this.ETpass = ETpass;
    this.back = back;
    this.companyName = companyName;
    this.companyNameLayout = companyNameLayout;
    this.companyNid = companyNid;
    this.confirmPassTV = confirmPassTV;
    this.countryLayout = countryLayout;
    this.countryTV = countryTV;
    this.cpasslayout = cpasslayout;
    this.districtLayout = districtLayout;
    this.districtTV = districtTV;
    this.divLayout = divLayout;
    this.divTV = divTV;
    this.email = email;
    this.father = father;
    this.lytLeader = lytLeader;
    this.mobNo2 = mobNo2;
    this.mobNoLayout = mobNoLayout;
    this.mother = mother;
    this.name = name;
    this.nid = nid;
    this.no1TV = no1TV;
    this.no2TV = no2TV;
    this.number = number;
    this.one = one;
    this.passlayout = passlayout;
    this.passoneTV = passoneTV;
    this.root2 = root2;
    this.save = save;
    this.showPassword = showPassword;
    this.showPassword2 = showPassword2;
    this.spCountry = spCountry;
    this.spDistrict = spDistrict;
    this.spDivision = spDivision;
    this.spLeaderList = spLeaderList;
    this.spThana = spThana;
    this.spType = spType;
    this.thanaLayout = thanaLayout;
    this.thanaTV = thanaTV;
    this.view = view;
  }

  @Override
  @NonNull
  public RelativeLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityLeaderJoinBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityLeaderJoinBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_leader_join, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityLeaderJoinBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.ETconfirmpass;
      TextInputEditText ETconfirmpass = ViewBindings.findChildViewById(rootView, id);
      if (ETconfirmpass == null) {
        break missingId;
      }

      id = R.id.ETpass;
      TextInputEditText ETpass = ViewBindings.findChildViewById(rootView, id);
      if (ETpass == null) {
        break missingId;
      }

      id = R.id.back;
      AppCompatImageView back = ViewBindings.findChildViewById(rootView, id);
      if (back == null) {
        break missingId;
      }

      id = R.id.company_name;
      AppCompatTextView companyName = ViewBindings.findChildViewById(rootView, id);
      if (companyName == null) {
        break missingId;
      }

      id = R.id.company_name_layout;
      CardView companyNameLayout = ViewBindings.findChildViewById(rootView, id);
      if (companyNameLayout == null) {
        break missingId;
      }

      id = R.id.company_nid;
      CardView companyNid = ViewBindings.findChildViewById(rootView, id);
      if (companyNid == null) {
        break missingId;
      }

      id = R.id.confirm_passTV;
      AppCompatTextView confirmPassTV = ViewBindings.findChildViewById(rootView, id);
      if (confirmPassTV == null) {
        break missingId;
      }

      id = R.id.country_layout;
      CardView countryLayout = ViewBindings.findChildViewById(rootView, id);
      if (countryLayout == null) {
        break missingId;
      }

      id = R.id.countryTV;
      AppCompatTextView countryTV = ViewBindings.findChildViewById(rootView, id);
      if (countryTV == null) {
        break missingId;
      }

      id = R.id.cpasslayout;
      LinearLayout cpasslayout = ViewBindings.findChildViewById(rootView, id);
      if (cpasslayout == null) {
        break missingId;
      }

      id = R.id.district_layout;
      CardView districtLayout = ViewBindings.findChildViewById(rootView, id);
      if (districtLayout == null) {
        break missingId;
      }

      id = R.id.districtTV;
      AppCompatTextView districtTV = ViewBindings.findChildViewById(rootView, id);
      if (districtTV == null) {
        break missingId;
      }

      id = R.id.div_layout;
      CardView divLayout = ViewBindings.findChildViewById(rootView, id);
      if (divLayout == null) {
        break missingId;
      }

      id = R.id.divTV;
      AppCompatTextView divTV = ViewBindings.findChildViewById(rootView, id);
      if (divTV == null) {
        break missingId;
      }

      id = R.id.email;
      TextInputEditText email = ViewBindings.findChildViewById(rootView, id);
      if (email == null) {
        break missingId;
      }

      id = R.id.father;
      TextInputEditText father = ViewBindings.findChildViewById(rootView, id);
      if (father == null) {
        break missingId;
      }

      id = R.id.lytLeader;
      LinearLayout lytLeader = ViewBindings.findChildViewById(rootView, id);
      if (lytLeader == null) {
        break missingId;
      }

      id = R.id.mob_no_2;
      CardView mobNo2 = ViewBindings.findChildViewById(rootView, id);
      if (mobNo2 == null) {
        break missingId;
      }

      id = R.id.mob_no_layout;
      CardView mobNoLayout = ViewBindings.findChildViewById(rootView, id);
      if (mobNoLayout == null) {
        break missingId;
      }

      id = R.id.mother;
      TextInputEditText mother = ViewBindings.findChildViewById(rootView, id);
      if (mother == null) {
        break missingId;
      }

      id = R.id.name;
      TextInputEditText name = ViewBindings.findChildViewById(rootView, id);
      if (name == null) {
        break missingId;
      }

      id = R.id.nid;
      TextInputEditText nid = ViewBindings.findChildViewById(rootView, id);
      if (nid == null) {
        break missingId;
      }

      id = R.id.no1TV;
      AppCompatTextView no1TV = ViewBindings.findChildViewById(rootView, id);
      if (no1TV == null) {
        break missingId;
      }

      id = R.id.no2TV;
      AppCompatTextView no2TV = ViewBindings.findChildViewById(rootView, id);
      if (no2TV == null) {
        break missingId;
      }

      id = R.id.number;
      TextInputEditText number = ViewBindings.findChildViewById(rootView, id);
      if (number == null) {
        break missingId;
      }

      id = R.id.one;
      RelativeLayout one = ViewBindings.findChildViewById(rootView, id);
      if (one == null) {
        break missingId;
      }

      id = R.id.passlayout;
      LinearLayout passlayout = ViewBindings.findChildViewById(rootView, id);
      if (passlayout == null) {
        break missingId;
      }

      id = R.id.passoneTV;
      AppCompatTextView passoneTV = ViewBindings.findChildViewById(rootView, id);
      if (passoneTV == null) {
        break missingId;
      }

      id = R.id.root2;
      LinearLayout root2 = ViewBindings.findChildViewById(rootView, id);
      if (root2 == null) {
        break missingId;
      }

      id = R.id.save;
      AppCompatButton save = ViewBindings.findChildViewById(rootView, id);
      if (save == null) {
        break missingId;
      }

      id = R.id.showPassword;
      ImageView showPassword = ViewBindings.findChildViewById(rootView, id);
      if (showPassword == null) {
        break missingId;
      }

      id = R.id.showPassword2;
      ImageView showPassword2 = ViewBindings.findChildViewById(rootView, id);
      if (showPassword2 == null) {
        break missingId;
      }

      id = R.id.sp_country;
      Spinner spCountry = ViewBindings.findChildViewById(rootView, id);
      if (spCountry == null) {
        break missingId;
      }

      id = R.id.sp_district;
      Spinner spDistrict = ViewBindings.findChildViewById(rootView, id);
      if (spDistrict == null) {
        break missingId;
      }

      id = R.id.sp_division;
      Spinner spDivision = ViewBindings.findChildViewById(rootView, id);
      if (spDivision == null) {
        break missingId;
      }

      id = R.id.sp_leader_list;
      Spinner spLeaderList = ViewBindings.findChildViewById(rootView, id);
      if (spLeaderList == null) {
        break missingId;
      }

      id = R.id.sp_thana;
      Spinner spThana = ViewBindings.findChildViewById(rootView, id);
      if (spThana == null) {
        break missingId;
      }

      id = R.id.sp_type;
      Spinner spType = ViewBindings.findChildViewById(rootView, id);
      if (spType == null) {
        break missingId;
      }

      id = R.id.thana_layout;
      CardView thanaLayout = ViewBindings.findChildViewById(rootView, id);
      if (thanaLayout == null) {
        break missingId;
      }

      id = R.id.thanaTV;
      AppCompatTextView thanaTV = ViewBindings.findChildViewById(rootView, id);
      if (thanaTV == null) {
        break missingId;
      }

      id = R.id.view;
      View view = ViewBindings.findChildViewById(rootView, id);
      if (view == null) {
        break missingId;
      }

      return new ActivityLeaderJoinBinding((RelativeLayout) rootView, ETconfirmpass, ETpass, back,
          companyName, companyNameLayout, companyNid, confirmPassTV, countryLayout, countryTV,
          cpasslayout, districtLayout, districtTV, divLayout, divTV, email, father, lytLeader,
          mobNo2, mobNoLayout, mother, name, nid, no1TV, no2TV, number, one, passlayout, passoneTV,
          root2, save, showPassword, showPassword2, spCountry, spDistrict, spDivision, spLeaderList,
          spThana, spType, thanaLayout, thanaTV, view);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
